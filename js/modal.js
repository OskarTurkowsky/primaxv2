// Normalmente se inicia con esta sentencia
$(document).ready(function() {
	// Cuerpo principal
	$modalEliminar = $("#modalEliminar");
	
	$modalEditar = $("#modalEditar");

	
	$('#btn-editar').on('click', editSubject);
	$('#btn-delete').on('click', deleteSubject);
	$('[data-editar]').on('click', showEdit);
	$('[data-eliminar]').on('click', showDelete);
	//$("#btn-register").on('click', show);


});

var $modalEditar;
var $modalEliminar;




function registerSubject () {
	event.preventDefault();
    var url = 'php/createC.php';
    var data = $("#register-form").serializeArray();
    $.ajax({
        url: url,
        data: data,
        method: 'POST'
    })
    .done(function( response ) {
    	console.log(response);
        if(response.error) {
        	console.log(response.message);
            alert(response.message);
        }else{
            alert(response.message);
            location.reload();
        }

    });
}

function showEdit() {
    var id = $(this).data('id');
    $modalEditar.find('[id="id"]').val(id);

	

	$modalEditar.modal('show');
}

function editSubject () {
	event.preventDefault();
    var url = 'php/updateC.php';
    var data = $("#edit-form").serializeArray();
    $.ajax({
        url: url,
        data: data,
        method: 'POST'
    })
    .done(function( response ) {
    	console.log(response);
        if(response.error) {
        	console.log(response.message);
            alert(response.message);
        }else{
            alert(response.message);
            location.reload();
        }

    });
}

function showDelete() {
    var id = $(this).data('id');
    $modalEliminar.find('[name="id"]').val(id);

	var nombre = $(this).data('nombre');
	$modalEliminar.find('[name="nombreEliminar"]').val(nombre);
	$modalEliminar.modal('show');
}

function deleteSubject () {
	event.preventDefault();
    var url = 'php/deleteC.php';
    var data = $("#delete-form").serializeArray();
    console.log(data);
    $.ajax({
        url: url,
        data: data,
        method: 'POST'
    })
    .done(function( response ) {
    	console.log(response);
        if(response.error) {
        	console.log(response.message);
            alert(response.message);
        }else{
            alert(response.message);
            location.reload();
        }

    });
}



// Vamos a realizar varias cosas en esta parte
