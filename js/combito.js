$(document).ready(function(){
  
  $('#provincias').on('change', function(){
   

    var id = $('#provincias').val()
    $.ajax({
      type: 'POST',
      url: '../php/distritos.php',
      data: {'id': id}
    })
    .done(function(dep){
      $('#distritos').html(dep)
    })
    .fail(function(){
      alert('Hubo un errror al cargar los distritos')
    })
  })

  
})