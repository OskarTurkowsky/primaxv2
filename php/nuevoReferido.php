﻿<?php 
	require 'funciones.php';
	@session_start();
	$dni					  = $_POST['dni'];
	$paterno 				= strtoupper($_POST['paterno']);
	$materno 				= strtoupper($_POST['materno']);
	$nombres 				= strtoupper($_POST['nombres']);
	$telefono1 			= $_POST['telefono1'];
	$telefono2			= $_POST['telefono2'];
	$telefono3			= $_POST['telefono3'];
	$departamento 	= $_POST['departamento'];
	$provincia			= $_POST['provincia'];
  $distrito				= $_POST['distrito'];
	$citado					= $_POST['citado'];
	$cliente				= $_POST['cliente'];
	$idTurno				= $_POST['turno'];
	$fecha					= $_POST['from_date'];
	$observacion		= $_POST['observaciones'];
	$resultado		  = $_POST['resultado'];
	$usuario 				= $_SESSION['id'];

	if ($telefono2 != '') {
		$telefono2 = ' / '.$telefono2;
		$telefono1 = $telefono1.$telefono2;
	}

	if ($telefono3 != '') {
		$telefono3 = ' / '.$telefono3;
		$telefono1 = $telefono1.$telefono3;
	}	

	date_default_timezone_set('America/Lima');
	$fecha_actual = date('Y-m-d H:i:s');

	$consulta3 = ejecutarQuery("SELECT * FROM persona where idPersona=$dni");
  $existP=mysqli_fetch_assoc($consulta3);
  $consulta3 = ejecutarQuery("SELECT * FROM backup where idBackup=$dni");
  $existB=mysqli_fetch_assoc($consulta3);
  $consulta3 = ejecutarQuery("SELECT * FROM referido where idReferido=$dni");
  $existR=mysqli_fetch_assoc($consulta3);
  $var=1;
  if ($existP['idPersona']==$dni || $existB['idBackup']==$dni || $existR['idReferido']==$dni) {
  	$var=0;
  }

	if ($resultado!='TURNO LLENO' && $var==1) {
	$consulta = ejecutarQuery("INSERT INTO persona (idPersona, apellidoPaterno, apellidoMaterno, nombres, telefono, idDistrito, estado, idUsuario, idGenero) VALUES(
													".$dni.",
													'".$paterno."',
													'".$materno."',
													'".$nombres."',
													'".$telefono1."',
													".$distrito.",
													30,
													".$usuario.",
													1	
												)");

  $consulta = ejecutarQuery("INSERT INTO backup (idBackup, fechaEvaluacion, disponibilidad, idConsultor ) VALUES (
  									".$dni.",
  									'".$fecha."',	
  									'POR DEFINIR',
  									".$citado."
  									 )");

  $consulta = ejecutarQuery("INSERT INTO referido (idReferido, idCliente, idConsultor,idUsuario, estado) VALUES (
  									".$dni.",
  									".$cliente.",
  									".$citado.",
                    ".$usuario.",
  									1
  									 )");

  if ($resultado=='EMPTY') {
  	$consulta = ejecutarQuery("INSERT INTO fecha (idFecha) VALUES (
  									'$fecha'
  									 )");
    $consulta = ejecutarQuery("INSERT INTO entrevista (idFecha, idTurno, acumulador) VALUES (
                    '$fecha',
                    $idTurno,
                    1
                     )");
  }
  else
  	{
      if ($resultado=='EMPTY ENTREVISTA') {
        $consulta = ejecutarQuery("INSERT INTO entrevista (idFecha, idTurno, acumulador) VALUES (
                    '$fecha',
                    $idTurno,
                    1
                     )");
      }
  		if ($resultado=='OK') {
    		$consulta2 = ejecutarQuery("SELECT * FROM entrevista where idFecha='$fecha' and idTurno=$idTurno");
        $entrevista=mysqli_fetch_assoc($consulta2);
        $acumulador=$entrevista['acumulador'];
        $acumulador++;
        $consulta = ejecutarQuery("UPDATE entrevista SET acumulador = $acumulador WHERE idFecha = '$fecha' and idTurno=$idTurno");
              
  	  }
    }

  $consulta3 = ejecutarQuery("SELECT idEntrevista FROM entrevista where idFecha='$fecha' and idTurno=$idTurno");
  $entrevista2=mysqli_fetch_assoc($consulta3);
  $idEntrevista=$entrevista2['idEntrevista'];

  $consulta = ejecutarQuery("INSERT INTO entrevista_referido (idEntrevista, idReferido, idUsuario) VALUES (
  									$idEntrevista,
  									$dni,
                    $usuario
  									 )");
  ?>
		<script>
			alert("Datos guardados correctamente");
    	window.location = '../vistas/nuevoReferido.php';
    </script>
	<?php   
	}
   else
   {
   	?>
		<script>
		  alert("Postulante ya está registrado");
		  window.location = '../vistas/nuevoReferido.php';
		</script>
   	<?php
   }
?>