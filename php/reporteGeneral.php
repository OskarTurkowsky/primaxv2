<?php
require 'funciones.php';
$fechaInicio    = $_POST['fechaInicio'];
$fechaFin       = $_POST['fechaFin'];
$cliente       = $_POST['cliente'];

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
  die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
               ->setLastModifiedBy("Maarten Balliauw")
               ->setTitle("Office 2007 XLSX Test Document")
               ->setSubject("Office 2007 XLSX Test Document")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('F8', 'RAZÓN SOCIAL:')
            ->setCellValue('F9', 'RUC:')
            ->setCellValue('F10', 'MONEDA:')
            ->setCellValue('G8', 'T-SOLUCIONA SAC')
            ->setCellValue('G9', 20482204440)
            ->setCellValue('G10', 'SOLES')
            ->setCellValue('J8', 'CARGO')
            ->setCellValue('J9', 'VENDEDOR DE PLAYA')
            ->setCellValue('J10', 'VENDEDOR DE TIENDA')
            ->setCellValue('K8', 'MONTO')
            ->setCellValue('D12', 'N°')
            ->setCellValue('E12', 'CC')
            ->setCellValue('F12', 'DESCRIPCIÓN')
            ->setCellValue('G12', 'ESTACIÓN')
            ->setCellValue('H12', 'MONTO')
            ->setCellValue('I12', 'CUENTA')
            ->setCellValue('J12', 'APELLIDOS Y NOMBRES')
            ->setCellValue('K12', 'DNI')
            ->setCellValue('L12', 'CARGO')
            ->setCellValue('M12', 'PROVINCIA')
            ->setCellValue('N12', 'F. INGRESO')
            ->setCellValue('O12', 'GARANTIA A 45 DIAS')
            ->setCellValue('P12', 'FECHA CESE')
            ->setCellValue('Q12', 'CUMPLE NO CUMPLE')
            ->setCellValue('R12', 'TELEFONO');

$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        )
);

$objPHPExcel->getActiveSheet()->getStyle('F8:F10')->applyFromArray($styleArray);
unset($styleArray);

//borde celdas
$styleBorder = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getActiveSheet()->getStyle('G8:G10')->applyFromArray($styleBorder);
$objPHPExcel->getActiveSheet()->getStyle('J8:K10')->applyFromArray($styleBorder);
$objPHPExcel->getActiveSheet()->getStyle('D12:R12')->applyFromArray($styleBorder);




//alineacion: centro
$styleArray2 = array(
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);
$objPHPExcel->getActiveSheet()->getStyle('D12:R500')->applyFromArray($styleArray2);
unset($styleArray2);

//bold
$objPHPExcel->getActiveSheet()->getStyle('J8:K8')->getFont()->setBold( true );

//fondo celda
$objPHPExcel->getActiveSheet()->getStyle('F8:F10')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle('J12:R12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle('D12:I12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC000');

//color letra
$styleFont = array(
    'font'  => array(
        'color' => array('rgb' => 'C00000')
    ));

$objPHPExcel->getActiveSheet()->getStyle('G8:G10')->applyFromArray($styleFont);
$objPHPExcel->getActiveSheet()->getStyle('D12:R12')->applyFromArray($styleFont);

//autosize
foreach(range('A','S') as $columnID) { 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) 
     ->setAutoSize(true); 
} 

//alto celdas
foreach(range('D','E') as $columnID) { 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) 
     ->setAutoSize(false); 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setWidth(10);
} 

foreach(range('H','I') as $columnID) { 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) 
     ->setAutoSize(false); 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setWidth(20);
} 

foreach(range('P','Q') as $columnID) { 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) 
     ->setAutoSize(false); 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setWidth(18);
} 

//ancho celdas
$objPHPExcel->getActiveSheet()->getRowDimension('12')->setRowHeight(45);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Reporte');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$query = ejecutarQuery("SELECT count(*) as contador FROM persona 
  inner join asignado on persona.idPersona=asignado.idPersona
  inner join detalle_requerimiento on detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_Requerimiento
  inner join requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
  inner join cliente on cliente.idCliente=requerimiento.idCliente
  where requerimiento.fechaAlta between '$fechaInicio' and '$fechaFin' and cliente.idCliente=$cliente");
$result = mysqli_fetch_assoc($query);
$contador=$result['contador']+13;
$i=13;
$query1 = ejecutarQuery("SELECT concat(persona.apellidoPaterno,' ', persona.apellidoMaterno,' ', persona.nombres) as nombres, persona.idPersona as dni, estacion.nombre as estacion, cargo.nombre as cargo, provincia.nombre as provincia, requerimiento.fechaAlta as fechaAlta, persona.telefono as telefono  FROM persona 
  inner join asignado on persona.idPersona=asignado.idPersona
  inner join distrito on distrito.idDistrito=persona.idDistrito
  inner join provincia on provincia.idProvincia=distrito.idProvincia
  inner join detalle_requerimiento on detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_Requerimiento
  inner join estacion on estacion.idEstacion=detalle_requerimiento.idEstacion
  inner join cargo on cargo.idcargo=detalle_requerimiento.idcargo
  inner join requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
  inner join cliente on cliente.idCliente=requerimiento.idCliente
  where requerimiento.fechaAlta between '$fechaInicio' and '$fechaFin' and cliente.idCliente=$cliente");

while($result2=mysqli_fetch_assoc($query1)){
  $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue("F$i", 'SELECCIÓN  DE PERSONAL')
      ->setCellValue("G$i", $result2['estacion'])
      ->setCellValue("J$i", $result2['nombres'])
      ->setCellValue("K$i", $result2['dni'])
      ->setCellValue("L$i", $result2['cargo'])
      ->setCellValue("M$i", $result2['provincia'])
      ->setCellValue("N$i", date("d/m/Y", strtotime($result2['fechaAlta'])))
      ->setCellValue("O$i", date("d/m/Y",strtotime($result2['fechaAlta']."+ 45 days")))      
      ->setCellValue("R$i", $result2['telefono']);
      $i++;
}

$contador=$contador-1;
$objPHPExcel->getActiveSheet()->getStyle("D12:R$contador")->applyFromArray($styleBorder);
unset($styleBorder);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
