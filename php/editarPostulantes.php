﻿<?php 

	require 'funciones.php';
	@session_start();
	
	$tallaBotas 		= $_POST['tallaBotas'];
	$tallaUniforme		= $_POST['tallaUniforme'];
	$telefono1 			= $_POST['telefono1'];
	$telefono2			= $_POST['telefono2'];
	$telefono3			= $_POST['telefono3'];
	$email					= $_POST['email'];
	$distrito 			= $_POST['distrito'];
	$direccion 			= $_POST['direccion'];
	$paterno 				= strtoupper($_POST['paterno']);
	$materno 				= strtoupper($_POST['materno']);
	$nombres 				= strtoupper($_POST['nombres']);
	$sexo 					= $_POST['sexo'];
	$fechaNacimiento	= $_POST['fechaNacimiento'];
	$estadoCivil		= $_POST['estadoCivil'];
	
	$nivelEducativo	= $_POST['nivelEducativo'];
	$dni						= $_GET['dni'];
	$observacion		= $_POST['observacion'];
	$idQuantum				= $_POST['quantum'];
	$aptitud				= $_POST['aptitud'];

	$spp 						= $_POST['spp'];
	$fechaspp				= $_POST['fechaSpp'];
	$codigospp			= $_POST['codigoSpp'];

	if ($telefono2 != '') {
		$telefono2 = ' / '.$telefono2;
		$telefono1 = $telefono1.$telefono2;
	}

	if ($telefono3 != '') {
		$telefono3 = ' / '.$telefono3;
		$telefono1 = $telefono1.$telefono3;
	}	

	$disponibilidad	= $_POST['disponibilidad'];
	$admin=$_SESSION['admin'];
	date_default_timezone_set('America/Lima');
	$fechaactual = date('Y-m-d H:i:s');

	 $persona = ejecutarQuery("UPDATE persona SET
                                    tallaBotas    			= ".$tallaBotas.",
                                    tallaUniforme       = '".$tallaUniforme."',
                                    telefono            = '".$telefono1."',
                                    email 							= '".$email."',
                                    apellidoPaterno 		= '".$paterno."',
																		apellidoMaterno 		= '".$materno."',
																		nombres 						= '".$nombres."',
																		idGenero =  ".$sexo.",
																		idDistrito 					= ".$distrito.",
                                    fechaNacimiento     = '".$fechaNacimiento."',
                                    estadoCivil  				= '".$estadoCivil."',
                                    idNivelEducativo      = ".$nivelEducativo.",
                                    idSpp = ".$spp.",
                                    idQuantum = ".$idQuantum.",
																		fechaSpp = '".$fechaspp."',
																		codigoSpp = '".$codigospp."',	
																		direccion = '".$direccion."',
																		observacion = '".$observacion."',
																		fechaEdicion = '".$fechaactual."'
																		WHERE idPersona = $dni
                            ");

	 $consulta = ejecutarQuery("INSERT INTO persona_ocurrencia (idPersona, apellidoPaterno, apellidoMaterno, nombres, idGenero, telefono, idDistrito, observacion, estado,idSpp, fechaSpp,codigoSpp, idUsuario, idOcurrencia_tipo) VALUES(
													".$dni.",
													'".$paterno."',
													'".$materno."',
													'".$nombres."',
													".$sexo.",
													'".$telefono1."',
													".$distrito.",
												  	'".$observacion."',
													0,
													".$spp.",
													'".$fechaspp."',
													'".$codigospp."',		
													".$usuario.",
													3	
												)");
	
	$consulta = ejecutarQuery("SELECT estado FROM persona where idPersona=$dni");
	$row=mysqli_fetch_array($consulta);
	$estado=$row['estado'];
	//razon antecedente
	if ($estado>1) {
		$antecedente		= $_POST['antecedente'];
		$tipoAntecedente= strtoupper($_POST['razon']);
	}
	//postulante sin antecedentes -> con antecedente
	if (($estado==2  && $antecedente==1) && ($admin==1 || $admin==3)) {
		$cliente = ejecutarQuery("UPDATE persona SET estado=9 where idPersona=$dni");
	}
	//con antecedentes -> sin antecedentes
	if ($estado==9 && $antecedente==0) {
		$cliente = ejecutarQuery("UPDATE persona SET estado=13 where idPersona=$dni");
	}
	//sin antecedentes ->con antecedentes
	if (($estado>2 && $antecedente==1) && ($admin==1 || $admin==3)) {			
		$consulta1 = ejecutarQuery("SELECT detalle_requerimiento.idDetalle_requerimiento as idDetalle_requerimiento FROM detalle_requerimiento INNER JOIN asignado ON asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento where idPersona=$dni"); 
    $rr=mysqli_fetch_assoc($consulta1);
    $detReq=$rr['idDetalle_requerimiento'];
    $consulta = ejecutarQuery("UPDATE detalle_requerimiento SET estado=0  WHERE idDetalle_requerimiento = $detReq ");
    $cliente = ejecutarQuery("DELETE FROM asignado where idPersona=$dni ");
		$cliente = ejecutarQuery("UPDATE persona SET estado=9 where idPersona=$dni");
    $cliente = ejecutarQuery("UPDATE persona SET tipoAntecedente='$tipoAntecedente' where idPersona=$dni");
	}

	//aptitud apto y observado
	if ($aptitud==3 || $aptitud==1 ) {
		$cliente = ejecutarQuery("UPDATE persona SET idAptitud=$aptitud where idPersona=$dni");
	}
	
	//NO APTO
	if ($aptitud==2) {
		$cliente = ejecutarQuery("UPDATE persona SET estado=21 where idPersona=$dni");
		$cliente = ejecutarQuery("UPDATE persona SET idAptitud=$aptitud where idPersona=$dni");
		$consulta1 = ejecutarQuery("SELECT detalle_requerimiento.idDetalle_requerimiento as idDetalle_requerimiento FROM detalle_requerimiento INNER JOIN asignado ON asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento where idPersona=$dni"); 
    $rr=mysqli_fetch_assoc($consulta1);
    $detReq=$rr['idDetalle_requerimiento'];
    $consulta = ejecutarQuery("UPDATE detalle_requerimiento SET estado=0  WHERE idDetalle_requerimiento = $detReq ");
	}
	


	if ($disponibilidad=='NO') {			
		
		$consulta1 = ejecutarQuery("SELECT detalle_requerimiento.idDetalle_requerimiento as idDetalle_requerimiento FROM detalle_requerimiento INNER JOIN asignado ON asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento where idPersona=$dni"); 
    $rr=mysqli_fetch_assoc($consulta1);
    $detReq=$rr['idDetalle_requerimiento'];
    $consulta = ejecutarQuery("UPDATE detalle_requerimiento SET estado=0  WHERE idDetalle_requerimiento = $detReq ");
    $cliente = ejecutarQuery("DELETE FROM asignado where idPersona=$dni ");
		$cliente = ejecutarQuery("UPDATE persona SET estado=0 where idPersona=$dni");

		$consulta = ejecutarQuery("UPDATE backup SET
														disponibilidad ='".$disponibilidad."'
														WHERE idBackup = $dni
													"
							);
	}
	
?>

<script>
    		window.history.go(-2);  
</script>
