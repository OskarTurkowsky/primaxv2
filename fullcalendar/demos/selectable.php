<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<link href='../dist/core/main.css' rel='stylesheet' />
<link href='../dist/daygrid/main.css' rel='stylesheet' />
<link href='../dist/timegrid/main.css' rel='stylesheet' />
<script src='../dist/core/main.js'></script>
<script src='../dist/interaction/main.js'></script>
<script src='../dist/daygrid/main.js'></script>
<script src='../dist/timegrid/main.js'></script>
<script src='../packages/core/locales-all.js'></script>
<script src='../packages/list/main.js'></script>
<link href='../node_modules/dragula/dist/dragula.css' rel='stylesheet' />
<script src='../node_modules/dragula/dist/dragula.js'></script>
<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var initialLocaleCode = 'es';

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      defaultDate: '2019-02-12',
      locale: initialLocaleCode,
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      select: function(arg) {
        var variableJS=arg.start.toLocaleDateString("US-en");
        console.log(variableJS);
        document.getElementById("fecha").innerHTML = variableJS;
        var title = prompt('Event Title:');
        if (title) {
          calendar.addEvent({
            title: title,
            start: arg.start,
            end: arg.end,
            allDay: arg.allDay
          })
        }
        calendar.unselect()
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: []
    });

    calendar.render();
  });

</script>
<style>

  body {
    margin-top: 40px;
    font-size: 14px;
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
  }

  #wrap {
    width: 1100px;
    margin: 0 auto;
  }

  #external-events {
    float: left;
    width: 150px;
    padding: 0 10px;
    border: 1px solid #ccc;
    background: #eee;
    text-align: left;
  }

  #external-events h4 {
    font-size: 16px;
    margin-top: 0;
    padding-top: 1em;
  }

  #external-events .fc-event {
    margin: 10px 0;
    cursor: pointer;
  }

  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
  }

  #external-events p input {
    margin: 0;
    vertical-align: middle;
  }

  #calendar {
    float: right;
    width: 900px;
  }

</style>
</head>
<body>
  <div id='wrap'>

    <div id='external-events'>
      <h4>Lista de Turnos</h4>

      <div id='external-events-list'>
        <div class='fc-event'>Turno 1: 9:00 am</div>
        <div class='fc-event'>Turno 2: 11:00 am</div>
        <div class='fc-event'>Turno 3: 2:30 pm</div>
      </div>

      <div>
        <div>
          Fecha: <label id="fecha"></label>
        </div>
        <div>
          Turno: 
          <select>
            <option>Turno 1</option>
            <option>Turno 2</option>
            <option>Turno 3</option>
          </select>
        </div>
        
      </div>

    </div>

    <div id='calendar'></div>

    <div style='clear:both'></div>

  </div>
</body>
</html>
