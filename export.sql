-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: primaxv2
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aptitud`
--

DROP TABLE IF EXISTS `aptitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aptitud` (
  `idAptitud` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idAptitud`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asignado`
--

DROP TABLE IF EXISTS `asignado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignado` (
  `idAsignado` int(11) NOT NULL AUTO_INCREMENT,
  `idDetalle_requerimiento` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idAsignado`),
  KEY `fk_asignacion_detalle_requerimiento1_idx` (`idDetalle_requerimiento`),
  KEY `fk_asignacion_persona1_idx` (`idPersona`),
  CONSTRAINT `fk_asignacion_detalle_requerimiento1` FOREIGN KEY (`idDetalle_requerimiento`) REFERENCES `detalle_requerimiento` (`idDetalle_requerimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asignacion_persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1890 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `backup`
--

DROP TABLE IF EXISTS `backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backup` (
  `idBackup` int(11) NOT NULL,
  `fechaEvaluacion` date DEFAULT NULL,
  `policial` varchar(2) DEFAULT NULL,
  `sanidad` varchar(2) DEFAULT NULL,
  `disponibilidad` varchar(16) NOT NULL,
  `fechaEdicion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idConsultor` int(11) NOT NULL,
  PRIMARY KEY (`idBackup`),
  KEY `fk_backup_consultor1_idx` (`idConsultor`),
  CONSTRAINT `fk_backup_consultor1` FOREIGN KEY (`idConsultor`) REFERENCES `consultor` (`idConsultor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clinica`
--

DROP TABLE IF EXISTS `clinica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinica` (
  `idClinica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(96) NOT NULL,
  `idProvincia` int(11) NOT NULL,
  `localidad` varchar(48) NOT NULL,
  `idCliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idClinica`),
  KEY `fk_clinica_provincia1_idx` (`idProvincia`),
  CONSTRAINT `fk_clinica_provincia1` FOREIGN KEY (`idProvincia`) REFERENCES `provincia` (`idProvincia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `consultor`
--

DROP TABLE IF EXISTS `consultor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consultor` (
  `idConsultor` int(11) NOT NULL,
  `nombre` varchar(90) DEFAULT NULL,
  `idDistrito` int(11) DEFAULT NULL,
  `estado` int(4) DEFAULT NULL,
  PRIMARY KEY (`idConsultor`),
  KEY `fk_consultor_distrito1_idx` (`idDistrito`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `idContrato` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL,
  PRIMARY KEY (`idContrato`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `idDepartamento` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detalle_requerimiento`
--

DROP TABLE IF EXISTS `detalle_requerimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_requerimiento` (
  `idDetalle_requerimiento` int(11) NOT NULL,
  `idcargo` int(11) NOT NULL,
  `idEstacion` int(11) NOT NULL,
  `idRequerimiento` char(4) NOT NULL,
  `idGenero` int(11) NOT NULL,
  `idClinica` int(11) DEFAULT NULL,
  `estado` int(4) NOT NULL DEFAULT '0',
  `idTurno_carta` int(11) DEFAULT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `idContrato` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDetalle_requerimiento`),
  KEY `fk_detalle_requerimiento1_cargo1_idx` (`idcargo`),
  KEY `fk_detalle_requerimiento1_estacion1_idx` (`idEstacion`),
  KEY `fk_detalle_requerimiento1_requerimiento1_idx` (`idRequerimiento`),
  KEY `fk_detalle_requerimiento_genero1_idx` (`idGenero`),
  KEY `fk_detalle_requerimiento_clinica1_idx` (`idClinica`),
  KEY `fk_detalle_requerimiento_turno_carta1_idx` (`idTurno_carta`),
  CONSTRAINT `fk_detalle_requerimiento_genero1` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_requerimiento_requerimiento1` FOREIGN KEY (`idRequerimiento`) REFERENCES `requerimiento` (`idRequerimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_requerimiento_turno_carta1` FOREIGN KEY (`idTurno_carta`) REFERENCES `turno_carta` (`idTurno_carta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distrito`
--

DROP TABLE IF EXISTS `distrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distrito` (
  `idDistrito` int(11) NOT NULL AUTO_INCREMENT,
  `idProvincia` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idDistrito`),
  KEY `idProvincia` (`idProvincia`),
  CONSTRAINT `distritos_ibfk_1` FOREIGN KEY (`idProvincia`) REFERENCES `provincia` (`idProvincia`)
) ENGINE=InnoDB AUTO_INCREMENT=250410 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entrevista`
--

DROP TABLE IF EXISTS `entrevista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrevista` (
  `idEntrevista` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_edicion` datetime DEFAULT NULL,
  `acumulador` int(11) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idTurno` int(11) NOT NULL,
  `idFecha` date NOT NULL,
  PRIMARY KEY (`idEntrevista`),
  KEY `fk_Fecha_turno_Turnos1_idx` (`idTurno`),
  KEY `fk_Fecha_turno_Fecha1_idx` (`idFecha`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entrevista_referido`
--

DROP TABLE IF EXISTS `entrevista_referido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrevista_referido` (
  `idEntrevista_referido` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) DEFAULT '1',
  `fecha_edicion` datetime DEFAULT NULL,
  `asistencia` int(11) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idEntrevista` int(11) NOT NULL,
  `idReferido` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idEntrevista_referido`),
  KEY `fk_Entrevista_has_referido_referido1_idx` (`idReferido`),
  KEY `fk_Entrevista_has_referido_Entrevista1_idx` (`idEntrevista`),
  KEY `fk_Entrevista_Referido_usuario1_idx` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estacion`
--

DROP TABLE IF EXISTS `estacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estacion` (
  `idEstacion` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `zona` varchar(32) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `idDepartamento` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  PRIMARY KEY (`idEstacion`),
  KEY `idDepartamento` (`idDepartamento`),
  KEY `fk_estacion_cliente1_idx` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estacion_distrito`
--

DROP TABLE IF EXISTS `estacion_distrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estacion_distrito` (
  `idEstacion_distrito` int(11) NOT NULL AUTO_INCREMENT,
  `idEstacion` int(11) NOT NULL,
  `idDistrito` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`idEstacion_distrito`),
  KEY `distrito_id` (`idDistrito`),
  KEY `fk_estacion_distrito_estacion1_idx` (`idEstacion`),
  KEY `fk_estacion_distrito_usuario1_idx` (`idUsuario`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1322 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fecha`
--

DROP TABLE IF EXISTS `fecha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha` (
  `idFecha` date NOT NULL,
  `dia` date DEFAULT NULL,
  PRIMARY KEY (`idFecha`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idGenero` int(11) NOT NULL,
  `nombre` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `historial_asignar`
--

DROP TABLE IF EXISTS `historial_asignar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historial_asignar` (
  `idAsignado` int(11) NOT NULL AUTO_INCREMENT,
  `idDetalle_requerimiento` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  PRIMARY KEY (`idAsignado`),
  KEY `fk_asignacion_detalle_requerimiento1_idx` (`idDetalle_requerimiento`),
  KEY `fk_asignacion_persona1_idx` (`idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=1213 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `niveleducativo`
--

DROP TABLE IF EXISTS `niveleducativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `niveleducativo` (
  `idNivelEducativo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`idNivelEducativo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ocurrencia`
--

DROP TABLE IF EXISTS `ocurrencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocurrencia` (
  `idOcurrencia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idOcurrencia`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `tallaBotas` int(11) DEFAULT NULL,
  `tallaUniforme` varchar(2) DEFAULT NULL,
  `telefono` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `apellidoPaterno` varchar(64) DEFAULT NULL,
  `apellidoMaterno` varchar(64) DEFAULT NULL,
  `nombres` varchar(96) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `estadoCivil` varchar(16) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `fechaSpp` date DEFAULT NULL,
  `codigoSpp` varchar(32) DEFAULT NULL,
  `observacion` text,
  `estado` tinyint(4) DEFAULT '0',
  `FechaDeRegistro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaEdicion` datetime DEFAULT NULL,
  `fechaExamenMedicoReal` date DEFAULT NULL,
  `fechaAntecedente` datetime DEFAULT NULL,
  `tipoAntecedente` varchar(120) DEFAULT NULL,
  `idDistrito` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idGenero` int(11) NOT NULL,
  `idNivelEducativo` int(11) DEFAULT NULL,
  `idSpp` int(11) DEFAULT NULL,
  `estadoAsignacion` tinyint(4) DEFAULT '0',
  `tallaCamisa` varchar(2) DEFAULT NULL,
  `tallaPantalon` int(11) DEFAULT NULL,
  `idTipoSangre` int(11) DEFAULT NULL,
  `idTurno` int(11) DEFAULT NULL,
  `idAptitud` int(11) DEFAULT NULL,
  `detalle_observado` varchar(150) DEFAULT NULL,
  `hijo_conyugue` varchar(3) DEFAULT NULL,
  `fecha_asignacion_antecedente` datetime DEFAULT NULL,
  `idQuantum` int(11) DEFAULT NULL,
  `idTurno_carta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPersona`),
  KEY `fk_persona_distrito1_idx` (`idDistrito`),
  KEY `fk_persona_usuario1_idx` (`idUsuario`),
  KEY `fk_persona_genero1_idx` (`idGenero`),
  KEY `fk_persona_nivelEducativo1_idx` (`idNivelEducativo`),
  KEY `fk_persona_spp1_idx` (`idSpp`),
  KEY `fk_persona_tipoSangre1_idx` (`idTipoSangre`),
  KEY `idTurno` (`idTurno`),
  KEY `fk_persona_aptitud1_idx` (`idAptitud`),
  KEY `fk_persona_quantum1_idx` (`idQuantum`),
  KEY `fk_persona_turno_carta1_idx` (`idTurno_carta`),
  CONSTRAINT `fk_persona_niveleducativo1` FOREIGN KEY (`idNivelEducativo`) REFERENCES `niveleducativo` (`idNivelEducativo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_quantum1` FOREIGN KEY (`idQuantum`) REFERENCES `quantum` (`idQuantum`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_spp1` FOREIGN KEY (`idSpp`) REFERENCES `spp` (`idSpp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_turno1` FOREIGN KEY (`idTurno`) REFERENCES `turno` (`idTurno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_turno_carta1` FOREIGN KEY (`idTurno_carta`) REFERENCES `turno_carta` (`idTurno_carta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`idAptitud`) REFERENCES `aptitud` (`idAptitud`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`idDistrito`) REFERENCES `distrito` (`idDistrito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persona_ibfk_3` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persona_ibfk_4` FOREIGN KEY (`idTipoSangre`) REFERENCES `tipo_sangre` (`idTipoSangre`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persona_ibfk_5` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_ocurrencia`
--

DROP TABLE IF EXISTS `persona_ocurrencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_ocurrencia` (
  `idPersona_ocurrencia` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `idOcurrencia` int(11) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  `tallaBotas` int(11) DEFAULT NULL,
  `tallaUniforme` varchar(2) DEFAULT NULL,
  `telefono` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `apellidoPaterno` varchar(64) DEFAULT NULL,
  `apellidoMaterno` varchar(64) DEFAULT NULL,
  `nombres` varchar(96) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `estadoCivil` varchar(16) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `fechaSpp` date DEFAULT NULL,
  `codigoSpp` varchar(32) DEFAULT NULL,
  `observacion` text,
  `estado` tinyint(4) DEFAULT '0',
  `FechaDeRegistro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaEdicion` datetime DEFAULT NULL,
  `fechaExamenMedicoReal` date DEFAULT NULL,
  `fechaAntecedente` datetime DEFAULT NULL,
  `tipoAntecedente` varchar(120) DEFAULT NULL,
  `idDistrito` int(11) NOT NULL,
  `idGenero` int(11) NOT NULL,
  `idNivelEducativo` int(11) DEFAULT NULL,
  `idSpp` int(11) DEFAULT NULL,
  `estadoAsignacion` tinyint(4) DEFAULT '0',
  `tallaCamisa` varchar(2) DEFAULT NULL,
  `tallaPantalon` int(11) DEFAULT NULL,
  `idTipoSangre` int(11) DEFAULT NULL,
  `idTurno` int(11) DEFAULT NULL,
  `idAptitud` int(11) DEFAULT NULL,
  `detalle_observado` varchar(150) DEFAULT NULL,
  `hijo_conyugue` varchar(3) DEFAULT NULL,
  `fecha_asignacion_antecedente` datetime DEFAULT NULL,
  `idQuantum` int(11) DEFAULT NULL,
  `idTurno_carta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPersona_ocurrencia`),
  KEY `fk_persona_ocurrencia_usuario1_idx` (`idUsuario`),
  KEY `fk_persona_ocurrencia_Ocurrencia1_idx` (`idOcurrencia`),
  KEY `fk_persona_ocurrencia_persona1_idx` (`idPersona`),
  KEY `fk_persona_ocurrencia_distrito1_idx` (`idDistrito`),
  KEY `fk_persona_ocurrencia_genero1_idx` (`idGenero`),
  KEY `fk_persona_ocurrencia_tipo_sangre1_idx` (`idTipoSangre`),
  KEY `fk_persona_ocurrencia_turno1_idx` (`idTurno`),
  KEY `fk_persona_ocurrencia_quantum1_idx` (`idQuantum`),
  KEY `fk_persona_ocurrencia_turno_carta1_idx` (`idTurno_carta`),
  KEY `fk_persona_ocurrencia_spp1_idx` (`idSpp`),
  KEY `fk_persona_ocurrencia_niveleducativo1_idx` (`idNivelEducativo`),
  KEY `fk_persona_ocurrencia_aptitud1_idx` (`idAptitud`),
  CONSTRAINT `fk_persona_ocurrencia_Ocurrencia1` FOREIGN KEY (`idOcurrencia`) REFERENCES `ocurrencia` (`idOcurrencia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_aptitud1` FOREIGN KEY (`idAptitud`) REFERENCES `aptitud` (`idAptitud`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_distrito1` FOREIGN KEY (`idDistrito`) REFERENCES `distrito` (`idDistrito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_genero1` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_niveleducativo1` FOREIGN KEY (`idNivelEducativo`) REFERENCES `niveleducativo` (`idNivelEducativo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_quantum1` FOREIGN KEY (`idQuantum`) REFERENCES `quantum` (`idQuantum`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_spp1` FOREIGN KEY (`idSpp`) REFERENCES `spp` (`idSpp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_tipo_sangre1` FOREIGN KEY (`idTipoSangre`) REFERENCES `tipo_sangre` (`idTipoSangre`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_turno1` FOREIGN KEY (`idTurno`) REFERENCES `turno` (`idTurno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_turno_carta1` FOREIGN KEY (`idTurno_carta`) REFERENCES `turno_carta` (`idTurno_carta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_ocurrencia_usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23905 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `provincia`
--

DROP TABLE IF EXISTS `provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincia` (
  `idProvincia` int(11) NOT NULL,
  `idDepartamento` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idProvincia`),
  KEY `idDepartamento` (`idDepartamento`),
  CONSTRAINT `provincia_ibfk_1` FOREIGN KEY (`idDepartamento`) REFERENCES `departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `puesto`
--

DROP TABLE IF EXISTS `puesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `puesto` (
  `idPuesto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idPuesto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quantum`
--

DROP TABLE IF EXISTS `quantum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quantum` (
  `idQuantum` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`idQuantum`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `referido`
--

DROP TABLE IF EXISTS `referido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `referido` (
  `idReferido` int(11) NOT NULL,
  `observacion` varchar(300) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fechaCreacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idCliente` int(11) NOT NULL,
  `idConsultor` int(11) NOT NULL,
  `idInduccion` int(11) DEFAULT NULL,
  `idEntrevistador` int(11) DEFAULT NULL,
  `idPuesto` int(11) DEFAULT NULL,
  `idResultado` int(11) DEFAULT NULL,
  `fechaEdicion` datetime DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `detalle_resultado` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idReferido`),
  KEY `fk_Referido_cliente1_idx` (`idCliente`),
  KEY `fk_Referido_consultor1_idx` (`idConsultor`),
  KEY `fk_Referido_consultor2_idx` (`idInduccion`),
  KEY `fk_Referido_consultor3_idx` (`idEntrevistador`),
  KEY `fk_Referido_Puesto1_idx` (`idPuesto`),
  KEY `fk_Referido_Resultado1_idx` (`idResultado`),
  KEY `fk_Referido_usuario1_idx` (`idUsuario`) USING BTREE,
  CONSTRAINT `referido_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referido_ibfk_2` FOREIGN KEY (`idConsultor`) REFERENCES `consultor` (`idConsultor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referido_ibfk_3` FOREIGN KEY (`idInduccion`) REFERENCES `consultor` (`idConsultor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referido_ibfk_4` FOREIGN KEY (`idEntrevistador`) REFERENCES `consultor` (`idConsultor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referido_ibfk_5` FOREIGN KEY (`idPuesto`) REFERENCES `puesto` (`idPuesto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referido_ibfk_6` FOREIGN KEY (`idResultado`) REFERENCES `resultado` (`idResultado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `referido_ibfk_7` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `requerimiento`
--

DROP TABLE IF EXISTS `requerimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requerimiento` (
  `idRequerimiento` char(4) NOT NULL,
  `fechaAlta` date NOT NULL,
  `fechaExamen` date DEFAULT NULL,
  `cantidad` int(11) DEFAULT '0',
  `extra` varchar(48) DEFAULT NULL,
  `estado` int(4) NOT NULL DEFAULT '1',
  `idCliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRequerimiento`),
  KEY `fk_requerimiento_cliente1_idx` (`idCliente`),
  CONSTRAINT `requerimiento_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resultado`
--

DROP TABLE IF EXISTS `resultado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resultado` (
  `idResultado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `detalle` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idResultado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spp`
--

DROP TABLE IF EXISTS `spp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spp` (
  `idSpp` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idSpp`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_sangre`
--

DROP TABLE IF EXISTS `tipo_sangre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_sangre` (
  `idTipoSangre` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idTipoSangre`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turno`
--

DROP TABLE IF EXISTS `turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno` (
  `idTurno` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  PRIMARY KEY (`idTurno`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turno_carta`
--

DROP TABLE IF EXISTS `turno_carta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turno_carta` (
  `idTurno_carta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idTurno_carta`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turnos`
--

DROP TABLE IF EXISTS `turnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turnos` (
  `idTurno` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_edicion` datetime DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idTurno`),
  KEY `fk_Turno_usuario1_idx` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(96) NOT NULL,
  `correo` varchar(96) NOT NULL,
  `clave` varchar(32) NOT NULL,
  `admin` char(1) NOT NULL,
  `cargo` varchar(48) NOT NULL,
  `img` varchar(48) NOT NULL DEFAULT '../img/usuario.jpg',
  `idDistrito` int(11) NOT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `correo` (`correo`),
  KEY `fk_usuario_distrito1_idx` (`idDistrito`) USING BTREE,
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idDistrito`) REFERENCES `distrito` (`idDistrito`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=750994882 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'primaxv2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-14 17:47:24
