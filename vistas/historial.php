﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}



$admin = $_SESSION['admin'];

?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Buscar Postulantes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-male"></i> Buscar Postulantes</a></li>
          <li class="active">Buscar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Buscar</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form name="form1">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" name="dni" class="form-control pull-right" id="dni" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="modal-header">
                            <button type="submit" class="btn btn-warning">BUSCAR</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            
            <div class="box" >
              <div class="box-header">
                <h3 class="box-title">Datos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body" value="midiv" id="midiv">
    <?php 
if (!empty($_GET)) {
  $miid = $_GET['dni'];
  $cont = ejecutarQuery("SELECT count(*) as contador from persona where idPersona=$miid");
  $result = mysqli_fetch_assoc($cont);
  $auto = $result['contador'];
  if ($auto==0) {
  ?>
    <h4>No existe </h4>
    <?php 
  }
  else
  {
    $sql = ejecutarQuery("SELECT * from persona where idPersona=$miid");
    $row=mysqli_fetch_assoc($sql);
    $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
    $gener = mysqli_fetch_assoc($gen);
    $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
    $prov = mysqli_fetch_assoc($pro);
    ?>
    <table id="historial" class="table-bordered table-hover">
      <thead>
        <tr>
          <th class="text-center" style="min-width: 60px">EDITAR POSTULANTE</th>
          <th class="text-center" style="min-width: 80px">EDITAR BACK-UP</th>
          <th class="text-center">ESTADO</th>
          <th class="text-center" style="min-width: 80px">DNI</th>
          <th class="text-center" style="min-width: 200px">NOMBRES</th>
          <th class="text-center" style="min-width: 80px">SEXO</th>
          <th class="text-center">PROVINCIA</th>
          <th class="text-center">TEL&Eacute;FONOS </th>
          <th class="text-center" style="min-width: 100px">ELIMINAR ESTACION </th>
        </tr>
      </thead>
      <tbody>
        <tr bgcolor="white">
          <td class="text-center">
            <?php 
            if ($row['estado']==9 && ($admin==0 || $admin==1) ) {
              echo "OPCION DESHABILITADA";
            }
            if ($row['estado']!=9) {
              ?>
              <a href="editarPostulantes.php?id=<?php echo $row['idPersona'];?>">
                <button title="EDITAR" type='button' class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                </button>
              </a>
              <?php 
            }
            else
              if ($row['estado']==9 && $admin==3) {
                ?>
                <a href="editarPostulantes.php?id=<?php echo $row['idPersona'];?>">
                  <button title="EDITAR" type='button' class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                  </button>
                </a>
                <?php
              }
             ?>
          </td>
          <td class="text-center">
            <a href="editarBackup.php?id=<?php echo $row['idPersona'];?>">
              <button title="DISPONIBILIDAD" type='button' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
              </button>
            </a>
          </td>
          <td class="text-center">
          <?php 
            switch ($row['estado']) {
              case 0:
                    echo "BACK UP";
                    break;
              case 1:
                    echo "POSTULANTE ELIMINADO";
                    break;
              case 2:
                    echo "POSTULANTE REGISTRADO";
                    break;
              case 3:
              case 4:
                    echo "ASIGNADO A REQUERIMIENTO";
                    break;
              case 5:
              case 6:
              case 7:
                    echo "SELECCIONADO PARA ANTECEDENTES";
                    break;   
              case 13;
              case 14:
              case 17:
              case 19:
              case 8:
              case 24:
                    echo "SIN ANTECEDENTES";
                    break;         
              case 9:
                    echo "CON ANTECEDENTES: ";
                    echo $row['tipoAntecedente'];
                    break;  
              case 10:
              case 15:
              case 16:
              case 18:
              case 20:
                    echo "EXAMEN MÉDICO APROBADO";
                    break;  
              case 11:
                    echo "EXAMEN MÉDICO DESAPROBADO";
                    break;  
              case 12:
                    echo "TRABAJADOR";
                    break; 
              case 21:
                    echo "EXAMEN MEDICO: NO APTO";  
                    break;
              case 22:
                    echo "PASAR ANTECEDENTES SIN REQUERIMIENTO";  
                    break; 
              case 23:
                    echo "CONFIRMAR ANTECEDENTES SIN REQUERIMIENTO";  
                    break;               
              case 24:
                    echo "SIN ANTECEDENTES (NO ASIGNADO A REQUERIMIENTO)";  
                    break; 
              case 25:
                    echo "SIN ANTECEDENTES (ASIGNADO A REQUERIMIENTO)";  
                    break; 
              case 27:
                    echo "SIN ANTECEDENTES (CONFIRMAR REQUERIMIENTO)";  
                    break; 
              }
          ?>
          </td>
          <td class="text-center"><?php 
          if (strlen($row['idPersona'])==7) {
                              echo '0'.$row['idPersona'];
                            }
                            else
                            {
                              echo $row['idPersona'];
                            }
                         ?></td>
          <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
          <td class="text-center"><?php echo $gener['nombre']; ?></td>
          <td class="text-center"><?php echo $prov['pro']."-".$prov['dis']; ?></td>
          <td class="text-center"><?php echo $row['telefono']; ?></td>
          <td class="text-center">
            <?php 
              if (($admin==3 || $admin==1) && ($row['estado']==3 || $row['estado']==4 || $row['estado']==5 || $row['estado']==6 || $row['estado']==7 || $row['estado']==8 || $row['estado']==10 || $row['estado']==11 || $row['estado']==14 || $row['estado']==16 || $row['estado']==17 || $row['estado']==18 || $row['estado']==19 ||$row['estado']==20) ) {
                ?> 
                    <a href="#desconfirmar<?php echo $row['idPersona'];?>" data-toggle="modal"><button title="REVERTIR" type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span></button></a>
              <?php
            }
           ?>            
          </td>
        </tr>
      </tbody>
    
    </table> 
  <?php 
  } 
}?>
  <div id="desconfirmar<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
       <div class="modal-dialog">
          <form method="post" action="../php/revertirPostulante.php?codigo=<?php echo $row['idPersona'];?>">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ELIMINAR ESTACION</h4>
                </div>
                <div class="modal-body">
                   <p>Esta seguro de eliminar la asignacion de estación del posulante <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres'];?>?</strong></p>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>YES</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                </div>
             </div>
          </form>
       </div>
  </div>
            </div>

            <div class="box-footer">
            </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>