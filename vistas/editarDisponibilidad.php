﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$dni = $_GET['id'];
?>


<?php include('header.php'); ?>
    
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Postulante
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i> Postulante</a></li>
          <li class="active">Editar Disponibilidad</li>
        </ol>
      </section>

      <?php  
        $ok = ejecutarQuery("SELECT * FROM persona where idPersona='$dni'");
        $oks = mysqli_fetch_assoc($ok);
        $bac = ejecutarQuery("SELECT * FROM backup where idBackup='$dni'");
        $back = mysqli_fetch_assoc($bac);
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Editar disponibilidad</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/editarDisponibilidad.php?dni=<?php echo $dni?>" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required value="<?php echo $dni?>" disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Nombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required value="<?php echo $oks['nombres']?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Apellido Paterno</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="paterno" required value="<?php echo $oks['apellidoPaterno']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Disponibilidad</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select class="form-control pull-right" name="disponibilidad">
                            <?php 
                              if ($back['disponibilidad']=='POR DEFINIR') {
                                echo "<option selected='selected'>POR DEFINIR</option>";
                                echo "<option>NO</option>";
                                echo "<option>SI</option>";
                              } else {
                                if ($back['disponibilidad']=='NO') {
                                  echo "<option>POR DEFINIR</option>";
                                  echo "<option selected='selected'>NO</option>";
                                  echo "<option>SI</option>";
                                } else {
                                  echo "<option>POR DEFINIR</option>";
                                  echo "<option>NO</option>";
                                  echo "<option selected='selected'>SI</option>";
                                }
                                
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Apellido Materno</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="materno" required value="<?php echo $oks['apellidoMaterno']?>">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label>Observaciones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase; resize: vertical;min-height: 60px;" name="observacion"="" value="<?php echo $oks['observacion']?>" >
                        </div>
                      </div>
                      <center> <button type="submit" class="btn btn-primary pull-center">EDITAR</button> </center>  
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>

        

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>