<?php
require '../php/funciones.php';

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
  die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("T - Soluciona")
               ->setLastModifiedBy("T - Soluciona")
               ->setTitle("Reporte examen médico")
               ->setSubject("Examen médico")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C4', 'N°')
            ->setCellValue('D4', 'FECHA DE ALTA/INICIO')
            ->setCellValue('E4', 'TALLA DE BOTAS')
            ->setCellValue('F4', 'TALLA DE UNIFORME')
            ->setCellValue('G4', 'TELEFONO')
            ->setCellValue('H4', 'CORREO')
            ->setCellValue('I4', 'CARGO')
            ->setCellValue('J4', 'CONSULTA')
            ->setCellValue('K4', 'EMPRESA')
            ->setCellValue('L4', 'ESTACION')
            ->setCellValue('M4', 'Apellido parterno trabajador')
            ->setCellValue('N4', 'Apellido materno trabajador')
            ->setCellValue('O4', 'Nombres trabajador')
            ->setCellValue('P4', 'Sexo')
            ->setCellValue('Q4', 'Fec.Nac.')
            ->setCellValue('R4', 'Est.Civil')
            ->setCellValue('S4', 'Dirección')
            ->setCellValue('T4', 'Departamento')
            ->setCellValue('U4', 'Provincia')
            ->setCellValue('V4', 'Distrito')
            ->setCellValue('W4', 'Desc.Nivel.Educ')
            ->setCellValue('X4', 'DNI')
            ->setCellValue('Y4', 'Sist.Prev.Pens.')
            ->setCellValue('Z4', 'F.Afil.SPP')
            ->setCellValue('AA4', 'Cód.Unico SPP')
            ->setCellValue('AB4', 'OBSERVACION');


$styleArray = array(
  'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'FFFFFF'),
        'size'  => 8,
        'name'  => 'Calibri'
  ),
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);

$objPHPExcel->getActiveSheet()->getStyle('C4:AB4')->applyFromArray($styleArray);
unset($styleArray);

$styleArray1 = array(
  'font'  => array(
        'color' => array('rgb' => '44546A')
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);

$objPHPExcel->getActiveSheet()->getStyle('C4:F4')->applyFromArray($styleArray1);
unset($styleArray1);

//borde celdas
$styleBorder = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);

//---------------autosize------------
foreach(range('C','Z') as $columnID) { 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) 
     ->setAutoSize(true); 
} 

$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true); 
//---------------END autosize------------

//------ ANCHO DE FILA
$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(33.75);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false); 
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10.71);
//---- END ANCHO FILA

//-------------COLOR DE RELLENO
$objPHPExcel->getActiveSheet()->getStyle('M4:AB4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('3054B4');

$objPHPExcel->getActiveSheet()->getStyle('C4:D4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

$objPHPExcel->getActiveSheet()->getStyle('E4:F4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('B4C6E7');

$objPHPExcel->getActiveSheet()->getStyle('G4:L4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC000');

//-------------END COLOR DE RELLENO

$query = ejecutarQuery("SELECT count(*) as contador FROM persona where idUsuario=77037740");
$result = mysqli_fetch_assoc($query);
$contador=$result['contador']+5;
$i=5;
$query1 = ejecutarQuery("SELECT concat(persona.apellidoPaterno,' ', persona.apellidoMaterno,' ', persona.nombres) as nombres, persona.idPersona as dni, persona.telefono as telefono  FROM persona 
  where idUsuario=77037740");

while($result2=mysqli_fetch_assoc($query1)){
  $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue("C$i", $i-4)
      ->setCellValue("F$i", 'SELECCIÓN  DE PERSONAL')
      ->setCellValue("J$i", $result2['nombres'])
      ->setCellValue("K$i", $result2['dni'])     
      ->setCellValue("R$i", $result2['telefono']);
      $i++;
}

$contador=$contador-1;
$objPHPExcel->getActiveSheet()->getStyle("C5:AB$contador")->applyFromArray($styleBorder);
unset($styleBorder);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Reporte');
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
