﻿<?php
require '../php/funciones.php';
if(!haIniciadoSesion())
{
 header('Location: ../index.php');
}
$inicio=$_GET['inicio'];
$fin=$_GET['fin'];
?>

<?php include('header.php'); ?>
    

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Control de asistencia
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-address-book-o"></i>Reclutamiento</a></li>
          <li class="active">Control de asistencia</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Registros</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="buscarAsistencia" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center" style="min-width: 80px">OPCIONES</th>
                    <th class="text-center">Apellidos y Nombres</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center">Celular/Telefono</th>
                    <th class="text-center">Puesto al que postula</th>
                    <th class="text-center">Empresa a la que postula</th>
                    <th class="text-center">Distrito donde vive</th>
                    <th class="text-center">Fecha Evaluación</th>
                    <th class="text-center">Citado por</th>
                    <th class="text-center">Persona que da inducción</th>
                    <th class="text-center">Psicologo Entrevistador</th>
                    <th class="text-center">Resultados Apto/No A.</th>
                    <th class="text-center">No apto: Motivo</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $consulta1=ejecutarQuery("SELECT * from referido inner join persona on persona.idPersona=referido.idReferido inner join backup on backup.idBackup=persona.idPersona
                       where referido.estado IN (0,1,2) and  backup.fechaEvaluacion between '$inicio' and '$fin'");
                    while($row=mysqli_fetch_assoc($consulta1)){
                      $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                      $prov = mysqli_fetch_assoc($pro);
                      $cl= ejecutarQuery("SELECT nombre from cliente where idCliente='$row[idCliente]'");
                      $cliente = mysqli_fetch_assoc($cl);
                      $ref= ejecutarQuery("SELECT fecha.idFecha as idFecha, turnos.nombre as turno, referido.idConsultor as idConsultor, referido.idInduccion as idInduccion, referido.idEntrevistador as idEntrevistador, puesto.nombre as puesto, resultado.nombre as resultado, referido.detalle_resultado as detalle_resultado from entrevista_referido 
                        inner join referido on entrevista_referido.idReferido=referido.idReferido 
                        inner join entrevista on entrevista.idEntrevista=entrevista_referido.idEntrevista
                        inner join fecha on entrevista.idFecha=fecha.idFecha
                        inner join turnos on entrevista.idTurno=turnos.idTurno
                        inner join puesto on puesto.idPuesto=referido.idPuesto
                        inner join resultado on resultado.idResultado=referido.idResultado
                        where referido.idReferido='$row[idReferido]'");
                      $referido = mysqli_fetch_assoc($ref);
                      $co= ejecutarQuery("SELECT nombre from consultor where idConsultor='$referido[idConsultor]'");
                      $consultor = mysqli_fetch_assoc($co);
                      $ind= ejecutarQuery("SELECT nombre from consultor where idConsultor='$referido[idInduccion]'");
                      $induccion = mysqli_fetch_assoc($ind);
                      $entrev= ejecutarQuery("SELECT nombre from consultor where idConsultor='$referido[idEntrevistador]'");
                      $entrevistador = mysqli_fetch_assoc($entrev);
                  ?>                
                      <tr>
                        <td class="text-center">      
                          <a href="editarReferido.php?id=<?php echo $row['idPersona'];?>">
                            <button type='button' title="EDITAR" class='btn btn-warning btn-circle'>
                              <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                            </button>
                          </a>
                          <a href="#delete<?php echo $row['idPersona'];?>" data-toggle="modal"><button type='button' title="ELIMINAR" class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>
                          </a>
                         
                        </td>
                        <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center"> 
                          <?php 
                            if (strlen($row['idPersona'])==7) {
                              echo '0'.$row['idPersona'];
                            }
                            else
                            {
                              echo $row['idPersona'];
                            }
                          ?>
                        </td>
                        <td class="text-center"><?php echo $row['telefono']; ?></td>
                        <td class="text-center"><?php echo $referido['puesto']; ?></td>
                        <td class="text-center"> 
                          <?php echo strtoupper($cliente['nombre']); ?></td>
                        <td class="text-center"> 
                          <?php echo $prov['dis']; ?>
                        </td>
                        <td class="text-center"> 
                          <?php echo date("d/m/Y", strtotime($referido['idFecha'])); ?></td>
                        <td class="text-center"> 
                          <?php echo $consultor['nombre']; ?>
                        </td>
                        <td class="text-center"> 
                          <?php echo $induccion['nombre']; ?>
                        </td>
                        <td class="text-center"> 
                          <?php echo $entrevistador['nombre']; ?>
                        </td>
                        <td class="text-center"> 
                          <?php echo $referido['resultado']; ?>
                        </td>
                        <td class="text-center"> 
                          <?php echo $referido['detalle_resultado']; ?>
                        </td>
                      </tr>

                      <div id="delete<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarReferido.php?id=<?php echo $row['idPersona'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar a <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idPersona']; ?>,'example','../php/eliminarReferido.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer">
            </div>
          </div>
        </div>
      </div>
          
      <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>