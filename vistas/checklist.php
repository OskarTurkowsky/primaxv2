<?php
require('../fpdf.php');
require '../php/funciones.php';
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario 				 = $_SESSION['id'];

class PDF extends FPDF
{
	// Cabecera de p�gina
	function checkList($postulante, $cargo, $estacion, $examenMedico, $fechaAlta, $validar)
	{
		$this->SetFont('Arial','B',14);
		$this->Cell(85);
	  	$this->Cell(20,15,'CHECK LIST - VENDEDORES DE PLAYA / MINIMERCADO',0,1,'C');
		$this->Ln(1);
		$this->Ln(6);

		$this->SetFont('Arial','',11);
		$this->Cell(7);
		$this->Cell(0,0,'CANDIDATO(A): ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,$postulante,0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'PUESTO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,$cargo,0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'ESTACION DE SERVICIO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,$estacion,0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'FECHA DE INGRESO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,$fechaAlta,0,1,'L');

		$this->Ln(5);
		$this->SetFont('Arial','B',11);
		$this->SetFillColor(230,230,230);
		$this->Cell(4);
		$this->Cell(0,6,"DOCUMENTOS DE LA CONSULTORA ",0,1,'C',true);
		$this->Ln(5);

		$this->SetFont('Arial','',11);
	
		$this->Cell(7);
		$this->Cell(0,0,'01 COPIA DE DNI VIGENTE',0,1,'L');
		$this->Rect(180, 66, 20, 7, '');
		$this->Image('../imagenes/check.png',187,66,6);

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'01 COPIA DE HIJOS (-18 a�os) Y/O CONYUGUE (Partida de Matrimonio)',0,1,'L');
		if ($validar=='SI') {
			$this->Image('../imagenes/check.png',187,73,6);
		}		
		$this->Rect(180, 73, 20, 7, '');
		
		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'01 COPIA DE CARNE DE SANIDAD',0,1,'L');
		$this->Image('../imagenes/check.png',187,80,6);
		$this->Rect(180, 80, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'FICHA DE DATOS PERSONALES',0,1,'L');
		$this->Image('../imagenes/check.png',187,87,6);
		$this->Rect(180, 87, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'ORIGINAL DE CERTIFICADO DE ANTECEDENTES POLICIALES VIGENTE',0,1,'L');
		$this->Image('../imagenes/check.png',187,94,6);
		$this->Rect(180, 94, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'01 COPIA DE SERVICIOS P�BLICOS (Luz, Agua o Tel�fono)',0,1,'L');
		$this->Image('../imagenes/check.png',187,101,6);
		$this->Rect(180, 101, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'DECLARACI�N JURADA DE NO REINGRESO',0,1,'L');
		$this->Image('../imagenes/check.png',187,108,6);
		$this->Rect(180, 108, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CV POSTULANTE',0,1,'L');
		$this->Image('../imagenes/check.png',187,115,6);
		$this->Rect(180, 115, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'COPIA DE CERTIFICADO DE ESTUDIOS / DECLARACION JURADA DE ESTUDIOS',0,1,'L');
		$this->Image('../imagenes/check.png',187,122,6);
		$this->Rect(180, 122, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CARTA DE PRESENTACI�N',0,1,'L');
		$this->Image('../imagenes/check.png',187,129,6);
		$this->Rect(180, 129, 20, 7, '');

		$this->Ln(6);
		$this->SetFont('Arial','B',11);
		$this->SetFillColor(230,230,230);
		$this->Cell(4);
		$this->Cell(0,6,"DOCUMENTOS DE BIENESTAR SOCIAL",0,1,'C',true);
		$this->Ln(6);

		$this->SetFont('Arial','B',11);

		$this->Cell(7);
		$this->Cell(0,0,'IDENTIFICACI�N DE RIESGOS Y RECOMENDACIONES DE SST',0,1,'L');
		$this->Rect(180, 147, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CARGOS DE RECEPCI�N DE REGLAMENTO INTERNO DE TRABAJO',0,1,'L');
		$this->Rect(180, 154, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CARGOS DE RECEPCI�N DE REGLAMENTO INTERNO DE SST',0,1,'L');
		$this->Rect(180, 161, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CARGO DE RECEPCI�N DE BOLET�N INFORMATIVO DEL SISTEMA DE PENSIONES',0,1,'L');
		$this->Rect(180, 168, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CONSTANCIA DE ALTA EN EL T-REGISTRO',0,1,'L');
		$this->Rect(180, 175, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CARGO DE RECEPCI�N DEL BOLET�N INFORMATIVO DE SEGURO SOCIAL Y SALUD',0,1,'L');
		$this->Rect(180, 182, 20, 7, '');

		$this->SetFont('Arial','',11);
		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'INSCRIPCI�N DE DERECHOHABIENTES EN T-REGISTRO',0,1,'L');
		$this->Rect(180, 189, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'DECLARACI�N JURADA DE VERACIDAD',0,1,'L');
		$this->Rect(180, 196, 20,7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'DECLARACI�N JURADA DE DOMICILIO',0,1,'L');
		$this->Rect(180, 203, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'DECLARACI�N JURADA DEL TRABAJADOR-RENTAS DE 5TA. Y 47A. CATEGOR�A',0,1,'L');
		$this->Rect(180, 210, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'DECLARACI�N JURADA DE SISTEMA DE PENSIONES',0,1,'L');
		$this->Rect(180, 217, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'FICHA DE ELECCI�N DEL SISTEMA PENSIONARIO',0,1,'L');
		$this->Rect(180, 224, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'CONFORMIDAD DE RECEPCI�N Y CUMPLIMIENTO DE USO DE EPP',0,1,'L');
		$this->Rect(180, 231, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'AUTORIZACI�N DE EX�MENES M�DICOS PRE-OCUPACIONALES',0,1,'L');
		$this->Rect(180, 238, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'DECLARACI�N DE CONSENTIMIENTO PARA TRATAMIENTO DE DATOS PERSONALES',0,1,'L');
		$this->Rect(180, 245, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'FICHA DE BIENESTAR SOCIAL',0,1,'L');
		$this->Rect(180, 252, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'ACUERDO DE ENVIO DE BOLETA ELECTRONICA',0,1,'L');
		$this->Rect(180, 259, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'PAGO DE UTILIDADES - AUTORIZACI�N',0,1,'L');
		$this->Rect(180, 266, 20, 7, '');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'MOF POR PUESTO',0,1,'L');
		$this->Rect(180, 273, 20, 7, '');

		}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$sql = ejecutarQuery("SELECT distinct * from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
		inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona
		inner join distrito on persona.idDistrito=distrito.idDistrito
        inner join provincia on provincia.idProvincia=distrito.idProvincia
        inner join departamento on departamento.idDepartamento=provincia.idDepartamento
		where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento group by persona.idPersona");
		while($fila=mysqli_fetch_assoc($sql)){ 
			$sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
				inner join provincia on provincia.idProvincia=distrito.idProvincia
				inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
			$usuarioDept=mysqli_fetch_array($sqqqq);

			$sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
			$filaasa=mysqli_fetch_array($sq);

			if (
			 ($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
				($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
				($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
				 ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
				 ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
				 (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759 || $usuario== 15359362 ) && 
				 ( $fila['idDepartamento'] == 12  || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 ||  $fila['idEstacion'] == 39 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==  209 || $fila['idEstacion'] ==  303 || $fila['idEstacion'] ==302 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256  || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146  || $fila['idEstacion'] == 44) ) ||
				 (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874 || $usuario==70275330) && 
				 ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] == 245) ) || 
				 ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
				 ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
				 ($filaasa['idDepartamento']==$usuarioDept['idDepartamento']) 
			) {
				$postulante=utf8_decode($fila['nombres'].' '.$fila['apellidoPaterno'].' '.$fila['apellidoMaterno']);
				$consulta = ejecutarQuery("SELECT nombre from cargo where idcargo=$fila[idcargo]");
				$getCargo = mysqli_fetch_assoc($consulta);
				$cargo=$getCargo['nombre'];
				$consulta1 = ejecutarQuery("SELECT nombre from estacion where idEstacion=$fila[idEstacion]");
				$getdireccion = mysqli_fetch_assoc($consulta1);
				$estacion=utf8_decode($getdireccion['nombre']);
				$examenMedico=date("d/m/Y", strtotime($fila['fechaExamen']));
				$fechaAlta=date("d/m/Y", strtotime($fila['fechaAlta']));
				$validar=$fila['hijo_conyugue'];
				$pdf->AddPage();
				$pdf->checkList($postulante, $cargo, $estacion, $examenMedico, $fechaAlta, $validar);
			}
		}
	$pdf->Output();
?>
