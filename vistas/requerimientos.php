<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Requerimientos
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i> Requerimientos</a></li>
          <li class="active">Registrar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <?php 
              if ($_SESSION['admin'] == '1'){
             ?>
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Datos Generales</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form method="POST" action="../php/nuevorequerimiento.php">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Cliente</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                          </div>
                          <select class="form-control pull-right" name="cliente" id="cliente">
                            <option>Elige una opción</option>
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM cliente order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idCliente']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Fecha de Alta</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="date" class="form-control pull-right" name="fechaAlta" required="">
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Extra</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-book"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="extra">
                        </div>
                      </div>
                      <!--<div class="form-group">
                        <label>Fecha de Examen Médico</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaExamenMedico" >
                        </div>
                      </div> -->
                    </div>                 
                    <div class="col-md-4">
                      <center>
                        <br>
                        <br>
                        <button type="submit" class='btn btn-primary btn-md'>GENERAR
                        </button>
                      </center>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
          <?php } ?>
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Requerimientos registrados &nbsp;&nbsp;&nbsp;</h3>
                <?php 
                          if ($_SESSION['admin'] == '3'){
                            ?>
                            <select id="mostrar">
                              <option value="activos">Activos</option>
                              <option value="todos">Todos</option>
                            </select>
                            <?php
                          }
                        ?>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
            <div class="box-body" id="notall" >
              <table id="exampleReq" class="table-bordered table-hover">
                <thead>
                    <tr>
                      <th></th>
                      <th class="text-center" style="min-width: 320px">OPCIONES
                      </th>
                      <th class="text-center" style="min-width: 150px">NOMBRE</th> 
                      <th class="text-center">FECHA ALTA</th>
                      <th class="text-center">FECHA EXAMEN</th>
                      <th class="text-center" style="min-width: 70px">EXTRA</th>
                      <th class="text-center">ESTADO</th>  
                      <th class="text-center">CANTIDAD</th>
                      <th class="text-center">CLIENTE</th>     
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                    if ($_SESSION['admin'] == '0' || $_SESSION['admin'] == '2' ){
                      $rs=ejecutarQuery("SELECT * FROM requerimiento where estado = 1 ");
                    }
                    else
                    {
                      $rs=ejecutarQuery("SELECT * FROM requerimiento where estado IN (1) ");
                    }
                    $contador=0;                     
                    while($row=mysqli_fetch_assoc($rs)){
                      $consulta = ejecutarQuery("SELECT * FROM cliente WHERE idCliente=$row[idCliente]");
                      $aa = mysqli_fetch_assoc($consulta);
                      $consulta1 = ejecutarQuery("SELECT count(fechaAlta) as contador, fechaAlta from requerimiento where fechaAlta='$row[fechaAlta]'");
                      $bb = mysqli_fetch_assoc($consulta1);
                    ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center">
                           <a href="detalleRequerimientos.php?id=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-primary" type="button" title="DETALLE"><i class="fa fa-folder-open" aria-hidden="true"></i>
                              </button>
                           </a>
                           <a href="estadoRequerimientos.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                <button class="btn btn-link btn-circle" type="button" title="STATUS REQ"><span class='fa fa-hourglass-half' aria-hidden='true'></span></i>
                                  </button>
                            </a>
                            <a href="listarRequerimiento.php?idReq=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE"><span class='fa fa-file-excel-o' aria-hidden='true'></span></i>
                              </button>
                            </a>
                           <?php 
                            if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3'){
                           ?>       
                            <a href="requerimientosAnulados.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                <button class="btn btn-dark btn-circle" type="button" title="REQ ANULADOS"><span class='fa fa-minus-circle' aria-hidden='true'></span></i>
                                  </button>
                            </a>
                          <?php
                            if ($row['estado']==1) {
                               ?>                                
                                <a href="listarDetalleRequerimiento.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                  <button class="btn btn-info btn-circle" type="button" title="LISTAR"><span class='fa fa-book' aria-hidden='true'></span></i>
                                  </button>
                                </a>
                                <a href="listarRequerimientoNoAsignado.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                    <button class="btn btn-default btn-circle" type="button" title="REPORTE SIN ASIGNAR"><span class='fa fa-archive' aria-hidden='true'></span></i>
                                      </button>
                                </a>
                                <a href="#cerrar<?php echo $row['idRequerimiento'];?>" data-toggle="modal"">
                                <button class="btn btn-warning" type="button" title="CERRAR">
                                  <span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span>
                                </button>
                                </a>
                               <?php
                            }                            
                          ?> 
                          <?php
                            if ($row['estado']==2 && $_SESSION['admin'] == '3') {
                               ?>                                
                                <a href="#consumar<?php echo $row['idRequerimiento'];?>" data-toggle="modal">
                                  <button class="btn btn-warning btn-circle"  type="button" title="CONSUMAR"><span class='fa fa-low-vision' aria-hidden='true'></span></i>
                                  </button>
                                </a>
                               <?php
                            }                            
                          ?>
                          <a href="#delete<?php echo $row['idRequerimiento'];?>" data-toggle="modal">
                              <button type='button'title="ELIMINAR" class='btn btn-danger btn-circle'>
                                <span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
                              </button>
                          </a>  
                        <?php } ?>
                        </td>
                        <?php 
                        if ($bb['fechaAlta']==$row['fechaAlta']) {
                           $contador++;
                        }
                        ?>
                        <td class="text-center"> <?php echo "REQUERIMIENTO ".date("d/m", strtotime($row['fechaAlta'])).' '.$aa['nombre'].' '.$contador?></td>
                        <?php 
                          if ($contador==$bb['contador']) {
                              $contador=0;
                            }
                         ?>
                        <td class="text-center"><?php echo date("d/m/Y", strtotime($row['fechaAlta'])); ?></td>
                        <td style="text-align: center"><?php 
                          if (date("d/m/Y", strtotime($row['fechaExamen']))=='30/11/-0001' || date("d/m/Y", strtotime($row['fechaExamen']))=='31/12/1969') {
                              echo "-";
                            }
                          else
                          {
                          echo date("d/m/Y", strtotime($row['fechaExamen']));
                          } ?>
                        </td>
                        <td class="text-center"><?php echo $row['extra']; ?></td>
                        <td style="text-align: center">
                          <?php 
                            if ($row['estado']==1){ echo '<a class="btn btn-sm  btn-success ">ACTIVO </a>';}
                            if ($row['estado']==2){ echo '<a class="btn btn-sm  btn-primary ">CULMINADO </a>';}
                          ?>
                        </td>  
                        <td class="text-center"><?php echo $row['cantidad']; ?></td>
                        <td class="text-center"> <?php echo $aa['nombre']; 
                        ?></td>
                        
                      </tr>
                        <!--Eliminar Modal -->
                      <div id="delete<?php echo $row['idRequerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarRequerimiento.php?idReq=<?php echo $row['idRequerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div id="cerrar<?php echo $row['idRequerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/cerrarRequerimiento.php?id=<?php echo $row['idRequerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CERRAR REQUERIMIENTO</h4>
                              </div>
                              <div class="modal-body">
                                <p>¿Está seguro de cerrar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-warning"> <span class="glyphicon glyphicon-ban-circle"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div id="consumar<?php echo $row['idRequerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/consumarRequerimiento.php?id=<?php echo $row['idRequerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CONSUMAR REQUERIMIENTO</h4>
                              </div>
                              <div class="modal-body">
                                <p>¿Está seguro de consumar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-warning"> <span class="glyphicon glyphicon-ban-circle"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    <?php
                      }
                    ?>
                </tbody>
                </table> 
            </div>

            <div class="box-body" id="all" hidden >
              <table id="exampleReq" class="table-bordered table-hover">
                <thead>
                    <tr>
                      <th></th>
                      <th class="text-center" style="min-width: 160px">OPCIONES
                      </th>
                      <th class="text-center" style="min-width: 150px">NOMBRE</th> 
                      <th class="text-center">FECHA ALTA</th>
                      <th class="text-center">FECHA EXAMEN</th>
                      <th class="text-center" style="min-width: 70px">EXTRA</th>
                      <th class="text-center">ESTADO</th>  
                      <th class="text-center">CANTIDAD</th>
                      <th class="text-center">CLIENTE</th>                 
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                    if ($_SESSION['admin'] == '0' || $_SESSION['admin'] == '2' ){
                      $rs=ejecutarQuery("SELECT * FROM requerimiento where estado = 1");
                    }
                    else
                    {
                      $rs=ejecutarQuery("SELECT * FROM requerimiento where estado IN (1,2,3)");
                    }                     
                    while($row=mysqli_fetch_assoc($rs)){
                      $consulta = ejecutarQuery("SELECT * FROM cliente WHERE idCliente=$row[idCliente]");
                      $aa = mysqli_fetch_assoc($consulta);
                      $consulta1 = ejecutarQuery("SELECT count(fechaAlta) as contador, fechaAlta from requerimiento where fechaAlta='$row[fechaAlta]'");
                      $bb = mysqli_fetch_assoc($consulta1);
                    ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center" style="min-width: 160px">
                           <a href="detalleRequerimientos.php?id=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-primary" type="button" title="DETALLE"><i class="fa fa-folder-open" aria-hidden="true"></i>
                              </button>
                           </a>
                           <?php 
                            if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3'){
                           ?>                             
                            <a href="listarRequerimiento.php?idReq=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE"><span class='fa fa-file-excel-o' aria-hidden='true'></span></i>
                              </button>
                            </a>
                            <a href="estadoRequerimientos.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                <button class="btn btn-link btn-circle" type="button" title="STATUS REQ"><span class='fa fa-hourglass-half' aria-hidden='true'></span></i>
                                  </button>
                            </a>
                            <a href="requerimientosAnulados.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                <button class="btn btn-dark btn-circle" type="button" title="REQ ANULADOS"><span class='fa fa-minus-circle' aria-hidden='true'></span></i>
                                  </button>
                            </a>
                          <?php
                            if ($row['estado']==1) {
                               ?>                                
                                <a href="listarDetalleRequerimiento.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                  <button class="btn btn-info btn-circle" type="button" title="LISTAR"><span class='fa fa-book' aria-hidden='true'></span></i>
                                  </button>
                                </a>
                                <a href="listarRequerimientoNoAsignado.php?idReq=<?php echo $row['idRequerimiento'];?>">
                                    <button class="btn btn-default btn-circle" type="button" title="REPORTE SIN ASIGNAR"><span class='fa fa-archive' aria-hidden='true'></span></i>
                                      </button>
                                </a>
                                <a href="#cerrar<?php echo $row['idRequerimiento'];?>" data-toggle="modal"">
                                <button class="btn btn-warning" type="button" title="CERRAR">
                                  <span class='glyphicon glyphicon-ban-circle' aria-hidden='true'></span>
                                </button>
                                </a>
                               <?php
                            }                            
                          ?> 
                          <?php
                            if ($row['estado']==2 && $_SESSION['admin'] == '3') {
                               ?>                                
                                <a href="#consumar<?php echo $row['idRequerimiento'];?>" data-toggle="modal">
                                  <button class="btn btn-warning btn-circle"  type="button" title="CONSUMAR"><span class='fa fa-low-vision' aria-hidden='true'></span></i>
                                  </button>
                                </a>
                               <?php
                            }                            
                          ?>
                          <a href="#delete<?php echo $row['idRequerimiento'];?>" data-toggle="modal">
                              <button type='button'title="ELIMINAR" class='btn btn-danger btn-circle'>
                                <span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
                              </button>
                          </a>  
                        <?php } ?>
                        </td>
                        <?php 
                        if ($bb['fechaAlta']==$row['fechaAlta']) {
                           $contador++;
                        }
                        ?>
                        <td class="text-center"> <?php echo "REQUERIMIENTO ".date("d/m", strtotime($row['fechaAlta'])).' '.$aa['nombre'].' '.$contador?></td>
                        <?php 
                          if ($contador==$bb['contador']) {
                              $contador=0;
                            }
                         ?>
                        <td class="text-center"><?php echo date("d/m/Y", strtotime($row['fechaAlta'])); ?></td>
                        <td style="text-align: center"><?php 
                          if (date("d/m/Y", strtotime($row['fechaExamen']))=='31/12/1969') {
                              echo "-";
                            }
                          else
                          {
                          echo date("d/m/Y", strtotime($row['fechaExamen']));
                          } ?>
                        </td>
                        <td class="text-center"><?php echo $row['extra']; ?></td>
                        <td style="text-align: center">
                          <?php 
                            if ($row['estado']==1){ echo '<a class="btn btn-sm  btn-success ">ACTIVO </a>';}
                            if ($row['estado']==2){ echo '<a class="btn btn-sm  btn-primary ">CULMINADO </a>';}
                          ?>
                        </td>  
                        <td class="text-center"><?php echo $row['cantidad']; ?></td>
                        <td class="text-center"> <?php echo $aa['nombre']; 
                        ?></td>
                        
                      </tr>
                        <!--Eliminar Modal -->
                      <div id="delete<?php echo $row['idRequerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarRequerimiento.php?idReq=<?php echo $row['idRequerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div id="cerrar<?php echo $row['idRequerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/cerrarRequerimiento.php?id=<?php echo $row['idRequerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CERRAR REQUERIMIENTO</h4>
                              </div>
                              <div class="modal-body">
                                <p>¿Está seguro de cerrar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-warning"> <span class="glyphicon glyphicon-ban-circle"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div id="consumar<?php echo $row['idRequerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/consumarRequerimiento.php?id=<?php echo $row['idRequerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CONSUMAR REQUERIMIENTO</h4>
                              </div>
                              <div class="modal-body">
                                <p>¿Está seguro de consumar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-warning"> <span class="glyphicon glyphicon-ban-circle"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    <?php
                      }
                    ?>
                </tbody>
                </table>            
            </div>

              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>