﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reporte de Antecedentes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-gavel"></i> Reporte Antecedentes</a></li>
        </ol>
      </section>
      <section class="content">

        <div class="row">
            <div class="col-xs-12">
              <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Tabla de Antecedentes</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <table id="soloexport" class="table-bordered table-hover">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 120px">ESTADO</th>
                      <th class="text-center" style="width: 80px">DNI</th>
                      <th class="text-center" style="width: 280px">NOMBRES</th>
                      <th class="text-center" style="width: 200px">DISTRITO</th>
                      <th class="text-center" style="width: 200px">RECLUTADOR</th>
                      <th class="text-center" style="width: 200px">USUARIO</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT * from persona where idAptitud=2");   
                      while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, distrito.idDistrito as idDistrito from distrito WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $us=ejecutarQuery("SELECT usuario.nombres as usuario, usuario.idUsuario as idUsuario from usuario WHERE usuario.idUsuario = '$row[idUsuario]' ");
                        $usuario = mysqli_fetch_assoc($us);

                        $reclut=ejecutarQuery("SELECT consultor.nombre as consultor, consultor.idConsultor as idConsultor from backup inner join persona on backup.idBackup=persona.idPersona inner join consultor on consultor.idConsultor=backup.idConsultor WHERE persona.idPersona = '$row[idPersona]' ");
                        $reclutador = mysqli_fetch_assoc($reclut);
                    ?>                  
                        <tr bgcolor="white">
                          <td class="text-center"  style="width: 120px"> 
                            <?php echo "NO APTO"?>                            
                          </td>
                          <td class="text-center" style="width: 80px">
                            <?php echo $row['idPersona']; ?></td>
                          <td class="text-center" style="width: 280px">
                            <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                          <td class="text-center" style="width: 200px" >
                            <?php echo $prov['dis']; ?> </td>                            
                          <td class="text-center" style="width: 200px" >
                          <?php echo strtoupper($reclutador['consultor']); ?> </td>
                          <td class="text-center" style="width: 200px" >
                          <?php echo strtoupper($usuario['usuario']); ?> </td>
                        </tr>                        
                    <?php
                      }
                    ?>                  
                  </tbody>                
                </table> 
              </div>
            </div>
        </div>
        <!-- /.row -->
      </section>
    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>