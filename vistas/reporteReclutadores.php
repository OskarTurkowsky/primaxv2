﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$admin=$_SESSION['admin'];
$idUsuario=$_SESSION['id'];
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Reporte de Reclutadores
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-files-o"></i>Reporte de reclutadores</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Tipo de reporte</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <h4><input type="radio" name="reporte" value="distrito">  Departamento</h4> 
                <h4><input type="radio" name="reporte" value="reclutador">  Reclutador</h4>
                <h4><input type="radio" name="reporte" value="fecha">  Rango de Fecha</h4> 
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>

        <div class="row" id="reporteDistrito" style="display:none">
          <div class="col-xs-12">
            <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Departamento</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <form method="POST" action="../php/reporteDepartamento.php">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Seleccionar Departamento:</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="departamento" class="form-control pull-right">
                            <?php 
                            $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                            $abcc=mysqli_fetch_assoc($aa);
                            //if ($admin==2) {
                            //  if ($abcc['idDistrito']==150101) {
                            //    $consulta = ejecutarQuery("SELECT departamento.* from departamento where nombre IN ('CALLAO', 'LIMA')");
                            //  }
                            //  if ($abcc['idDistrito']==140101) {
                            //    $consulta = ejecutarQuery("SELECT departamento.* from departamento where nombre IN ('ICA', 'LAMBAYEQUE')");
                            //  }
                            //  else
                            //  {
                            //  $consulta = ejecutarQuery("SELECT departamento.* from departamento inner join provincia on provincia.idDepartamento=departamento.idDepartamento inner join distrito on distrito.idProvincia=provincia.idProvincia inner join usuario on usuario.idDistrito=distrito.idDistrito where usuario.idDistrito=$abcc[idDistrito] group by departamento.nombre") ;}
                            //}
                            //else
                            //{
                            $consulta = ejecutarQuery("SELECT * from departamento order by nombre");
                            //}
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <br>
                        <button type="submit" class='btn btn-primary btn-md'>GENERAR
                        </button>
                    </div>   
                  </div>
                </form>
              </div>
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="reporteReclutador" style="display:none">
          <div class="col-xs-12">
            <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Reclutador</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <form method="POST" action="../php/reporteReclutador.php">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Seleccionar Reclutador:</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <input style="width:200px; height:30px" list="listado" id="answerInput">
                          <datalist id="listado" >
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM consultor order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION data-value="<?php echo $eee['idConsultor']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </datalist>
                          <input type="hidden" name="citado" id="answerInput-hidden">
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <br>
                        <button type="submit" class='btn btn-primary btn-md'>GENERAR
                        </button>
                    </div>   
                  </div>
                </form>
              </div>
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="rangoFecha" style="display:none">
          <div class="col-xs-12">
            <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Rango de Fecha</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <form method="POST" action="../php/fechaReclutador.php">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Fecha de Reporte</label>
                        <br>
                        Desde: 
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="date" class="form-control pull-right" name="fechaInicio" required>
                        </div>
                        <br>
                        Hasta: 
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="date" class="form-control pull-right" name="fechaFin" required>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <br> <br> <br> <br>
                        <button type="submit" class='btn btn-primary btn-md'>GENERAR
                        </button>
                    </div>   
                  </div>
                </form>
              </div>
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>