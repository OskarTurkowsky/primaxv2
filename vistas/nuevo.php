<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

?>

<?php include('header.php'); ?>

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Back Up
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-address-book-o"></i> Back-Up</a></li>
          <li class="active">Registro</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Formulario de Registro</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/nuevobackup.php" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required="" id="dni" maxlength="8">
                        </div>
                        <div id="verificar" hidden="">
                          <span class="label label-danger">
                            <label id="verificarDni"></label>
                          </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Paterno</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="paterno" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Materno</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="materno" required=""">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Nombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required="" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Sexo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-venus-mars"></i>
                          </div>
                          <select class="form-control pull-right" name="sexo">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM genero order by idGenero LIMIT 2");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idGenero']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>   
                      </div> 
                      <div class="form-group">
                        <label>Tel&eacute;fono 1</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono1" class="form-control pull-right" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Tel&eacute;fono 2</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono2" class="form-control pull-right">
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Tel&eacute;fono 3</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono3" class="form-control pull-right">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Departamento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="departamento" id="" onchange="from(document.form1.departamento.value,'midiv','../php/provincias.php');">
                            <option>Elige una opción</option>
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Provincia</label>
                        <div class="input-group" id="midiv">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="provincia" id="provincias">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Distrito</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="distrito" id="distritos">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Citado por</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="citado" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM consultor where estado=1  order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idConsultor']; ?>"><?php echo $eee['nombre']; ?></OPsTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha de Evaluacion</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaEvaluacion" required="">
                        </div>
                      </div>  
                      <div class="form-group">
                        <label>Antecedentes Policiales</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-address-card-o"></i>
                          </div>
                          <select class="form-control pull-right" name="policial">
                            <option>NO</option>
                            <option>SI</option>
                          </select>
                        </div>   
                      </div>
                    </div>
                    <div class="col-md-4">
                      
                      <div class="form-group">
                        <label>Carnet de Sanidad</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-address-card-o"></i>
                          </div>
                          <select class="form-control pull-right" name="sanidad">
                            <option>NO</option>
                            <option>SI</option>
                          </select>
                        </div>   
                      </div>
                      <div class="form-group">
                        <label>Disponibilidad</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select class="form-control pull-right" name="disponibilidad">
                            <option>POR DEFINIR</option>
                            <option>NO</option>
                            <option>SI</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Sistema Privado de Pensiones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-check"></i>
                          </div>
                          <select name="spp" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM spp order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idSpp']; ?>"><?php echo $eee['nombre']; ?></OPsTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha SPP</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaSpp">
                        </div>   
                      </div>
                      <div class="form-group">
                        <label>Codigo SPP</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-barcode"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="codigoSpp" style="text-transform:uppercase;">
                        </div>   
                      </div>
                      <div class="form-group">
                        <label>Observaciones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <textarea class="form-control pull-right" rows="1" style="resize: vertical;min-height: 60px; text-transform:uppercase" name="observaciones">
                          </textarea>
                        </div>
                      </div>
                      <center> <button type="submit" class="btn btn-primary pull-center">REGISTRAR</button> </center> 
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>          
        <!-- /.row -->

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){  
      var consulta;
      $("#dni").focus();
      $("#dni").keyup(function(e){
        consulta = $("#dni").val();
        if (consulta.length==8) {
          $("#verificarDni").delay(0).queue(function(n) {
            $("#verificarDni").html('');
              $.ajax({
                      type: "POST",
                      url: "verificarDni.php",
                      data: "dni="+consulta,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        if (data.trim()=='') {
                          $("#verificar").hide();
                          n();
                        }
                        else
                        {
                          $("#verificar").show();
                          $("#verificarDni").html(data);
                          n();
                        }
                      }
              });
          });  
        }
        if (consulta.length>8) {
          $("#verificar").show();
          $("#verificarDni").html("DNI no válido");
          n();
        }  
        else
          $("#verificar").hide();                 
      });
  });  
</script>