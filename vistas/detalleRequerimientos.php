﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$idRequerimiento = $_GET['id'];
$admin=$_SESSION['admin'];

$consulta = ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento=$idRequerimiento");
$aa = mysqli_fetch_assoc($consulta);
?>

<?php include('header.php'); ?>

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Requerimientos
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i> Requerimientos</a></li>
          <li class="active">Detalle de Requerimientos</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <?php if ($admin==1) {?>          
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Formulario de Requerimientos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/nuevoDetalleRequerimiento.php?idRequerimiento=<?php echo $idRequerimiento?>" method="POST" enctype="multipart/form-data" name="form1">
                     <div class="col-md-4"> 
                      <?php 
                      //PRIMAX
                        if ($aa['idCliente']==2055454574) {
                          ?>
                          <div class="form-group">
                            <label>Departamento</label>
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-globe"></i>
                              </div>
                              <select class="form-control pull-right" name="departamento" id="departamento" onchange="from(document.form1.departamento.value,'midiv3','../php/estaciones.php');" >
                                <option>Elige una opción</option>
                                <?php 
                                $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                                while($eee=mysqli_fetch_assoc($consulta)){
                                ?>
                                  <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                                <?php
                                }
                                ?> 
                              </select>
                            </div>
                          </div>
                          <?php 
                        }
                        if ($aa['idCliente']==2025903307) {
                          ?>
                          <div class="form-group">
                            <label>Departamento</label>
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-globe"></i>
                              </div>
                              <select class="form-control pull-right" name="departamento" id="departamento" onchange="from(document.form1.departamento.value,'midiv3','../php/estacioness.php');" >
                                <option>Elige una opción</option>
                                <?php 
                                $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                                while($eee=mysqli_fetch_assoc($consulta)){
                                ?>
                                  <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                                <?php
                                }
                                ?> 
                              </select>
                            </div>
                          </div>
                          <?php 
                        }
                       ?>                         
                      <div class="form-group">
                        <label>Cargo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-check"></i>
                          </div>
                          <select name="cargo" class="form-control pull-right" >
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM cargo order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idcargo']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>                         
                      <div  class="form-group">
                        <label>Cantidad Hombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-mars"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="cantMasculino" value="0">
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">   
                     <!-- <div class="form-group">
                          <label>Localidad</label>
                          <div class="input-group" id="midiv5" >  
                            <div class="input-group-addon">
                              <i class="fa fa-hospital-o"></i>
                            </div>
                          <select class="form-control pull-right" name="localidad" id="localidad" >
                          </select>    
                          </div>                                                    
                        </div> -->    
                      <div class="form-group">
                        <label>Turno</label>
                        <div class="input-group">  
                          <div class="input-group-addon">
                            <i class="fa fa-hospital-o"></i>
                          </div>                    
                        <select name="turno" class="form-control pull-right" >
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM turno_carta order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idTurno_carta']; ?>" <?php if ($eee['idTurno_carta']==2) {
                                echo "selected='selected'";
                              } 
                               ?> ><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>   
                        </div>                                                    
                      </div>                    
                      <div class="form-group">
                        <label>Cantidad Mujeres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-venus"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="cantFemenino" value="0">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Tipo Contrato</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-file-text-o"></i>
                          </div>
                          <select name="contrato" class="form-control pull-right" >
                            <OPTION VALUE="0">NINGUNO</OPTION> 
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM contrato");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idContrato']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select> 
                        </div>
                      </div>        
                    </div>   
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Estacion</label>
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <input style="width:270px; height:30px" list="listado" id="estaciones" > 
                        </div>
                        <div class="input-group" id="midiv3">                          
                          <datalist id="listado">
                          </datalist>
                          <input type="hidden"  name="estacion" id="estaciones-hidden" >
                        </div>
                      </div> 
                       <div class="form-group">
                        <label>Cantidad Indistinto</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-users"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="cantIndistinto" value="0">
                        </div>
                      </div>
                      <br> 
                      <center> <button type="submit" class="btn btn-primary pull-center">REGISTRAR</button> </center>    
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>
        <?php } ?>

        <div class="row" id="tabla">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Requerimientos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                    <th ></th>
                    <th style="min-width: 110px" class="text-center">OPCIONES</th>
                    <th class="text-center">CLIENTE</th>
                    <th class="text-center">CARGO</th>
                    <th class="text-center">DEPARTAMENTO</th>
                    <th class="text-center">ESTACIÓN</th>
                    <th class="text-center">TURNO</th>
                    <th class="text-center">GENERO</th>
                    <th class="text-center">ESTADO</th>
                    <th class="text-center">CONTRATO</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                  if ($admin==1 || $admin==3) {   
                    $rs=ejecutarQuery("SELECT * FROM detalle_requerimiento where idRequerimiento='$idRequerimiento' and estado!=3 "); }
                  else {
                    $rs=ejecutarQuery("SELECT * FROM detalle_requerimiento where idRequerimiento='$idRequerimiento' and estado!=3 and estado !=4 ");
                  }     
                    while($row=mysqli_fetch_assoc($rs)){
                      $car=ejecutarQuery("SELECT * FROM cargo inner join detalle_requerimiento on  detalle_requerimiento.idcargo=cargo.idcargo where detalle_requerimiento.idcargo='$row[idcargo]' ");
                      $carg = mysqli_fetch_assoc($car);
                      $es=ejecutarQuery("SELECT * FROM estacion INNER JOIN detalle_requerimiento on estacion.idEstacion=detalle_requerimiento.idEstacion where estacion.idEstacion=$row[idEstacion] ");
                      $est = mysqli_fetch_assoc($es);
                      $dep=ejecutarQuery("SELECT departamento.nombre as nom FROM departamento INNER JOIN estacion ON departamento.idDepartamento=estacion.idDepartamento WHERE estacion.idEstacion= $row[idEstacion]");
                      $dept = mysqli_fetch_assoc($dep);
                      $gen=ejecutarQuery("SELECT genero.nombre as nomb FROM genero INNER JOIN detalle_requerimiento ON genero.idGenero=detalle_requerimiento.idGenero WHERE detalle_requerimiento.idGenero=$row[idGenero]");
                      $genr = mysqli_fetch_assoc($gen);
                      $clien=ejecutarQuery("SELECT cliente.nombre as cliente FROM cliente INNER JOIN requerimiento ON cliente.idCliente=requerimiento.idCliente WHERE requerimiento.idCliente=$aa[idCliente]");
                      $cliente = mysqli_fetch_assoc($clien);
                      if($row['idTurno_carta']!=null){
                        $turn=ejecutarQuery("SELECT turno_carta.nombre as turno FROM turno_carta where idTurno_carta=$row[idTurno_carta]");
                        $turno = mysqli_fetch_assoc($turn);
                      }
                      if($row['idContrato']!=null){
                        $cont=ejecutarQuery("SELECT contrato.nombre as contrato FROM contrato where idContrato=$row[idContrato]");
                        $contrato = mysqli_fetch_assoc($cont);
                      }
                      
                   // if ($aa['idCliente']==2055454574) {
                     //   $cli=ejecutarQuery("SELECT * FROM clinica INNER JOIN detalle_requerimiento ON clinica.idClinica=detalle_requerimiento.idClinica WHERE detalle_requerimiento.idClinica=$row[idClinica]");
                       // $clini = mysqli_fetch_assoc($cli);
                        
                     // }
                     // if ($row['idLocalidad']!=null) {
                       // $loca=ejecutarQuery("SELECT * FROM localidad inner JOIN detalle_requerimiento ON localidad.idLocalidad=detalle_requerimiento.idLocalidad WHERE detalle_requerimiento.idLocalidad=$row[idLocalidad]");
                       // $localidad = mysqli_fetch_assoc($loca);
                     // }                      
                  ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center">
                          <?php 
                          if($row['estado']!=2 && $row['estado']!=4){
                            ?>  
                            <a href="buscarDetalleRequerimiento.php?idDet=<?php echo $row['idDetalle_requerimiento'];?>&idReq=<?php echo $idRequerimiento;?>">
                              <button class="btn btn-primary btn-circle" type="button" title="BUSCAR"><span class='fa fa-search' aria-hidden='true'></span></i>
                              </button>
                            </a> 
                            <?php                    
                          }
                          if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3'){
                            if ($row['estado']!=4) {
                           ?>      
                           <a href="#anular<?php echo $row['idDetalle_requerimiento'];echo $idRequerimiento?>" data-toggle="modal">
                              <button class="btn btn-secondary btn-circle" type="button" title="ANULAR"><span class='fa fa-times' aria-hidden='true'></span></i>
                              </button>
                            </a> 
                            <?php  }?>  
                           <a href="#delete<?php echo $row['idDetalle_requerimiento'];echo $idRequerimiento?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></a>  
                          <?php  }?>  
                        </td>
                        <td class="text-center"><?php echo $cliente['cliente']; ?></td>
                        <td class="text-center"><?php echo $carg['nombre']; ?></td>
                        <td class="text-center"><?php echo $dept['nom']; ?></td>
                        <td class="text-center"><?php echo $est['nombre']; ?></td> 
                        <td class="text-center"><?php if($turno['turno']==null){echo '-';} else { echo $turno['turno'];} ?></td> 
                        <td class="text-center"><?php echo $genr['nomb']; ?></td>
                        <td style="text-align: center">
                          <?php 
                            if ($row['estado']==0){ echo '<a class="btn btn-warning">SIN ASIGNAR </a>';}
                            if ($row['estado']==1){ echo '<a class="btn btn-success ">ASIGNADO </a>';}
                            if ($row['estado']==2){ echo '<a class="btn btn-primary">CULMINADO </a>';}
                            if ($row['estado']==4){ echo '<a class="btn btn-danger ">ANULADO </a>';}
                          ?>
                        </td> 
                        <td class="text-center"><?php
                        if($row['idContrato']==null){
                         echo "-";
                         }
                         else{
                          echo $contrato['contrato'];
                         } ?></td>
                      </tr>
                        <!--Eliminar Modal -->
                      <div id="delete<?php echo $row['idDetalle_requerimiento'];echo $idRequerimiento?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarDetalleRequerimiento.php?idDet=<?php echo $row['idDetalle_requerimiento'];?>&idReq=<?php echo $idRequerimiento;?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                        <!--anular Modal -->
                      <div id="anular<?php echo $row['idDetalle_requerimiento'];echo $idRequerimiento?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/anularDetalleRequerimiento.php?idDet=<?php echo $row['idDetalle_requerimiento'];?>&idReq=<?php echo $idRequerimiento;?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ANULAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de anular el requerimiento?</p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php
                    }
                  ?>
                  
                  
                </tbody>
                
              </table> 

            </div>
            <div class="box-footer">
              
            </div>
            </div>
          </div>
        </div>
          
        <!-- /.row -->

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>