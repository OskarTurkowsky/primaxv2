<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$style = array(
        'alignment' => array(
            'justify' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
    );



$objPHPExcel->getDefaultStyle()->applyFromArray($style);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('D12', 'N°')
            ->setCellValue('E12', 'CC')
            ->setCellValue('F12', 'DESCRIPCION')
            ->setCellValue('G12', 'ESTACION')
            ->setCellValue('H12', 'MONTO')
            ->setCellValue('I12', 'CUENTA')
            ->setCellValue('J12', 'APELLIDOS Y NOMBRES')
            ->setCellValue('K12', 'DNI')
            ->setCellValue('L12', 'CARGO')
            ->setCellValue('M12', 'PROVINCIA')
            ->setCellValue('N12', 'F. INGRESO')
            ->setCellValue('O12', 'GARANTIA A 45 DIAS')
            ->setCellValue('P12', 'FECHA CESE')
            ->setCellValue('Q12', 'CUMPLE/NO CUMPLE')
            ->setCellValue('R12', 'TELEFONO');

$objPHPExcel->getActiveSheet()->getStyle('M1:M'.$objPHPExcel->getActiveSheet()->getHighestRow())
    ->getAlignment()->setWrapText(true); 

$objPHPExcel->getActiveSheet()->getRowDimension('12')->setRowHeight(45); 
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35); 
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(27); 
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20); 
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25); 
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(37); 
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(35); 
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15); 

$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getActiveSheet()->getStyle('A1:B2')->applyFromArray($styleArray);
unset($styleArray);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
