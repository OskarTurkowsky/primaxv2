﻿
<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$nombrepsicologo = $_SESSION['nombres'];
?>

<?php include('header.php'); ?>
    

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Repogramación
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-address-book-o"></i>Reclutamiento</a></li>
          <li class="active">Repogramación</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Registros</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="search" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center" style="min-width: 100px">OPCIONES</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center" >NOMBRES</th>
                    <th class="text-center" >DIRECCION</th>
                    <th class="text-center">FECHA EVALUACION</th>
                    <th class="text-center">TURNO</th>
                    <th class="text-center">TELEFONO</th>
                    <th class="text-center">CITADO POR</th>
                    <th class="text-center">EMPRESA</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $idUsuario=$_SESSION['id'];
                    $admin=$_SESSION['admin'];
                    $consulta=ejecutarQuery("SELECT departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$idUsuario");
                    $deptUser=mysqli_fetch_assoc($consulta);   
                    $consulta1=ejecutarQuery("SELECT * from referido inner join persona on persona.idPersona=referido.idReferido
                       inner join distrito on distrito.idDistrito=persona.idDistrito
                       inner join provincia on provincia.idProvincia=distrito.idProvincia
                       inner join departamento on departamento.idDepartamento=provincia.idDepartamento 
                       where (persona.idUsuario=$idUsuario or departamento.idDepartamento='$deptUser[idDepartamento]') and referido.estado=4 ");
                    while($row=mysqli_fetch_assoc($consulta1)){
                      $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                      $prov = mysqli_fetch_assoc($pro);
                      $co= ejecutarQuery("SELECT nombre from consultor where idConsultor='$row[idConsultor]'");
                      $cons = mysqli_fetch_assoc($co);
                      $cl= ejecutarQuery("SELECT nombre from cliente where idCliente='$row[idCliente]'");
                      $cliente = mysqli_fetch_assoc($cl);
                      $ref= ejecutarQuery("SELECT fecha.idFecha as idFecha, turnos.nombre as turno from entrevista_referido 
                        inner join referido on entrevista_referido.idReferido=referido.idReferido 
                        inner join entrevista on entrevista.idEntrevista=entrevista_referido.idEntrevista
                        inner join fecha on entrevista.idFecha=fecha.idFecha
                        inner join turnos on entrevista.idTurno=turnos.idTurno
                        where referido.idReferido='$row[idReferido]'");
                      $referido = mysqli_fetch_assoc($ref);
                  ?>                
                      <tr>
                        <td class="text-center">   
                          <a href="reprogramacion.php?id=<?php echo $row['idPersona'];?>">
                            <button type='button' title="EDITAR" class='btn btn-warning btn-circle'>
                              <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                            </button>
                          </a>
                          <a href="#delete<?php echo $row['idPersona'];?>" data-toggle="modal"><button type='button' id="eliminar" title="ELIMINAR" class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>
                          </a>
                         
                        </td>
                          <td class="text-center"> 
                          <?php 
                            if (strlen($row['idPersona'])==7) {
                              echo '0'.$row['idPersona'];
                            }
                            else
                            {
                              echo $row['idPersona'];
                            }
                           ?>
                          </td>
                          <td class="text-center"> 
                            <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?>
                            </td>

                          <td class="text-center"> 
                          <?php echo $prov['pro']." - ".$prov['dis']; ?>
                          </td>

                          <td class="text-center"> 
                          <?php echo date("d/m/Y", strtotime($referido['idFecha'])); ?></td>

                          <td class="text-center"> 
                          <?php echo $referido['turno']; ?></td>
                          
                          <td class="text-center"> 
                          <?php echo $row['telefono']; ?></td>
                          
                          <td class="text-center"> 
                          <?php echo $cons['nombre']; ?></td>
                          
                          <td class="text-center"> 
                          <?php echo strtoupper($cliente['nombre']); ?></td>
                      </tr>

                      <div id="delete<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarReferido.php?id=<?php echo $row['idPersona'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar a <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idPersona']; ?>,'example','../php/eliminarReferido.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php
                  }
                  ?>
                </tbody>
              </table>              
            </div>
            <div class="box-footer">
            </div>
          </div>
        </div>
      </div>
          
      <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>
<!--<script type="text/javascript">
  $(document).ready(function(){  
     var total = '';
    $('input[type=checkbox]').click(function() {
      aux = '';
      $("input:checkbox:checked").each(function() {
        aux = aux.concat($(this).val(),"-");
      });
      total = aux;
      
      $("#eliminar").click(function()
           {
             console.log(total);
           }
        );
    }); 
    
    }
  );
</script>->