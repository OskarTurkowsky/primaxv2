<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Carta de presentación
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i>Formatos</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Requerimientos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
            <div class="box-body" id="notall" >
              <table id="exampleFormato" class="table-bordered table-hover">
                <thead>
                    <tr>
                      <th></th>
                      <th class="text-center">FORMATO</th>
                      <th class="text-center">CLIENTE</th>
                      <th class="text-center">FECHA ALTA</th>
                      <th class="text-center">FECHA EXAMEN</th>
                      <th class="text-center">CANTIDAD</th>
                      <th class="text-center">ESTADO</th>                   
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                    $rs=ejecutarQuery("SELECT * FROM requerimiento where estado=1 ");
                                         
                    while($row=mysqli_fetch_assoc($rs)){
                    ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center">
                          <a href="cartaPresentacion2.php?idReq=<?php echo $row['idRequerimiento'];?>" class="btn-link">Ver detalles</a>
                        </td>
                        <td class="text-center"> <?php 
                        $consulta = ejecutarQuery("SELECT * FROM cliente WHERE idCliente=$row[idCliente]");
                        $aa = mysqli_fetch_assoc($consulta);
                        echo $aa['nombre']; 
                        ?></td>
                        <td class="text-center"><?php echo date("d/m/Y", strtotime($row['fechaAlta'])); ?></td>
                        <td style="text-align: center"><?php 
                          if (date("d/m/Y", strtotime($row['fechaExamen']))=='31/12/1969') {
                              echo "-";
                            }
                          else
                          {
                          echo date("d/m/Y", strtotime($row['fechaExamen']));
                          } ?>
                        </td>
                        <td class="text-center"><?php echo $row['cantidad']; ?></td>
                        <td style="text-align: center">
                          <?php 
                            if ($row['estado']==1){ echo '<a class="btn btn-sm  btn-success ">ACTIVO </a>';}
                            if ($row['estado']==2){ echo '<a class="btn btn-sm  btn-primary ">CULMINADO </a>';}
                          ?>
                        </td>  
                      </tr>
                    <?php
                      }
                    ?>
                </tbody>
                </table> 
            </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>