﻿<?php
require '../php/funciones.php';

if(!haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$idUsuario=$_SESSION['id'];
$admin=$_SESSION['admin'];
$inicio=$_GET['inicio'];
$fin=$_GET['fin'];
?>

<?php include('header.php'); ?>
    

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reporte de Reclutadores
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-search"></i>Reporte de Reclutadores</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Rango de fecha</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="soloexport" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 80px">DNI</th>
                    <th class="text-center" style="width: 150px">NOMBRES</th>
                    <th class="text-center" >DISTRITO</th>                    
                    <th class="text-center" >DISPONIBILIDAD</th>
                    <th class="text-center" >ANTECEDENTE POLICIAL</th>
                    <th class="text-center" >CARNET DE SANIDAD</th>
                    <th class="text-center" style="width: 200px">TELEFONOS</th>
                    <th class="text-center" >FECHA EVALUACIÓN</th>
                    <th class="text-center" >OBSERVACIÓN</th>
                    <th class="text-center" style="width: 150px">CONSULTOR</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $rs=ejecutarQuery(" SELECT backup.*, persona.idPersona as idPersona, persona.nombres, persona.apellidoPaterno as apellidoPaterno, persona.apellidoMaterno as apellidoMaterno, persona.idDistrito as idDistrito, persona.observacion as observacion, persona.telefono as telefono  FROM persona  inner join backup on persona.idPersona=backup.idBackup where backup.fechaEvaluacion between '$inicio' and '$fin' and persona.estado IN (0,2,13,15) and backup.disponibilidad!='NO' ");                   
                    while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as distrito FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);
                        $cons=ejecutarQuery("SELECT nombre from consultor where idConsultor=$row[idConsultor] ");
                        $consultor = mysqli_fetch_assoc($cons);
                  ?>                
                      <tr>
                        <td class="text-center" style="width: 80px"><?php echo $row['idPersona']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center" ><?php echo $prov['distrito']; ?></td>
                        <td class="text-center" ><?php echo $row['disponibilidad']; ?></td>
                        <td class="text-center" ><?php echo $row['policial']; ?></td>
                        <td class="text-center" ><?php echo $row['sanidad']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $row['telefono']; ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($row['observacion']); ?></td>
                        <td class="text-center" style="width: 150px"><?php echo strtoupper($consultor['nombre']); ?></td>
                      </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer">
              <?php 
              $cons=ejecutarQuery(" SELECT count(*) as contador FROM persona  inner join backup on persona.idPersona=backup.idBackup where backup.fechaEvaluacion between '$inicio' and '$fin' and persona.estado IN (0,2,13,15) "); 
              $consult=mysqli_fetch_assoc($cons);
               ?>
              <h4> Total:
               <?php echo $consult['contador']; ?> </h4>
            </div>
          </div>
          
        </div>
      </div>
      </section>

    </div>
    
<?php include('footer.php'); ?>