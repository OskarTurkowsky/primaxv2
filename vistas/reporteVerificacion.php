﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

?>

<?php include('header.php'); ?>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reporte de verificación
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-search"></i> Reporte de verificación</a></li>
        </ol>
      </section>
      <section class="content">
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Tabla de Postulantes</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <table id="example" class="table-bordered table-hover">
                  <thead>
                    <tr>
                      <th ></th>
                      <th>OPCIONES</th>
                      <th class="text-center">DNI</th>
                      <th class="text-center" style="min-width: 180px">NOMBRES</th>
                      <th class="text-center">SEXO</th>
                      <th class="text-center">CLIENTE</th>
                      <th class="text-center">CARGO</th>
                      <th class="text-center">ESTACIÓN</th>
                      <th class="text-center">CLINICA</th>  
                      <th class="text-center">FECHA ALTA</th> 

                      <th class="text-center">FECHA EXAMEN<font color="white">--------</font></th>    
                      <th class="text-center">DEPARTAMENTO<font color="white">------</font></th>
                      <th class="text-center">PROVINCIA<font color="white">-------------</font></th>  
                      <th class="text-center">DISTRITO<font color="white">---------------</font></th>       
                      <th class="text-center">DIRECCIÓN<font color="white">-------------</font></th> 
                      <th class="text-center">FECHA NACIMIENTO<font color="white">-</font></th>
                      <th class="text-center">ESTADO CIVIL<font color="white">----------</font></th>   
                      <th>TALLA BOTAS  <font color="white">----------</font> </th>
                      <th>TALLA UNIFORME <font color="white">-----</font></th>
                      <th>TEL&Eacute;FONOS <font color="white">------------</font></th>
                      <th>SPP <font color="white">----------------------</font></th>
                      <th>FECHA SPP  <font color="white">-------------</font></th>
                      <th>CODIGO SPP  <font color="white">-----------</font></th>
                      <th>OBSERVACIONES <font color="white">-----</font></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                      $idUsuario=$_SESSION['id'];
                      $admin=$_SESSION['admin'];
                      $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                      $abcc=mysqli_fetch_assoc($aa); 
                      if ($admin==0) {
                        $rs=ejecutarQuery("SELECT persona.*, asig.idDetalle_requerimiento as idDet FROM persona
                         INNER JOIN (select asignado.*, max(idAsignado) 
                        from asignado group by asignado.idPersona desc) asig INNER JOIN detalle_requerimiento
                         ON asig.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento 
                         INNER JOIN requerimiento ON requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
                         inner join usuario on usuario.idUsuario=persona.idUsuario WHERE persona.idPersona=asig.idPersona
                         and detalle_requerimiento.estado=1 and persona.estado IN (3,16,14, 25) and persona.idUsuario=$idUsuario"); 
                      }
                      else
                      if ($admin==2) {
                      $rs=ejecutarQuery("SELECT persona.*, asig.idDetalle_requerimiento as idDet FROM persona
                         INNER JOIN (select asignado.*, max(idAsignado) 
                        from asignado group by asignado.idPersona desc) asig INNER JOIN detalle_requerimiento
                         ON asig.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento 
                         INNER JOIN requerimiento ON requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
                         inner join usuario on usuario.idUsuario=persona.idUsuario WHERE persona.idPersona=asig.idPersona
                         and detalle_requerimiento.estado=1 and persona.estado IN (3,16,14, 25) and usuario.idDistrito=$abcc[idDistrito] group by asig.idPersona");  
                        }
                      else
                      if ($admin==1 || $admin==3) {
                        $rs=ejecutarQuery("SELECT persona.*, asig.idDetalle_requerimiento as idDet FROM persona
                         INNER JOIN (select asignado.*, max(idAsignado) 
                        from asignado group by asignado.idPersona desc) asig INNER JOIN detalle_requerimiento
                         ON asig.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento 
                         INNER JOIN requerimiento ON requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
                         inner join usuario on usuario.idUsuario=persona.idUsuario WHERE persona.idPersona=asig.idPersona
                         and detalle_requerimiento.estado=1 and persona.estado IN (3,16,14, 25) group by asig.idPersona"); 
                      }           
                      while($row=mysqli_fetch_assoc($rs)){
                        $estado=$row['estado'];
                        $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
                        $dtr = mysqli_fetch_assoc($dd);

                        $rr=ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento='$dtr[idRequerimiento]'"); 
                        $rq = mysqli_fetch_assoc($rr);
                        $idRequerimiento=$rq['idRequerimiento'];

                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);

                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
                        $spp = mysqli_fetch_assoc($sp);

                        $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
                        $cargo = mysqli_fetch_assoc($car);

                        $es= ejecutarQuery("SELECT nombre from estacion where idEstacion = '$dtr[idEstacion]' ");
                        $est = mysqli_fetch_assoc($es);

                        $cl= ejecutarQuery("SELECT nombre from clinica where idClinica = '$dtr[idClinica]' ");
                        $cli = mysqli_fetch_assoc($cl);

                        $clien=ejecutarQuery("SELECT cliente.nombre as cliente, cliente.idCliente as idCliente FROM cliente INNER JOIN requerimiento ON cliente.idCliente=requerimiento.idCliente WHERE requerimiento.idCliente=$rq[idCliente]");
                        $cliente = mysqli_fetch_assoc($clien);
                    ?>                  
                        <tr bgcolor="white">
                          <td></td>
                          <td class="text-center">    
                          <?php 
                          if ($estado==3 or $estado==14 or $estado==16 or $estado==25) {
                            ?> 
                            <a href="#desconfirm<?php echo $row['idPersona']; echo $row['idDet']; echo $idRequerimiento?>" data-toggle="modal"><button title="REVERTIR" type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span></button></a>
                            <?php 
                          }
                          if ($estado==8){
                            echo "PASÓ EXÁMEN MÉDICO";
                            } 
                          if ($estado==10){
                            echo "TRABAJADOR";
                            } 
                            if ($estado==4){
                            echo "REPORTE GENERADO";
                            }
                          ?>                             
                         </td>
                          <td class="text-center"><?php 
                            if (strlen($row['idPersona'])==7) {
                                echo '0'.$row['idPersona'];
                              }
                              else
                              {
                                echo $row['idPersona'];
                              }
                           ?></td>
                          <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                          <td class="text-center"><?php echo $gener['nombre']; ?></td>
                          <td class="text-center"><?php echo $cliente['cliente']; ?></td>
                          <td class="text-center"><?php echo $cargo['nombre']; ?></td>
                          <td class="text-center"><?php echo $est['nombre']; ?></td>
                          <td class="text-center"> <?php echo $cli['nombre']; ?> </td>
                          <td class="text-center"> <?php echo date("d/m/Y", strtotime($rq['fechaAlta'])); ?> </td>
                          <td class="text-center"> <?php if(date("d/m/Y", strtotime($row['fechaExamenMedicoReal'])) == '30/11/-0001' || $cliente['idCliente']==2025903307) {echo ''; } else  echo  date("d/m/Y", strtotime($row['fechaExamenMedicoReal'])); ?> </td>

                          <td><?php echo $prov['dep']; ?></td>
                          <td><?php echo $prov['pro']; ?></td>
                          <td><?php echo $prov['dis']; ?></td>
                          <td><?php echo strtoupper($row['direccion']); ?></td>
                          <td><?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>       </td>
                          <td><?php echo $row['estadoCivil']; ?></td>
                          <td><?php echo $row['tallaBotas']; ?></td>
                          <td><?php echo $row['tallaUniforme']; ?></td>
                          <td><?php echo $row['telefono']; ?></td>
                          <td><?php echo $spp['nombre']; ?></td>
                          <td>
                            <?php if(date("d/m/Y", strtotime($row['fechaSpp'])) == '30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                          </td>
                          <td><?php echo $row['codigoSpp']; ?></td>
                          <td><?php echo strtoupper($row['observacion']); ?></td>
                        </tr>
                         
                        <!--DesConfirmar Modal -->
                        <div id="desconfirm<?php echo $row['idPersona']; echo $row['idDet']; echo $idRequerimiento?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">
                            <form method="post" action="../php/revertirPostulanteRequerimiento.php?codigo=<?php echo $row['idPersona'];?>&idDet=<?php echo $row['idDet'];?>&idReq=<?php echo $idRequerimiento;?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">REVERTIR REQUERIMIENTO DE POSTULANTE</h4>
                                </div>
                                <div class="modal-body">
                                  <p>¿Esta seguro de revertir al postulante <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres'];?>?</strong></p>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>SI</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                    <?php
                      }
                    ?>                  
                  </tbody>                
                </table> 
              <div class="box-footer">
                <a href="../php/generarReporteVerificacion.php">
                  <center>
                    <button type="submit" class='btn btn-primary btn-lg'>GENERAR</button>    
                  </center>
                </a>                
              </div>
              </div>
            </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->     

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>