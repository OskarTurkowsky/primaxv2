<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>




<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Bienvenido
          <small><?php echo $_SESSION['nombres'];?></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-angellist"></i> Home</a></li>
          <li class="active">T-Soluciona</li>
        </ol>
      </section>
      <br>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <center>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                  <img src="../img/carril1.jpg" alt="Los Angeles">
                  <div class="carousel-caption">
                    <h3>T-SOLUCIONA</h3>
                    <p>Thank you</p>
                  </div>
                </div>

                <div class="item">
                  <img src="../img/carril2.jpg" alt="Chicago">
                  <div class="carousel-caption">
                    <h3>T-SOLUCIONA</h3>
                    <p>Thank you</p>
                  </div>
                </div>

                <div class="item">
                  <img src="../img/carril3.jpg" alt="New York">
                  <div class="carousel-caption">
                    <h3>T-SOLUCIONA</h3>
                    <p>Thank you</p>
                  </div>
                </div>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </center>
        </div>
        <!-- /.row (main row) -->
      </section>
      <!-- /.content -->
    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>