<?php
require '../php/funciones.php';
if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>

<!-- CONTENIDO DE LA PAGINA -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Antecedentes
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-eye"></i> Antecedentes</a></li>
    </ol>
  </section><br>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title"> Tabla de datos para Antecedentes </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  
          </div>
          <form class="form-signin" autocomplete="off" action="../php/sendgroup.php" method="POST" name="form1" enctype="multipart/form-data">
          <div class="box-body">
            <table id="example" class="table-bordered table-hover">
              <thead>
                <tr>
                  <th></th>
                  <th class="text-center" style="min-width: 150px">OPCIONES</th>
                  <!--<th class="text-center" style="min-width: 80px">FECHA ALTA</th>-->
                  <th class="text-center" style="min-width: 60px">DNI</th>
                  <th class="text-center" style="min-width: 200px"   >NOMBRES</th>
                  <th class="text-center">TEL&Eacute;FONOS</th>
                  <th class="text-center">CONSULTOR</th>
                  <th class="text-center" >FECHA ALTA</th>
                  <th class="text-center" >FECHA NACIMIENTO</th>
                  <th class="text-center" >SEXO <font color="white">---------------------</font></th>
                  <th class="text-center" >ESTADO CIVIL <font color="white">----------</font></th>
                  <th>TALLA BOTAS  <font color="white">----------</font> </th>
                  <th>TALLA UNIFORME <font color="white">-----</font></th>
                  <th>DIRECCION <font color="white">-------------</font></th>
                  <th>PROVINCIA <font color="white">-------------</font></th>
                  <th>DISTRITO <font color="white">---------------</font></th>
                  <th>SPP <font color="white">----------------------</font></th>
                  <th>FECHA SPP  <font color="white">-------------</font></th>
                  <th>CODIGO SPP  <font color="white">-----------</font></th>
                  <th>OBSERVACIONES <font color="white">-----</font></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $idUsuario=$_SESSION['id'];
                $admin=$_SESSION['admin'];
                $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                $abcc=mysqli_fetch_assoc($aa);

                if ($admin==1 || $admin==3) {
                  $rs=ejecutarQuery("SELECT persona.*, asig.idDetalle_requerimiento as idDet, asig.idAsignado FROM (select asignado.*, max(idAsignado) from asignado group by asignado.idPersona desc) asig inner join persona on persona.idPersona=asig.idPersona
                       WHERE persona.estado IN (7,9) group by asig.idPersona");
                }
                else
                  if ($admin==0) {
                    $rs=ejecutarQuery("SELECT persona.*, asig.idDetalle_requerimiento as idDet, asig.idAsignado FROM (select asignado.*, max(idAsignado) from asignado group by asignado.idPersona desc) asig inner join persona on persona.idPersona=asig.idPersona
                      INNER JOIN usuario ON usuario.idUsuario=persona.idUsuario
                       WHERE persona.estado IN (7,9) and persona.idUsuario=$idUsuario");
                  }
                else
                if ($admin==2) {
                  $rs=ejecutarQuery("SELECT persona.*, asig.idDetalle_requerimiento as idDet, asig.idAsignado FROM (select asignado.*, max(idAsignado) from asignado group by asignado.idPersona desc) asig inner join persona on persona.idPersona=asig.idPersona
                      INNER JOIN usuario ON usuario.idUsuario=persona.idUsuario
                       WHERE persona.estado IN (7,9) and usuario.idDistrito=$abcc[idDistrito] group by asig.idPersona");
                }                
                while($row=mysqli_fetch_assoc($rs)){
                  $requerimiento=ejecutarQuery("SELECT * from asignado inner join detalle_requerimiento on detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_requerimiento inner join requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento where asignado.idDetalle_requerimiento='$row[idDet]' ");
                  $datosReq = mysqli_fetch_assoc($requerimiento);
                  $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                  $prov = mysqli_fetch_assoc($pro);
                  $us=ejecutarQuery("SELECT * FROM usuario where idUsuario='$row[idUsuario]'"); 
                  $usu = mysqli_fetch_assoc($us);
                  $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                  $gener = mysqli_fetch_assoc($gen);
                  $sp= ejecutarQuery("SELECT nombre from spp where idSpp='$row[idSpp]'");
                  $spp = mysqli_fetch_assoc($sp);
                ?>
                  <tr>
                    <td></td>
                    <td class="text-center" <?php if ($row['estado']==9) { echo 'style="background-color: #cc3737 ; color:#FFFFFF"'; }?>>
                      <?php 
                        if ( $admin==1 || $admin==3) {
                          ?>
                            <input type="checkbox" name="id[]"  value="<?php echo $row['idPersona'] ?>"  />
                           &nbsp;&nbsp;
                          <?php 
                        }
                       ?>
                      
                    <?php 
                      $id[] = $row['idPersona'];
                      switch ($row['estado']) {
                        case 5:
                        case 6:
                        case 7:
                          if ($_SESSION['admin'] == 0 || $_SESSION['admin'] == 2){
                    ?>
                            <button type="button" class="btn btn-warning" >
                              <i class="fa fa-clock-o" aria-hidden="true" title="EN PROCESO"></i>
                            </button> 
                    <?php
                          } else{
                    ?>
                              <a href="../php/enviar.php?codigo=<?php echo $row['idPersona'];?>">
                              <button class="btn btn-primary btn-circle" type="button" title="NO TIENE ANTECEDENTES"><i class="fa fa-check"></i>
                              </button></a>

                              <a href="#eliminar<?php echo $row['idPersona'];?>" data-toggle="modal">
                                <button type="button" title="SI TIENE ANTECEDENTES" class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
                                </button>
                              </a>
                    <?php
                          }
                            break;
                        case 8:
                            echo "SIN ANTECEDENTES ";
                            break;    
                        case 9:
                            echo "CON ANTECEDENTES: $row[tipoAntecedente]";
                            break;
                        }
                    ?> 
                      
                    </td>
                    <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?>><?php 
                      if (strlen($row['idPersona'])==7) {
                            echo '0'.$row['idPersona'];
                          }
                          else
                          {
                            echo $row['idPersona'];
                          }
                     ?></td>
                    
                    <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?>><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                    
                    <td class="taxt-center" <?php if ($row['estado']==9) { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?>><?php echo $row['telefono']; ?></td>
                                           
                    <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?>><?php echo $usu['nombres']; ?></td>
                    <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> > <?php 
                      if(date("d/m/Y", strtotime($datosReq['fechaAlta']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($datosReq['fechaAlta'])); 
                      ?>       </td>
                    <td><?php echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>       </td>
                    <td><?php echo $gener['nombre']; ?></td>
                    <td><?php echo $row['estadoCivil']; ?></td>
                    <td><?php echo $row['tallaBotas']; ?></td>
                    <td><?php echo $row['tallaUniforme']; ?></td>
                    <td><?php echo strtoupper($row['direccion']); ?></td>
                    <td><?php echo $prov['pro']; ?></td>
                    <td><?php echo $prov['dis']; ?></td>
                    <td><?php echo $spp['nombre']; ?></td>
                    <td>
                          <?php if(date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                    </td>
                    <td><?php echo $row['codigoSpp']; ?></td>
                    <td><?php echo strtoupper($row['observacion']); ?></td>
                  </tr>

                    <div id="eliminar<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/eliminar.php?codigo=<?php echo $row['idPersona'];?>" name="antecedentes">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">DETALLE DE ANTECEDENTE</h4>
                              </div>
                              <div class="modal-body">
                                <p><strong>POSTULANTE: </strong><?php echo $row['apellidoPaterno']." ".$row['nombres'];?></p>
                                <p><strong>TIPO DE ANTECEDENTE: </strong>
                                  <input type="text" class="form-control pull-right" name="tipoAntecedente" style="text-transform:uppercase;" >
                                </p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnCheck" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> GUARDAR</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> CANCELAR</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                <?php   
                } 
                ?>
              </tbody>
            </table>
          </div>
          <CENTER>
            <?php if ($admin==1 || $admin==3) {
              ?>
              <button type="submit" name="btnEliminar" class="btn btn-success btn-lg" value="Enviar" >CONFIRMAR</button>
              <?php
            } ?>
            
     
          </CENTER>
          
          </form>
          <div class="box-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- /.row -->

  </section>
  <!-- /.content -->
  

</div>
<!-- FIN DEL CONTENIDO DE LA PAGINA-->


<?php include('footer.php'); ?>