﻿<?php
require '../php/funciones.php';
if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

?>

<?php include('header.php'); ?>

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reclutamiento
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-address-book-o"></i>Reclutamiento</a></li>
          <li class="active">Registro</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header">
                <h3 class="box-title">Formulario de Registro</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/nuevoReferido.php" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required="" id="dni">
                        </div>
                        <div id="verificar" hidden="">
                          <span class="label label-danger">
                            <label id="verificarDni"></label>
                          </span>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Paterno *</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="paterno" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Materno *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="materno" required=""">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Nombres *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required="" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Tel&eacute;fono 1 *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono1" class="form-control pull-right" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Observaciones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <textarea class="form-control pull-right" rows="1" style="resize: vertical;min-height: 60px; text-transform:uppercase" name="observaciones">
                          </textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Tel&eacute;fono 2</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono2" class="form-control pull-right">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Tel&eacute;fono 3</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono3" class="form-control pull-right">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Departamento *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="departamento" id="" onchange="from(document.form1.departamento.value,'midiv','../php/provincias.php');">
                            <option>Elige una opción</option>
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Provincia *</label>
                        <div class="input-group" id="midiv">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="provincia" id="provincias">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Distrito *</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="distrito" id="distritos">
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Citado por *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="citado" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM consultor where estado=1 order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idConsultor']; ?>" <?php if ($eee['idConsultor']==$_SESSION['id']){?> selected="selected" <?php } ?> > <?php echo strtoupper($eee['nombre']); ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Empresa a la que postula *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="cliente" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM cliente order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idCliente']; ?>" <?php if ($eee['idCliente']==2055454574){?> selected="selected" <?php } ?> ><?php echo $eee['nombre']; ?></OPsTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Turno *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="turno" id="turno" class="form-control pull-right">
                            <?php                           
                            $consulta = ejecutarQuery("SELECT * FROM turnos order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idTurno']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha de Entrevista *</label>
                        <div class="input-group" id="div_entrevista">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="from_date" id="from_date" required="">
                        </div>
                      </div>
                      <br>
                      <div class="form-group" id="details" hidden="">
                        <center>
                          <h4> 
                            <span class="label label-danger">
                              <label id="result"></label>
                            </span>
                          </h4>
                          <input type="text" name="resultado" id="resultado" hidden="">
                        </center>                        
                      </div>
                      <br><br>
                      <div class="form-group" >
                        <center> 
                          <button type="submit"  id="boton" class="btn btn-primary pull-center">REGISTRAR
                          </button> 
                        </center>                         
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){  

      var consulta;
      $("#dni").focus();
      $("#dni").keyup(function(e){
        consulta = $("#dni").val();
        if (consulta.length==8) {
          $("#verificarDni").delay(0).queue(function(n) {
            $("#verificarDni").html('');
              $.ajax({
                      type: "POST",
                      url: "verificarDni.php",
                      data: "dni="+consulta,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        if (data.trim()=='') {
                          $("#verificar").hide();
                          $('#boton').attr("disabled", false);
                          n();
                        }
                        else
                        {
                          $("#verificar").show();
                          $("#verificarDni").html(data);
                          $('#boton').attr("disabled", true);
                          n();
                        }
                      }
              });
          });  
        }  
        else
          $("#verificar").hide();                 
      });

    document.getElementById('from_date').addEventListener('change', function(){
        var from_date = $('#from_date').val();  
        var turno = $('#turno').val(); 
        if(turno != '' && from_date != '')  
        {  
          $.ajax({  
                url:"filter.php",  
                method:"POST",  
                data:{from_date:from_date, turno:turno},  
                success:function(data)  
                {  
                  console.log(data);
                  $('#resultado').val(data);
                  if (data.trim()=='EMPTY' || data.trim()=='OK' || data.trim()=='EMPTY ENTREVISTA') {
                    $("#details").hide();
                    $('#boton').attr("disabled", false);
                  }
                  else
                  {
                    $('#boton').attr("disabled", true);
                    $("#details").show();
                    $('#result').html(data);
                    
                  }
                }  
          });  
        }  
      }
    );

  document.getElementById('turno').addEventListener('change', function(){
        var from_date = $('#from_date').val();  
        var turno = $('#turno').val(); 
        
        if(turno != '' && from_date != '')  
        {  
          $.ajax({  
                url:"filter.php",  
                method:"POST",  
                data:{from_date:from_date, turno:turno},  
                success:function(data)  
                {  
                  console.log(data);
                  $('#resultado').val(data);
                  if (data.trim()=='EMPTY' || data.trim()=='OK' || data.trim()=='EMPTY ENTREVISTA') {
                    $("#details").hide();
                    $('#boton').attr("disabled", false);
                  }
                  else
                  {
                    $('#boton').attr("disabled", true);
                    $("#details").show();
                    $('#result').html(data);
                  }
                }  
          });  
        }  
      }
  );    
  });  
</script>