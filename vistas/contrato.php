<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Tipo de Contrato
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-building"></i>Tipo de Contrato</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Añadir tipo de contrato</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form method="POST" action="../php/nuevoContrato.php" name="form1">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Contrato</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-file-text-o"></i>
                          </div>
                          <input type="text" name="contrato"class="form-control pull-right" >
                        </div>
                      </div> 
                    </div>              
                    <div class="col-md-4">
                        <br>
                        <button type="submit" class='btn btn-primary btn-md'>AÑADIR
                        </button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>