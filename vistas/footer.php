<!-- FOOTER-->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <img src="../img/logo.png" height="33" width="220">
        <strong><span style="color: #444444;"> <FONT FACE="Brush Script MT" size="4">Somos Tu Socio Estrat&eacutegico</FONT></span> </strong>
      </div>
      <strong>Copyright &copy; 2019  <a href="#" > Area de Sistemas</a></strong> 
    </footer>
    <!-- FIN FOOTER -->


    <!-- MENU DE LA IZQUIERDA -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
        </div>
      </div>
    </aside>
    <!-- FIN MENU DE LA IZQUIERDA -->
    
  </div> <!--wrapper -->
  
  <!--DataTable-->
  <!-- DataTables -->
  

  <!-- JAVASCRIPT-->
  <script src="../js/bootstrap.min.js"></script>
  <!-- mios-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
  </script>
  <script src="../js/combito.js"></script>
  <script src="../js/ajax.js"></script>
  
  <!-- jQuery 3 -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- Morris.js charts -->
  <script src="../bower_components/raphael/raphael.min.js"></script>
  <script src="../bower_components/morris.js/morris.min.js"></script>
  <!-- Sparkline -->
  <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="../bower_components/moment/min/moment.min.js"></script>
  <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="../dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../dist/js/demo.js"></script>
  <!-- Hora -->
  <script src="../js/hora.js"></script>
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- page script -->
  <!-- DataTables -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
  <script>
  $(document).ready(function() {
    // Setup - add a text input to each footer cell
      $('#buscar tfoot th').each( function () {
          var title = $(this).text();
          $(this).html( '<input type="text" class="form-control" placeholder="Filtrar" />' );
      } );
   
      // DataTable
      var table = $('#buscar').DataTable({
        order: [ 1, 'asc' ],
        "scrollX": true, 
        dom: 'Bfrtip', 
        buttons: [
            'copy', 'csv', 
            {
               extend: 'excel',
               title: 'T-SOLUCIONA'  
            }, 
            'pdf', 'print'
        ]
      });

      var table = $('#search').DataTable({
        order: [ 1, 'asc' ],
        "scrollX": true, 
        dom: 'Bfrtip', 
        buttons: [
            'copy', 'csv', 
            {
               extend: 'excel',
               title: 'T-SOLUCIONA'  
            }, 
            'pdf', 'print'
        ]
      });

      var table = $('#buscarAsistencia').DataTable({
        order: [ 1, 'asc' ],
        "scrollX": true, 
        dom: 'Bfrtip', 
        buttons: [
            'copy', 'csv', 
            {
               extend: 'excel',
               title: 'CONTROL DE ASISTENCIA' ,
               exportOptions: {
                    columns: [ 1,2,3,4,5,6,7,8,9,10,11,12 ]
                }
            }, 
            'pdf', 'print'
        ]
      });

      $('#historial').DataTable({
        "scrollX": true,
        "searching": false,
        "lengthChange": false,
        "paging": false,
        "info": false, 
        "order":false
      });

      $('#descargar').DataTable({
        "scrollX": true,
        "searching": true,
        "lengthChange": false,
        "info": true, 
        "order":true
      });

      // Apply the search
      table.columns().every( function () {
          var that = this;
   
          $( 'input', this.footer() ).on( 'keyup change', function () {
              if ( that.search() !== this.value ) {
                  that
                      .search( this.value )
                      .draw();
              }
          } );
      } );  
    
      $('#example').DataTable({
        responsive: {
              details: {
                  type: 'column'
              }
          },
          columnDefs: [ {
              className: 'control',
              orderable: false,
              targets:   0
          } ],
          order: [ 2, 'desc' ]
      });

      $('#exampleReq').DataTable({
        responsive: {
              details: {
                  type: 'column'
              }
          },
          columnDefs: [ {
              className: 'control',
              orderable: false,
              targets:   0
          } ],
          order: [ 3, 'desc' ]
      });

      $('#exampleFormato').DataTable({
        responsive: {
              details: {
                  type: 'column'
              }
          },
          columnDefs: [ {
              className: 'control',
              orderable: false,
              targets:   0
          } ],
          order: [ 6, 'asc' ]
      });

      var table = $('#soloexport').DataTable({
        order: [ 1, 'asc' ],
        "scrollX": true, 
        dom: 'Bfrtip', 
        buttons: [
            'copy', 'csv', 
            {
               extend: 'excel',
               title: 'T-SOLUCIONA'  
            }, 
            'pdf', 'print'
        ]
      });   

      $("input[type=radio]").click(function(event){
        var valor = $(event.target).val();
        if (valor == "distrito") {
            $("#reporteDistrito").show();
            $("#rangoFecha").hide();
            $("#reporteReclutador").hide();
        }
        if (valor == "reclutador") {
            $("#reporteDistrito").hide();
            $("#reporteReclutador").show();
            $("#rangoFecha").hide();
        } 
        if(valor =="fecha"){
            $("#reporteDistrito").hide();
            $("#reporteReclutador").hide();
            $("#rangoFecha").show();
        } 
      });

      $("#antecedentes").click(function(event){
        var valor = $(event.target).val();
        if (valor == 0) {
            $("#razon").hide();
        }
        if (valor == 1) {
            $("#razon").show();
        } 
      });

      $("#cliente").click(function(event){
        var valor = $(event.target).val();
        //PECSA
        if (valor == 2025903307) {
            $("#fechaExamenMedico").hide();
        }
        //PRIMAX
        if (valor == 2055454574) {
            $("#fechaExamenMedico").show();
        } 
      });

      $("#mostrar").click(function(event){
        var valor = $(event.target).val();
        if (valor == "activos") {
            $("#notall").show();
            $("#all").hide();            
        }
        if (valor == "todos") {
            $("#all").show();
            $("#notall").hide();
        } 
      });

      $("#aptitud").click(function(event){
        var valor = $(event.target).val();
        if (valor == 1) {
            $("#motivo").hide();
        }
        if (valor == 2) {
            $("#motivo").show();
        } 
      });

  } );
  </script>
  <script>
    $(document).ready(function() {
      $('#solotable').DataTable({
        order: [ 1, 'asc' ]  
      });
      
    } );
    
    document.querySelector('input[list]').addEventListener('input', function(e) {
        var input = e.target,
            list = input.getAttribute('list'),
            options = document.querySelectorAll('#' + list + ' option'),
            hiddenInput = document.getElementById(input.id + '-hidden'),
            inputValue = input.value;

        hiddenInput.value = inputValue;

        for(var i = 0; i < options.length; i++) {
            var option = options[i];

            if(option.innerText === inputValue) {
                hiddenInput.value = option.getAttribute('data-value');
                break;
            }
        }
    });

    
    
  </script>
  
  <!-- FIN JAVASCRIPT-->

</body>
</html>
