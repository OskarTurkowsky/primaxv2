<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$idPersona = $_GET['id'];

$sql = ejecutarQuery("SELECT departamento.idDepartamento as idDepartamento from persona inner join distrito on distrito.idDistrito=persona.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento where persona.idPersona=$idPersona");
$dept=mysqli_fetch_assoc($sql);
$departamento=$dept['idDepartamento'];
?>

<?php include('header.php'); ?>

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Estaciones
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i> Estaciones</a></li>
          <li class="active">Posibles estaciones</li>
        </ol>
      </section>
      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Posibles Estaciones</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/guardarEstaciones.php?id=<?php echo $idPersona?>" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4"> 
                      <div class="form-group">
                        <label>1° Estacion</label>
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <input style="width:270px; height:30px" list="listado1" id="estaciones1" > 
                        </div>
                        <div class="input-group" id="midiv3">                          
                          <datalist id="listado1" >
                            <?php 
                            $sql = ejecutarQuery("SELECT * from estacion where idDepartamento=$departamento");
                            while($fila=mysqli_fetch_assoc($sql)){ 
                            ?>
                              <option data-customvalue="<?php echo $fila['idEstacion']?>" value="<?php echo $fila['nombre']?>"></option>
                            <?php
                            }
                            ?>
                          </datalist>
                        </div>
                        <input id="listado1-hide" hidden="" name="estacion1">
                      </div> 
                    </div>
                    <div class="col-md-4">  
                      <div class="form-group">
                        <label>2° Estacion</label>
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <input style="width:270px; height:30px" list="listado2" id="estaciones2"> 
                        </div>
                        <div class="input-group" id="midiv3">                          
                          <datalist id="listado2" >
                            <?php 
                            $sql = ejecutarQuery("SELECT * from estacion where idDepartamento=$departamento");
                            while($fila=mysqli_fetch_assoc($sql)){ 
                            ?>
                              <option data-customvalue="<?php echo $fila['idEstacion']?>" value="<?php echo $fila['nombre']?>"></option>
                            <?php
                            }
                            ?>
                          </datalist>
                          <input id="listado2-hide" hidden="" name="estacion2">
                        </div>
                      </div> 
                    </div>   
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>3° Estacion</label>
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <input style="width:270px; height:30px" list="listado3" id="estaciones3" > 
                        </div>
                        <div class="input-group" id="midiv3">                          
                          <datalist id="listado3" >
                            <?php 
                            $sql = ejecutarQuery("SELECT * from estacion where idDepartamento=$departamento" );
                            while($fila=mysqli_fetch_assoc($sql)){ 
                            ?>
                              <option data-customvalue="<?php echo $fila['idEstacion']?>" value="<?php echo $fila['nombre']?>"></option>
                            <?php
                            }
                            ?>
                          </datalist>
                          <input id="listado3-hide" hidden="" name="estacion3">
                        </div>
                      </div>
                      <center> <button type="submit" class="btn btn-primary pull-center">GUARDAR</button> </center>    
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>

      

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>

  <script>    
    document.getElementById("estaciones1").setAttribute("autocomplete","off");

    $('#estaciones1').on('input', function() {
      var value = $(this).val();
      $('#listado1-hide').val($('#listado1 [value="' + value + '"]').data('customvalue'));
    });

    $('#estaciones2').on('input', function() {
      var value = $(this).val();
      $('#listado2-hide').val($('#listado2 [value="' + value + '"]').data('customvalue'));
    });

    $('#estaciones3').on('input', function() {
      var value = $(this).val();
      $('#listado3-hide').val($('#listado3 [value="' + value + '"]').data('customvalue'));
    });

  </script>