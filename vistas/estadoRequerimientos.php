﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$idRequerimiento = $_GET['idReq'];
$admin=$_SESSION['admin'];
$consulta = ejecutarQuery("SELECT idCliente FROM requerimiento WHERE idRequerimiento=$idRequerimiento");
$aa = mysqli_fetch_assoc($consulta);  
?>

<?php include('header.php'); ?>

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Requerimientos
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i> Requerimientos</a></li>
          <li class="active">Detalle de Requerimientos</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row" id="tabla">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Requerimientos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="soloexport" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 40px">CLIENTE</th>
                    <th class="text-center" style="width: 150px">CARGO</th>
                    <th class="text-center" style="width: 100px">DEPARTAMENTO</th>
                    <th class="text-center" style="width: 120px">ESTACIÓN</th>
                    <th class="text-center" style="width: 200px">CLINICA</th>
                    <th class="text-center" style="width: 60px">GENERO</th>
                    <th class="text-center" style="width: 120px">DNI POSTULANTE</th>
                    <th class="text-center" style="width: 120px">TELEFONO POSTULANTE</th>
                    <th class="text-center" style="width: 250px">POSTULANTE</th>
                    <th class="text-center" style="width: 250px">POSTULANTE OBSERVADO</th>
                    <th class="text-center" style="width: 50px">APTITUD</th>
                    <th class="text-center" style="width: 50px">OBSERVACION</th>
                    <th class="text-center" style="width: 120px">USUARIO</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $rs=ejecutarQuery("SELECT  * FROM detalle_requerimiento WHERE idRequerimiento=$idRequerimiento  and estado!=3"); 
                    $auxiliar=1;
                    $variable=0;
                    while($row=mysqli_fetch_assoc($rs)){
                      $car=ejecutarQuery("SELECT * FROM cargo inner join detalle_requerimiento on  detalle_requerimiento.idcargo=cargo.idcargo where detalle_requerimiento.idcargo='$row[idcargo]' ");
                      $carg = mysqli_fetch_assoc($car);
                      $es=ejecutarQuery("SELECT * FROM estacion INNER JOIN detalle_requerimiento on estacion.idEstacion=detalle_requerimiento.idEstacion where estacion.idEstacion=$row[idEstacion] ");
                      $est = mysqli_fetch_assoc($es);
                      $dep=ejecutarQuery("SELECT departamento.nombre as nom FROM departamento INNER JOIN estacion ON departamento.idDepartamento=estacion.idDepartamento WHERE estacion.idEstacion= $row[idEstacion]");
                      $dept = mysqli_fetch_assoc($dep);
                      $gen=ejecutarQuery("SELECT genero.nombre as nomb FROM genero INNER JOIN detalle_requerimiento ON genero.idGenero=detalle_requerimiento.idGenero WHERE detalle_requerimiento.idGenero=$row[idGenero]");
                      $genr = mysqli_fetch_assoc($gen);
                      if ($aa['idCliente']==2055454574 && $row['idClinica']!=NULL) {
                        $cli=ejecutarQuery("SELECT * FROM clinica INNER JOIN detalle_requerimiento ON clinica.idClinica=detalle_requerimiento.idClinica WHERE detalle_requerimiento.idClinica=$row[idClinica]");
                        $clini = mysqli_fetch_assoc($cli);
                      }    
                      $clien=ejecutarQuery("SELECT cliente.nombre as cliente FROM cliente INNER JOIN requerimiento ON cliente.idCliente=requerimiento.idCliente WHERE requerimiento.idCliente=$aa[idCliente]");
                      $cliente = mysqli_fetch_assoc($clien);
                      $pos=ejecutarQuery("SELECT persona.idPersona as DNI, persona.nombres as nombres, persona.apellidoPaterno as apellidoPaterno, persona.apellidoMaterno as apellidoMaterno,persona.telefono as telefono, usuario.nombres as us,persona.idAptitud as idAptitud, persona.detalle_observado as detalle_observado FROM persona INNER JOIN asignado on asignado.idPersona=persona.idPersona INNER JOIN usuario on usuario.idUsuario=persona.idUsuario where asignado.idDetalle_requerimiento=$row[idDetalle_requerimiento]");
                      $post = mysqli_fetch_assoc($pos);

                      $pos2=ejecutarQuery("SELECT persona.idPersona as DNI, persona.nombres as nombres, persona.apellidoPaterno as apellidoPaterno, persona.apellidoMaterno as apellidoMaterno,persona.telefono as telefono, usuario.nombres as us,persona.idAptitud as idAptitud, persona.detalle_observado as detalle_observado FROM persona INNER JOIN historial_asignar on historial_asignar.idPersona=persona.idPersona INNER JOIN usuario on usuario.idUsuario=persona.idUsuario where historial_asignar.idDetalle_requerimiento=$row[idDetalle_requerimiento] order by persona.idPersona desc limit 1");
                      $post2 = mysqli_fetch_assoc($pos2);

                      if ($post['idAptitud']!=NULL) {
                        $ap=ejecutarQuery("SELECT aptitud.nombre as nombre FROM aptitud INNER JOIN persona ON persona.idAptitud=aptitud.idAptitud WHERE persona.idAptitud=$post[idAptitud]");
                        $apt = mysqli_fetch_assoc($ap);
                      }

                      if ($post['DNI']!=null) {
                        $asg=ejecutarQuery("SELECT idPersona FROM asignado where idPersona=$post[DNI]");
                        $asignado = mysqli_fetch_assoc($asg);
                      }
                      if ($post2['DNI']!=null) {
                        $hst=ejecutarQuery("SELECT idPersona FROM historial_asignar where idPersona=$post2[DNI] order by  idAsignado desc limit 1");
                        $historial = mysqli_fetch_assoc($hst);
                      }

                  ?>                  
                      <tr bgcolor="white">
                            <td class="text-center"><?php echo $cliente['cliente']; ?></td>
                            <td class="text-center"><?php echo $carg['nombre']; ?></td>
                            <td class="text-center"><?php
                            if ($est['idEstacion']==183 || $est['idEstacion']==253 || $est['idEstacion']==300 || $est['idEstacion']==301 || $est['idEstacion']==148 || $est['idEstacion']==146 || $est['idEstacion']==302) {
                                echo "LIMA SUR CHICO";
                            }
                            else
                            {
                              echo $dept['nom']; 
                            }
                             ?></td>
                            <td class="text-center"><?php echo $est['nombre']; ?></td> 
                            <td class="text-center">
                              <?php 
                                if ($aa['idCliente']==2055454574 && $row['idClinica']!=NULL) {
                                  echo $clini['nombre']; 
                                }
                                else
                                  echo "-";
                              ?>
                            </td> 
                            <td class="text-center"><?php echo $genr['nomb']; ?></td>
                            <td class="text-center"><?php echo $post['DNI']; ?></td>
                            <td class="text-center"><?php echo $post['telefono']; ?></td>
                            <td class="text-center"><?php 
                              if($asignado['idPersona']!=null) {
                                echo $post['apellidoPaterno']." ".$post['apellidoMaterno']." ".$post['nombres'];
                                $variable=1;
                              }
                              else
                                $variable=0;
                              ?> 
                            </td>   
                            <td class="text-center"><?php 
                              if ($historial['idPersona']!=null && ($post2['idAptitud']!=NULL || $post2['idAptitud']!=1)){
                                echo $post2['apellidoPaterno']." ".$post2['apellidoMaterno']." ".$post2['nombres'];
                              }
                              ?> </td>    
                            <td class="text-center">
                              <?php 
                                if ($post['idAptitud']!=NULL) {
                                  echo $apt['nombre']; 
                                }
                              ?>
                            </td>
                            <td class="text-center" ><?php echo strtoupper($post['detalle_observado']);?> </td> 
                            <td class="text-center" ><?php echo strtoupper($post['us']);?> </td>           
                      </tr>
                    <?php 
                      }                    
                  ?>                                    
                </tbody>
                
              </table> 

            </div>
            <div class="box-footer">
              
            </div>
            </div>
          </div>
        </div>
          
        <!-- /.row -->

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>