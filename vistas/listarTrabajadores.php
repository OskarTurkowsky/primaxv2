<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$idUsuario=$_SESSION['id'];
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i> Elegir Trabajadores</a></li>
        </ol>
      </section><br>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Trabajadores </h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                      <th ></th>
                      <th class="text-center">OPCIONES</th>
                      <th class="text-center">DNI</th>
                      <th class="text-center" style="min-width: 180px">NOMBRES</th>
                      <th class="text-center">SEXO</th>
                      <th class="text-center">CARGO</th>
                      <th class="text-center">ESTACIÓN</th>
                      <th class="text-center">CLINICA</th>  

                      <th class="text-center">FECHA ALTA</th> 
                      <th class="text-center">FECHA EXAMEN<font color="white">-------</font></th>    
                      <th class="text-center">DEPARTAMENTO<font color="white">------</font></th>
                      <th class="text-center">PROVINCIA<font color="white">-------------</font></th>  
                      <th class="text-center">DISTRITO<font color="white">---------------</font></th>       
                      <th class="text-center">DIRECCIÓN<font color="white">-------------</font></th> 
                      <th class="text-center">FECHA NACIMIENTO<font color="white">-</font></th>
                      <th class="text-center">ESTADO CIVIL<font color="white">----------</font></th>   
                      <th>TALLA BOTAS  <font color="white">----------</font> </th>
                      <th>TALLA UNIFORME <font color="white">-----</font></th>
                      <th>TEL&Eacute;FONOS <font color="white">------------</font></th>
                      <th>SPP <font color="white">----------------------</font></th>
                      <th>FECHA SPP  <font color="white">-------------</font></th>
                      <th>CODIGO SPP  <font color="white">-----------</font></th>
                      <th>OBSERVACIONES <font color="white">-----</font></th>    
                  </tr>
                </thead>

                <tbody>
                      <?php
                      $rs=ejecutarQuery("SELECT persona.*, asignado.idDetalle_requerimiento as idDet FROM persona INNER JOIN asignado WHERE persona.idPersona=asignado.idPersona AND persona.estado IN(10,20) and persona.idUsuario=$idUsuario");
                      while($row=mysqli_fetch_assoc($rs)){
                        $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
                        $dtr = mysqli_fetch_assoc($dd);

                        $rr=ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento='$dtr[idRequerimiento]'"); 
                        $rq = mysqli_fetch_assoc($rr);
                        $idRequerimiento=$rq['idRequerimiento'];

                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);

                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
                        $spp = mysqli_fetch_assoc($sp);

                        $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
                        $cargo = mysqli_fetch_assoc($car);

                        $es= ejecutarQuery("SELECT nombre from estacion where idEstacion = '$dtr[idEstacion]' ");
                        $est = mysqli_fetch_assoc($es);

                        $cl= ejecutarQuery("SELECT nombre from clinica where idClinica = '$dtr[idClinica]' ");
                        $cli = mysqli_fetch_assoc($cl);
                      ?>               
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center">
                          <button type="button" class="btn btn-warning" >
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                          </button> 
                       </td>
                        <td class="text-center"> <?php echo $row['idPersona']; ?></td>
                        <td class="text-center">
                          <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center"> 
                          <?php echo $gener['nombre']; ?></td>
                        <td class="text-center"> 
                          <?php echo $cargo['nombre']; ?></td>
                        <td class="text-center">
                            <?php echo $est['nombre']; ?>
                        </td>
                        <td class="text-center" > 
                            <?php echo $cli['nombre']; ?> 
                        </td>
                        <td class="text-center"> 
                          <?php echo date("d/m/Y", strtotime($rq['fechaAlta'])); ?> 
                        </td>

                        <td class="text-center"> <?php echo date("d/m/Y", strtotime($row['fechaExamenMedicoReal'])); ?> </td>
                        <td><?php echo $prov['dep']; ?></td>
                        <td><?php echo $prov['pro']; ?></td>
                        <td><?php echo $prov['dis']; ?></td>
                        <td><?php echo strtoupper($row['direccion']); ?></td>
                        <td><?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>       </td>
                        <td><?php echo $row['estadoCivil']; ?></td>
                        <td><?php echo $row['tallaBotas']; ?></td>
                        <td><?php echo $row['tallaUniforme']; ?></td>
                        <td><?php echo $row['telefono']; ?></td>
                        <td><?php echo $spp['nombre']; ?></td>
                        <td>
                            <?php if(date("d/m/Y", strtotime($row['fechaSpp'])) == '30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                        </td>
                        <td><?php echo $row['codigoSpp']; ?></td>
                        <td><?php echo strtoupper($row['observacion']); ?></td>                        
                      </tr>
                  <?php
                    } 
                  ?>
                </tbody>
                
              </table>
            </div>
            <div class="box-footer">              
            </div>
            </div>
          </div>
        </div>
      </section>
    </div>
<?php include('footer.php'); ?>