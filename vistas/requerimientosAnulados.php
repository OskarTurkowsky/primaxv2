﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$idRequerimiento = $_GET['idReq'];
$admin=$_SESSION['admin'];
$consulta = ejecutarQuery("SELECT idCliente FROM requerimiento WHERE idRequerimiento=$idRequerimiento");
$aa = mysqli_fetch_assoc($consulta);   
?>

<?php include('header.php'); ?>

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Requerimientos
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i> Requerimientos</a></li>
          <li class="active">Detalle de Requerimientos</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row" id="tabla">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Requerimientos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="soloexport" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 20px">CLIENTE</th>
                    <th class="text-center" style="width: 150px">CARGO</th>
                    <th class="text-center" style="width: 150px">DEPARTAMENTO</th>
                    <th class="text-center" style="width: 200px">ESTACIÓN</th>
                    <th class="text-center" style="width: 200px">CLINICA</th>
                    <th class="text-center" style="width: 60px">GENERO</th>
                    <th class="text-center" style="width: 30px">TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $rs=ejecutarQuery("SELECT  * FROM detalle_requerimiento where idRequerimiento='$idRequerimiento' and estado=4"); 
                    $auxiliar=1;
                    while($row=mysqli_fetch_assoc($rs)){
                      $con=ejecutarQuery("SELECT count(*) as contador, cargo.nombre as cargo, cargo.idcargo as idcargo, departamento.nombre as departamento, estacion.nombre as estacion, estacion.idEstacion as idEstacion, detalle_requerimiento.idClinica as idClinica, genero.nombre as genero, cliente.idCliente as idCliente, cliente.nombre as cliente from detalle_requerimiento 
                          inner join cargo on cargo.idcargo=detalle_requerimiento.idcargo
                          inner join estacion on estacion.idEstacion=detalle_requerimiento.idEstacion
                          inner join departamento on departamento.idDepartamento=estacion.idDepartamento
                          inner join genero on genero.idGenero=detalle_requerimiento.idGenero
                          inner join requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
                          inner join cliente on cliente.idCliente=requerimiento.idCliente
                          where detalle_requerimiento.estado != 3 and genero.idGenero=$row[idGenero] and cargo.idcargo='$row[idcargo]' and estacion.idEstacion=$row[idEstacion] and requerimiento.idRequerimiento='$idRequerimiento' and cliente.idCliente='$aa[idCliente]' ");
                      $contador = mysqli_fetch_assoc($con);
                      if ($aa['idCliente']==2055454574 && $contador['idClinica']!=NULL) {
                        $cli=ejecutarQuery("SELECT * FROM clinica INNER JOIN detalle_requerimiento ON clinica.idClinica=detalle_requerimiento.idClinica WHERE detalle_requerimiento.idClinica=$contador[idClinica]");
                        $clini = mysqli_fetch_assoc($cli);
                      } 
                      if ($auxiliar==$contador['contador']) {
                  ?>                  
                      <tr bgcolor="white">
                            <td class="text-center" style="width: 20px"><?php echo $contador['cliente']; ?> </td>
                            <td class="text-center" style="width: 150px"><?php echo $contador['cargo']; ?> </td>
                            <td class="text-center" style="width: 150px"><?php echo $contador['departamento']; ?> </td>
                            <td class="text-center" style="width: 200px"><?php echo $contador['estacion']; ?> </td> 
                            <td class="text-center" style="width: 200px">
                              <?php 
                                if ($aa['idCliente']==2055454574 && $contador['idClinica']!=NULL) {
                                  echo $clini['nombre']; 
                                }
                                else
                                  echo "-";
                              ?>
                             </td> 
                            <td class="text-center" style="width: 60px"><?php echo $contador['genero']; ?> </td>
                            <td class="text-center" style="width: 30px"><?php echo $contador['contador']; ?> </td>                 
                      </tr>
                    <?php 
                      $auxiliar=1;  
                      }
                      else{$auxiliar++;} 
                    }
                  ?>                                    
                </tbody>
                
              </table> 

            </div>
            <div class="box-footer">
              
            </div>
            </div>
          </div>
        </div>
          
        <!-- /.row -->

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>