﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reporte de Antecedentes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-gavel"></i> Reporte Antecedentes</a></li>
        </ol>
      </section>
      <section class="content">

        <div class="row">
            <div class="col-xs-12">
              <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Tabla de Antecedentes</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <table id="soloexport" class="table-bordered table-hover">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 120px">ESTADO</th>
                      <th class="text-center" style="width: 80px">DNI</th>
                      <th class="text-center" style="width: 280px">NOMBRES</th>
                      <th class="text-center" style="width: 200px">DISTRITO</th>
                      <th class="text-center" style="width: 130px">FECHA</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT * from persona where fechaAntecedente is not null");   
                      while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, distrito.idDistrito as idDistrito from distrito WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);
                    ?>                  
                        <tr bgcolor="white">
                          <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #b32400; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> style="width: 120px"> 
                            <?php 
                              if ($row['estado']==9) {
                                echo "CON ANTECEDENTES: ".$row['tipoAntecedente'];
                              }
                              else {
                                echo "SIN ANTECEDENTES";
                              }
                             ?>                                                        
                          </td>
                          <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #b32400; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> style="width: 80px">
                            <?php echo $row['idPersona']; ?></td>
                          <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #b32400; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> style="width: 280px">
                            <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                          <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #b32400; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> style="width: 200px" >
                            <?php echo $prov['dis']; ?> </td>
                          <td class="text-center" <?php if ($row['estado']==9) { ?> style="background-color: #b32400; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> style="width: 130px">
                            <?php echo  date("d/m/Y H:i:s", strtotime($row['fechaAntecedente'])); ?> </td>
                        </tr>                        
                    <?php
                      }
                    ?>                  
                  </tbody>                
                </table> 
              </div>
            </div>
        </div>
        <!-- /.row -->
      </section>
    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>