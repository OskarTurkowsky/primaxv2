<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$idUsuario=$_SESSION['id'];
$admin=$_SESSION['admin'];

?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-eye"></i>Examen Médico</li></a>
        </ol>
      </section><br>
      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">EXAMEN MÉDICO  </h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
        <form class="form-signin" autocomplete="off" action="../php/sendExamen.php" method="POST" name="form1" enctype="multipart/form-data">
            <div class="box-body">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                      <th ></th>
                      <th class="text-center" style="min-width: 180px">OPCIONES</th>
                      <th class="text-center">DNI</th>
                      <th class="text-center" style="min-width: 180px">NOMBRES</th>
                      <th class="text-center">SEXO</th>
                      <th class="text-center">CARGO</th>
                      <th class="text-center">ESTACIÓN</th>
                      <th class="text-center">CLINICA</th>  
                      <th class="text-center">FECHA ALTA</th> 
                      <th class="text-center">FECHA EXAMEN<font color="white">--------</font></th>    
                      <th class="text-center">DEPARTAMENTO<font color="white">------</font></th>
                      <th class="text-center">PROVINCIA<font color="white">-------------</font></th>  
                      <th class="text-center">DISTRITO<font color="white">---------------</font></th>       
                      <th class="text-center">DIRECCIÓN<font color="white">-------------</font></th> 
                      <th class="text-center">FECHA NACIMIENTO<font color="white">-</font></th>
                      <th class="text-center">ESTADO CIVIL<font color="white">----------</font></th>   
                      <th>TALLA BOTAS  <font color="white">----------</font> </th>
                      <th>TALLA UNIFORME <font color="white">-----</font></th>
                      <th>TEL&Eacute;FONOS <font color="white">------------</font></th>
                      <th>SPP <font color="white">----------------------</font></th>
                      <th>FECHA SPP  <font color="white">-------------</font></th>
                      <th>CODIGO SPP  <font color="white">-----------</font></th>
                      <th>OBSERVACIONES <font color="white">-----</font></th>
                  </tr>
                </thead>

                <tbody>
                      
                  <?php if ($_SESSION['admin']=='1' || $_SESSION['id']=='46587753'){
                      $rs=ejecutarQuery("SELECT asig.idAsignado, persona.*, asig.idDetalle_requerimiento as idDet FROM (select asignado.*, max(idAsignado) 
                        from asignado group by asignado.idPersona desc) asig inner join persona on persona.idPersona=asig.idPersona
                         WHERE persona.idPersona=asig.idPersona AND persona.estado IN (8,10,11,19, 27)");
                      while($row=mysqli_fetch_assoc($rs)){
                        $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
                        $dtr = mysqli_fetch_assoc($dd);

                        $rr=ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento='$dtr[idRequerimiento]'"); 
                        $rq = mysqli_fetch_assoc($rr);
                        $idRequerimiento=$rq['idRequerimiento'];

                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);

                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
                        $spp = mysqli_fetch_assoc($sp);

                        $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
                        $cargo = mysqli_fetch_assoc($car);

                        $es= ejecutarQuery("SELECT nombre from estacion where idEstacion = '$dtr[idEstacion]' ");
                        $est = mysqli_fetch_assoc($es);

                        $cl= ejecutarQuery("SELECT nombre from clinica where idClinica = '$dtr[idClinica]' ");
                        $cli = mysqli_fetch_assoc($cl);
                      ?>   
                      <?php $idPersona=$row['idPersona'];?>            
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> >
                          <?php 
                            if (( $admin==1 || $admin==3 )&& $row['estado']!=10) {
                              ?>
                                <input type="checkbox" name="id[]"  value="<?php echo $row['idPersona'] ?>"  />
                               &nbsp;&nbsp;
                              <?php 
                            }
                           ?>
                          <?php 
                          $id[] = $row['idPersona'];
                            switch ($row['estado']) {
                              case 8:
                              case 19:
                              case 27:
                                  ?> 
                                  <a href="../php/examenMedico.php?codigo=<?php echo $idPersona;?>">
                                    <button class="btn btn-success btn-circle" type="button" title="SI EXAMEN MEDICO"><i class="fa fa-check"></i>
                                      </button>
                                  </a>
                                  <a href="#observado<?php echo $row['idPersona'];echo $dtr['idDetalle_requerimiento'];?>" data-toggle="modal">
                                      <button class="btn btn-warning btn-circle" type="button" title="OBSERVADO"><i class="fa fa-stethoscope"></i>
                                      </button>
                                  </a> 
                                  <a href="../php/noExamenMedico.php?codigo=<?php echo $idPersona;?>&idDetReq=<?php echo $dtr['idDetalle_requerimiento'];?>">
                                    <button class="btn btn-danger btn-circle" type="button" title="NO EXAMEN MEDICO "><i class="fa fa-close"></i>
                                      </button>
                                  </a>
                                  <a href="#noApto<?php echo $row['idPersona']; echo $dtr['idDetalle_requerimiento'];?>" data-toggle="modal">
                                      <button class="btn btn-info btn-circle" type="button" title="NO APTO"><i class="fa fa-ban"></i>
                                      </button>
                                  </a> 
                                  <?php
                                  break;
                              case 10:
                                  echo "PASÓ EXAMEN MÉDICO";
                                  break;
                              case 11:
                                  echo "NO PASÓ EXAMEN MÉDICO";
                                  break;    
                              case 14:
                                  echo "PASÓ EXAMEN MÉDICO";
                                  break;
                              }
                           ?>
                        </td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php 
                            if (strlen($row['idPersona'])==7) {
                              echo '0'.$row['idPersona'];
                            }
                            else
                            {
                              echo $row['idPersona'];
                            }
                           ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> >
                          <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo $gener['nombre']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo $cargo['nombre']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> >
                            <?php echo $est['nombre']; ?>
                        </td>
                        <td class="text-center" 
                          <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> > 
                            <?php echo $cli['nombre']; ?> 
                        </td>
                        <td class="text-center" 
                          <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737; color:#FFFFFF" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo date("d/m/Y", strtotime($rq['fechaAlta'])); ?> 
                        </td>
                        <td class="text-center"> <?php echo date("d/m/Y", strtotime($row['fechaExamenMedicoReal'])); ?> </td>
                        <td><?php echo $prov['dep']; ?></td>
                        <td><?php echo $prov['pro']; ?></td>
                        <td><?php echo $prov['dis']; ?></td>
                        <td><?php echo strtoupper($row['direccion']); ?></td>
                        <td><?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>       </td>
                        <td><?php echo $row['estadoCivil']; ?></td>
                        <td><?php echo $row['tallaBotas']; ?></td>
                        <td><?php echo $row['tallaUniforme']; ?></td>
                        <td><?php echo $row['telefono']; ?></td>
                        <td><?php echo $spp['nombre']; ?></td>
                        <td>
                            <?php if(date("d/m/Y", strtotime($row['fechaSpp'])) == '30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                        </td>
                        <td><?php echo $row['codigoSpp']; ?></td>
                        <td><?php echo strtoupper($row['observacion']); ?></td>
                      </tr>
                      <!--Observado Modal -->
                      <div id="observado<?php echo $row['idPersona'];echo $dtr['idDetalle_requerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/postulanteObservado.php?idPersona=<?php echo $row['idPersona'];?>&idDetReq=<?php echo $dtr['idDetalle_requerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">POSTULANTE OBSERVADO</h4>
                              </div>
                              <div class="modal-body">
                                <p><strong>POSTULANTE: </strong><?php echo $row['apellidoPaterno']." ".$row['nombres'];?></p>
                                <p><strong>DETALLE DE OBSERVACION: </strong>
                                  <input type="text" class="form-control pull-right" name="detalleObservado" style="text-transform:uppercase;" >
                                </p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnCheck" class="btn btn-success btn-circle"> <span class="glyphicon glyphicon-check"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!--NO APTO Modal -->
                      <div id="noApto<?php echo $row['idPersona']; echo $dtr['idDetalle_requerimiento'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post"  action="../php/postulanteNoApto.php?idPersona=<?php echo $row['idPersona'];?>&idDetReq=<?php echo $dtr['idDetalle_requerimiento'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">POSTULANTE NO APTO</h4>
                              </div>
                              <div class="modal-body">
                                <p><strong>NO APTO: </strong><?php echo $row['apellidoPaterno']." ".$row['nombres'];?></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnCheck" class="btn btn-success btn-circle"> <span class="glyphicon glyphicon-check"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php } } else {
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                    $abcc=mysqli_fetch_assoc($aa);   
                    $rs=ejecutarQuery("SELECT persona.*, asignado.idDetalle_requerimiento as idDet FROM persona 
                      INNER JOIN asignado ON persona.idPersona=asignado.idPersona
                      INNER JOIN usuario ON usuario.idUsuario=persona.idUsuario
                       WHERE persona.estado IN (8,10,11,19, 27) and usuario.idDistrito=$abcc[idDistrito]  group by persona.idPersona");
                      while($row=mysqli_fetch_assoc($rs)){
                        $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
                        $dtr = mysqli_fetch_assoc($dd);

                        $rr=ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento='$dtr[idRequerimiento]'"); 
                        $rq = mysqli_fetch_assoc($rr);
                        $idRequerimiento=$rq['idRequerimiento'];

                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);

                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
                        $spp = mysqli_fetch_assoc($sp);

                        $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
                        $cargo = mysqli_fetch_assoc($car);

                        $es= ejecutarQuery("SELECT nombre from estacion where idEstacion = '$dtr[idcargo]' ");
                        $est = mysqli_fetch_assoc($es);

                        $cl= ejecutarQuery("SELECT nombre from clinica where idClinica = '$dtr[idClinica]' ");
                        $cli = mysqli_fetch_assoc($cl);
                      ?>   
                      <?php $idPersona=$row['idPersona'];?>            
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php 
                            switch ($row['estado']) {
                              case 8:
                              case 19:
                              case 27:
                                  ?> 
                                  <button type="button" class="btn btn-warning" >
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                  </button> 
                                  <?php
                                  break;
                              case 10:
                                  echo "PASÓ EXAMEN MÉDICO";
                                  break;
                              case 11:
                                  echo "NO PASÓ EXAMEN MÉDICO";
                                  break; 
                              case 14:
                                  echo "PASÓ EXAMEN MÉDICO";
                                  break;   
                              }
                           ?>
                       </td>
                       <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo $row['idPersona']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> >
                          <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo $gener['nombre']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo $cargo['nombre']; ?></td>
                        <td class="text-center" <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> >
                            <?php echo $est['nombre']; ?>
                        </td>
                        <td class="text-center" 
                          <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> > 
                            <?php echo $cli['nombre']; ?> 
                        </td>
                        <td class="text-center" 
                          <?php if ($row['estado']==11) 
                          { ?> style="background-color: #cc3737" <?php } else {?> style="background-color: white"<?php } ?> > 
                          <?php echo date("d/m/Y", strtotime($rq['fechaAlta'])); ?> 
                        </td>

                        <td class="text-center"> <?php echo $row['fechaExamenMedicoReal']; ?> </td>
                        <td><?php echo $prov['dep']; ?></td>
                        <td><?php echo $prov['pro']; ?></td>
                        <td><?php echo $prov['dis']; ?></td>
                        <td><?php echo $row['direccion']; ?></td>
                        <td><?php echo date("d/m/Y", strtotime($row['fechaNacimiento'])); ?></td>
                        <td><?php echo $row['estadoCivil']; ?></td>
                        <td><?php echo $row['tallaBotas']; ?></td>
                        <td><?php echo $row['tallaUniforme']; ?></td>
                        <td><?php echo $row['telefono']; ?></td>
                        <td><?php echo $spp['nombre']; ?></td>
                        <td>
                            <?php if(date("d/m/Y", strtotime($row['fechaSpp'])) == '01/01/1970') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                        </td>
                        <td><?php echo $row['codigoSpp']; ?></td>
                        <td><?php echo $row['observacion']; ?></td>
                      </tr>
                      
                  <?php }} ?>
                </tbody>
                
              </table>
            
            </div>
        <CENTER>
            <?php if ($admin==1 || $admin==3) {
              ?>
              <button type="submit" name="btnEliminar" class="btn btn-success btn-lg" value="Enviar" >CONFIRMAR</button>
              <?php
            } ?>
            
     
        </CENTER>
          
        </form>
            <div class="box-footer">
            
            </div>
            </div>
          </div>
        </div>

        <!-- /.row -->

      </section>
      <!-- /.content -->
      

    </div>
<?php include('footer.php'); ?>