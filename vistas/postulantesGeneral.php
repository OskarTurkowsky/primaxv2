<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}


?>

<?php include('header.php'); ?>
    <script type="text/javascript">
    function validaremail(e) { // 1
        tecla = (document.all) ? e.keyCode : e.which; // 2
        if (tecla==8) return true; // 3
        patron =/[A-Za-z\s0-9@._-]/; // 4
        te = String.fromCharCode(tecla); // 5
        return patron.test(te); // 6
    }
    </script>
    <script type="text/javascript">
    function validar(e) { // 1
        tecla = (document.all) ? e.keyCode : e.which; // 2
        if (tecla==8) return true; // 3
        patron =/[A-Za-z\s]/; // 4
        te = String.fromCharCode(tecla); // 5
        return patron.test(te); // 6
    }
    </script>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Postulantes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i> Postulantes</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <!--<div class="row">
          <div class="col-xs-12">
            <div class="box box-default collapsed-box">
              <div class="box-header">
                <h3 class="box-title">Formulario de Registro</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/nuevo.php" method="POST" name="form1" enctype="multipart/form-data">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Paterno</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="paterno" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Materno</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="materno" required=""">
                        </div>                       
                      </div>
                      <div class="form-group">
                        <label>Nombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required="" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha de Nacimiento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaNacimiento" required="">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Sexo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-venus-mars"></i>
                          </div>
                          <select class="form-control pull-right" name="sexo">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM genero order by idGenero LIMIT 2");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idGenero']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>   
                      </div>  
                      <div class="form-group">
                        <label>Estado Civil</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-bullseye"></i>
                          </div>
                          <select class="form-control pull-right" name="estadoCivil">
                            <option>SOLTERO(A)</option>
                            <option>CASADO(A)</option>
                            <option>VIUDO(A)</option>
                            <option>DIVORCIADO(A)</option>
                          </select>
                        </div>   
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Tel&eacute;fono 1</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono1" class="form-control pull-right" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Tel&eacute;fono 2</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono2" class="form-control pull-right">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Tel&eacute;fono 3</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono3" class="form-control pull-right">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Correo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-envelope-o"></i>
                          </div>
                          <input type="email" name="email" class="form-control pull-right" onkeypress="return validaremail(event)" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Talla de Botas</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-arrows-h"></i>
                          </div>
                          <input type="number" name="tallaBotas" class="form-control pull-right" required>
                        </div>                        
                      </div>
                      <div class="form-group">
                        <label>Talla de Uniforme</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-child"></i>
                          </div>
                          <select name="tallaUniforme" class="form-control pull-right">
                            <option>S</option>
                            <option>M</option>
                            <option>L</option>
                            <option>XL</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Deseable nivel educativo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-graduation-cap"></i>
                          </div>
                          <select class="form-control pull-right" name="nivelEducativo">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM nivelEducativo order by idNivelEducativo");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idNivelEducativo']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>                            
                          </select>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Departamento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="departamento" id="" onchange="from(document.form1.departamento.value,'midiv','../php/provincias.php');">
                            <option>Elige una opción</option>
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Provincia</label>
                        <div class="input-group" id="midiv">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="provincia" id="provincias">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Distrito</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="distrito" id="distritos">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Direcci&oacute;n</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="direccion" required="">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Sistema Privado de Pensiones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-check"></i>
                          </div>
                          <select name="spp" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM spp order by idSpp");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idSpp']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>   
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha SPP</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaSpp">
                        </div>   
                      </div>
                      <div class="form-group">
                        <label>Codigo SPP</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-barcode"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="codigoSpp" style="text-transform:uppercase;" >
                        </div>   
                      </div>    
                    </div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <label>Observaciones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <textarea class="form-control pull-right" rows="1" style="resize: vertical;min-height: 35px; text-transform:uppercase" name="observaciones">
                          </textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <br>
                      <center> <button type="submit" class="btn btn-primary pull-center">REGISTRAR</button> </center>      
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>
        -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Postulantes</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                    <th ></th>
                    <th class="text-center" style="min-width: 170px">OPCIONES</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center" style="min-width: 200px">NOMBRES</th>
                    <th class="text-center">SEXO</th>
                    <th class="text-center">PROVINCIA</th>
                    <th class="text-center">DIRECCION </th> 
                    <th class="text-center">FECHA NACIMIENTO <font color="white">--</font></th>

                    <th class="text-center">TEL&Eacute;FONOS <font color="white">----------</font></th>
                    <th class="text-center" >ESTADO CIVIL <font color="white">--------</font></th>
                    <th>TALLA BOTAS  <font color="white">---------</font> </th>
                    <th>TALLA UNIFORME <font color="white">----</font></th>
                    <th>SPP <font color="white">---------------------</font></th>
                    <th>FECHA SPP  <font color="white">------------</font></th>
                    <th>CODIGO SPP  <font color="white">----------</font></th>
                    <th>TURNO  <font color="white">-----------------</font></th>
                    <th>QUANTUM<font color="white">-------------</font></th>
                    <th>OBSERVACIONES <font color="white">----</font></th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $idUsuario=$_SESSION['id'];
                    $admin=$_SESSION['admin'];
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                    $abcc=mysqli_fetch_assoc($aa);   
                    $rs=ejecutarQuery("SELECT * FROM persona WHERE persona.estado IN (2,4,13,15,17,18, 24) ");
                    while($row=mysqli_fetch_assoc($rs)){
                      $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                      $prov = mysqli_fetch_assoc($pro);
                      $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                      $gener = mysqli_fetch_assoc($gen);
                      $sp= ejecutarQuery("SELECT nombre from spp where idSpp='$row[idSpp]'");
                      $spp = mysqli_fetch_assoc($sp);
                      $tur= ejecutarQuery("SELECT nombre from turno where idTurno='$row[idTurno]'");
                      $turno = mysqli_fetch_assoc($tur);
                      $qua= ejecutarQuery("SELECT nombre from quantum where idQuantum='$row[idQuantum]'");
                      $quantum = mysqli_fetch_assoc($qua);

                  ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center">
                          <?php if (($row['estado']==2 || $row['estado']==13 || $row['estado']==15|| $row['estado']==24) &&  $admin!=3): ?>
                            <a href="editarPostulantes.php?id=<?php echo $row['idPersona'];?>">
                            <button title="EDITAR" type='button' class='btn btn-warning btn-circle'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                            </button>
                          </a>
                          <a href="#delete<?php echo $row['idPersona'];?>" data-toggle="modal">
                            <button title="ELIMINAR" type='button' class='btn btn-danger btn-circle'>
                              <span class='glyphicon glyphicon-trash' aria-hidden='true'>
                              </span>
                            </button>
                          </a> 
                          <?php endif ?>   

                          <?php if ($row['estado']==4 || $row['estado']==17 || $row['estado']==18 || $row['estado']==26): 
                            echo "ASIGNADO A REQUERIMIENTO";?>
                          <a href="#confirm<?php echo $row['idPersona'];?>" data-toggle="modal"><button title="CONFIRMAR" type='button' class='btn btn-success btn-circle'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button></a>
                          <?php endif ?>   

                          <?php if ( ($row['estado']==3 || $row['estado']==14 || $row['estado']==16 || $row['estado']==25) && $admin==0): ?>
                          <a href="#desconfirm<?php echo $row['idPersona'];?>" data-toggle="modal"><button title="REVERTIR" type='button' class='btn btn-info btn-circle'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span></button></a>
                          <?php endif ?>

                          <?php 
                          if ($row['estado']==2 || $row['estado']==13 || $row['estado']==15|| $row['estado']==24) {
                          ?>
                          <a href="asignar.php?id=<?php echo $row['idPersona'];?>">
                              <button class="btn btn-primary btn-circle" type="button" title="ASIGNAR REQUERIMIENTO" "><i class="fa fa-search"></i>
                              </button>
                          </a>  
                          <?php } ?>

                          <?php 
                          if (($admin==1 || $admin ==2) && $row['estado']==2) {
                          ?>
                          <a href="../php/verificarAntecedentes.php?id=<?php echo $row['idPersona'];?>">
                              <button class="btn  btn-secondary" type="button" title="ANTECEDENTES" "><i class="fa fa-gavel"></i>
                              </button>
                          </a>  
                          <?php } ?>

                       </td>
                        <td class="text-center"><?php 
                        if (strlen($row['idPersona'])==7) {
                              echo '0'.$row['idPersona'];
                            }
                            else
                            {
                              echo $row['idPersona'];
                            } ?></td>
                        <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center"><?php echo $gener['nombre']; ?></td>
                        <td class="text-center"><?php echo $prov['pro']."-".$prov['dis']; ?></td>
                        <td class="text-center"><?php echo strtoupper($row['direccion']); ?></td>
                        <td class="text-center"><?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>       </td>
                        <td class="text-center"><?php echo $row['telefono']; ?></td>
                        <td><?php echo $row['estadoCivil']; ?></td>
                        <td><?php echo $row['tallaBotas']; ?></td>
                        <td><?php echo $row['tallaUniforme']; ?></td>
                        <td><?php echo $spp['nombre']; ?></td>
                        <td>
                          <?php if(date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                        </td>
                        <td><?php if ($row['codigoSpp']==null) {
                          echo 'NO REGISTRA';
                        }else echo $row['codigoSpp']; ?></td>
                        <td><?php echo $turno['nombre']; ?></td>
                        <td><?php echo $quantum['nombre']; ?></td>
                        <td><?php echo strtoupper($row['observacion']); ?></td>
                        
                      </tr>
                       
                      <div id="delete<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarPostulante.php?id=<?php echo $row['idPersona'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                                <p>Esta seguro de eliminar Registro <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idPersona']; ?>,'example','../php/eliminarPostulante.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                        <!--Confirmar Modal -->
                      <div id="confirm<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/confirmarPostulante.php?codigo=<?php echo $row['idPersona'];?>">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CONFIRMAR POSTULANTE</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                                <p>Esta seguro de confirmar al postulante <strong><?php echo $row['apellidoPaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!--DesConfirmar Modal -->
                      <div id="desconfirm<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/desconfirmarPostulante.php?codigo=<?php echo $row['idPersona'];?>">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">REVERTIR CONFIRMACIÓN DE POSTULANTE</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de revertir al postulante <strong><?php echo $row['apellidoPaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>YES</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>

                  <?php
                    }
                  ?>
                  
                  
                </tbody>
                
              </table> 

              <div class="modal fade" id="edit" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Editar Registro</h4>
                    </div>
                    <?php $id = $_GET['codigo'] ?>
                    <h1><?php echo $id; ?></h1>
                    <form method="POST" id="edit-form"> 
                      <div class="modal-body">
                              <input type="hidden" name="id" value="" id="id">
                              
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" id="btn-editar">Guardar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> 
            </div>
            <div class="box-footer">
              
            </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>