<?php
require('../fpdf.php');
require '../php/funciones.php';
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario 				 = $_SESSION['id'];

class PDF extends FPDF
{
	// Cabecera de p�gina
	function carta($fecha, $estacion, $trabajador, $cargo, $direccion)
	{
		//-------logo y fecha alta
		// Logo de la empresa
		$this->Image('../imagenes/logo GRIS.jpg',10,15,80);
		// Arial bold 15
		$this->SetFont('Arial','',12);
		// Movernos a la derecha
		$this->Cell(80);
		// FECHA DE alta
		$this->Cell(150,30,$fecha,0,0,'C');
		// Salto de l�nea
		$this->Ln(25);


		//titulo general
		// Arial bold 15
		$this->SetFont('Arial','b',22);
		// Movernos a la derecha
		$this->Cell(85);
		// FECHA DE alta
		 $this->Cell(20,30,'CARTA DE PRESENTACI�N',0,1,'C');
	
		// Salto de l�nea
		
		$this->Line(20, 62, 210-20, 62); // 20mm from each edge
		$this->Line(50, 62, 210-50, 62); // 50mm from each edge
	  
		$this->SetDrawColor(188,188,188);
		$this->Line(20,63,210-20,63);
		$this->Ln(13);

		// estacioon
		$this->SetFont('Arial','B',14);
		$this->Cell(9);
		$this->Cell(0,0,$estacion,0,1,'L');
		$this->Ln(13);

		//TEXTO
		$this->Cell(9);
		$this->SetFont('Arial','',14);

		$this->Cell(0,0,'T-Soluciona tiene el agrado de presentar al Sr(a).:',0,1,'L');

		//NOMBRE DEL POSTULANTE
		$this->Ln(13);
		$this->Cell(80);
		$this->SetFont('Arial','B',14);
		$this->Cell(20,5,$trabajador,0,1,'C');

		$this->Ln(13);
		$this->Cell(9);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Quien ha ido evaluado a trav�s de un proceso de filtros, para posici�n de:',0,1,'L');

		//CARGO
		$this->Ln(13);
		$this->Cell(80);
		$this->SetFont('Arial','B',14);
		$this->Cell(20,5,$cargo,0,1,'C');

		//direccion y estacion
		$this->Ln(13);
		$this->Cell(9);
		$this->SetFont('Arial','',14);
		$this->MultiCell(160,5,$direccion,0,'C',false);

		//TEXTO
		$this->Ln(13);
		$this->Cell(9);
		$this->MultiCell(170,5,'De esta forma, la persona en cuesti�n culmin� satisfactoriamente el paso por nuestra consultora.',0,'J',false);
		$this->Ln(5);
		$this->Cell(9);
		$this->Cell(0,0,'Sin otro particular, agradecemos su preferencia.',0,1,'L');
		$this->Ln(10);
		$this->Cell(9);
		$this->Cell(0,0,'Atte.',0,1,'L');

		//FIRMA 
		$this->Ln(14);
		$this->Image('../imagenes/FIRMA CARMEN GRIS.jpg',72,217,75);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(0.3);
		$this->Line(63,255,210-63,255);
		$this->Ln(49);
		$this->Cell(54);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'CARMEN PALACIOS CHAFALOTE',0,1,'L');
		$this->Ln(5);
		$this->Cell(56);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Selecci�n de Personal T-Soluciona','C',1);
	}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	date_default_timezone_set('America/Lima');
	setlocale(LC_TIME, 'spanish');
	$sql = ejecutarQuery("SELECT distinct * from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
		inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona
		inner join distrito on persona.idDistrito=distrito.idDistrito
        inner join provincia on provincia.idProvincia=distrito.idProvincia
        inner join departamento on departamento.idDepartamento=provincia.idDepartamento
		where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento group by persona.idPersona");

		while($fila=mysqli_fetch_assoc($sql)){ 
			$sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
				inner join provincia on provincia.idProvincia=distrito.idProvincia
				inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
			$usuarioDept=mysqli_fetch_array($sqqqq);

			$sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
			$filaasa=mysqli_fetch_array($sq);

			//Si departamento usuario es lima (15) 
			if (
				($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
				($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
				($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
				 ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
				 ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
				 (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759) && 
				 ($fila['idDepartamento'] == 12  || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 ||  $fila['idEstacion'] == 39 || $fila['idEstacion'] ==  209 || $fila['idEstacion'] ==302 || $fila['idEstacion'] ==303 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256 || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146 || $fila['idEstacion'] == 44) ) ||
				 (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874) && 
				 ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] ==303 || $fila['idEstacion'] == 245) ) || 
				 ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
				 ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
				 ($filaasa['idDepartamento']==$usuarioDept['idDepartamento'])
				  ) {

						$departamento=strtolower($filaasa['departamento']);
						if ($filaasa['idDepartamento']==15 ||$filaasa['idDepartamento']==7) {
							$departamento='Lima';
						}

						$fecha = ucwords($departamento).', '.strftime("%e de %B del %Y", strtotime($fila['fechaAlta']));
						$trabajador = utf8_decode($fila['apellidoPaterno'].' '.$fila['apellidoMaterno'].', '.$fila['nombres']);

						$consulta = ejecutarQuery("SELECT nombre from cargo where idcargo=$fila[idcargo]");
						$getCargo = mysqli_fetch_assoc($consulta);
						$cargo=$getCargo['nombre'];
						
						$consulta1 = ejecutarQuery("SELECT * from estacion where idEstacion=$fila[idEstacion]");
						$getdireccion = mysqli_fetch_assoc($consulta1);

						$direccion='Direcci�n: '.utf8_decode($getdireccion['direccion']);
						$estacion=utf8_decode($getdireccion['nombre']);

						$pdf->AddPage();
						$pdf -> carta($fecha, $estacion, $trabajador, $cargo, $direccion);
				}			
		}

	$pdf->Output();
?>