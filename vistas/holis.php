﻿<?php
require('../fpdf.php');
require '../php/funciones.php';
$idRequerimiento=$_GET['idReq'];

class PDF extends FPDF
{
	// Cabecera de página
	function carta($fecha, $estacion, $trabajador, $cargo)
	{
		//-------logo y fecha alta
		// Logo de la empresa
		$this->Image('logo.png',10,15,80);
		// Arial bold 15
		$this->SetFont('Arial','',12);
		// Movernos a la derecha
		$this->Cell(80);
		// FECHA DE alta
		$this->Cell(150,30,$fecha,0,0,'C');
		// Salto de línea
		$this->Ln(25);


		//titulo general
		// Arial bold 15
		$this->SetFont('Arial','b',22);
		// Movernos a la derecha
		$this->Cell(85);
		// FECHA DE alta
		 $this->Cell(20,30,'CARTA DE PRESENTACIÓN',0,1,'C');
	
		// Salto de línea
		
		$this->Line(20, 62, 210-20, 62); // 20mm from each edge
		$this->Line(50, 62, 210-50, 62); // 50mm from each edge
	  
		$this->SetDrawColor(188,188,188);
		$this->Line(20,63,210-20,63);
		$this->Ln(13);

		// estacioon
		$this->SetFont('Arial','B',14);
		$this->Cell(9);
		$this->Cell(0,0,$estacion,0,1,'L');
		$this->Ln(13);

		//TEXTO
		$this->Cell(9);
		$this->SetFont('Arial','',14);

		$this->Cell(0,0,'T-Soluciona tiene el agrado de presentar al Sr.:',0,1,'L');

		//NOMBRE DEL POSTULANTE
		$this->Ln(13);
		$this->Cell(80);
		$this->SetFont('Arial','B',14);
		$this->Cell(20,5,$trabajador,0,1,'C');

		$this->Ln(13);
		$this->Cell(9);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Quien ha ido evaluado a través de un proceso de filtros, para posición de:',0,1,'L');

		//CARGO
		$this->Ln(13);
		$this->Cell(80);
		$this->SetFont('Arial','B',14);
		$this->Cell(20,5,$cargo,0,1,'C');

		//direccion y estacion
		$this->Ln(13);
		$this->Cell(9);
		$this->SetFont('Arial','',14);
		$this->MultiCell(151,5,'Dirección: Av. Canada Cra.11 con Victor Alzamora - LA VICTORIA / E/S CANADA',0,'C',false);

		//TEXTO
		$this->Ln(13);
		$this->Cell(9);
		$this->MultiCell(170,5,'De esta forma, la persona en cuestión culminó satisfactoriament el paso por nuestra consultora y es considerada APTA.',0,'J',false);
		$this->Ln(5);
		$this->Cell(9);
		$this->Cell(0,0,'Sin otro particular, agradecemos su preferencia.',0,1,'L');
		$this->Ln(10);
		$this->Cell(9);
		$this->Cell(0,0,'Atte.',0,1,'L');

		//FIRMA 
		$this->Ln(10);
		$this->Image('FIRMA CARMEN.png',72,217,75);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(0.3);
		$this->Line(63,255,210-63,255);
		$this->Ln(49);
		$this->Cell(54);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'CARMEN PALACIOS CHAFALOTE',0,1,'L');
		$this->Ln(5);
		$this->Cell(56);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Selección de Personal T-Soluciona','C',1);
	}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$estacion = 'E/S TAVIRSA:';
$trabajador = 'LA TORRE GUERRERO, JORGE LUIS';
$cargo ='VENDEDOR PLAYA';
//date_default_timezone_set('America/Lima');
//setlocale(LC_TIME, 'spanish');
$date = '2018-10-19';
$sql = ejecutarQuery("SELECT distinct * from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
	inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona
	where persona.estado=12 and requerimiento.idRequerimiento=$idRequerimiento");
	while($fila=mysqli_fetch_assoc($sql)){ 
		$fecha = 'Lima, '.strftime("%e de %B del %Y", strtotime($fila['fechaAlta']));
		$trabajador = $fila['apellidoPaterno'].' '.$fila['apellidoMaterno'].', '.$fila['nombres'];
		$pdf->AddPage();
		$pdf -> carta($fecha, $estacion, $trabajador, $cargo);
	}
$pdf->Output();
?>
