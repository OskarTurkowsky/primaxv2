<?php
require '../php/funciones.php';
if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario         = $_SESSION['id'];
?>

<?php include('header.php'); ?>

<!-- CONTENIDO DE LA PAGINA -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Antecedentes
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-eye"></i> Antecedentes</a></li>
    </ol>
  </section><br>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title"> Tabla de datos para Antecedentes </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  
          </div>
          <form class="form-signin" autocomplete="off" action="prueba.php?idReq=<?php echo $idRequerimiento;?>" method="POST" name="form1" enctype="multipart/form-data">
          <div class="box-body">
            <table id="solotable" class="table-bordered table-hover">
              <thead>
                    <tr>
                      <th class="text-center" >DNI</th>
                      <th class="text-center" >POSTULANTE</th>
                      <th class="text-center" >TURNO</th>          
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT distinct * from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona inner join distrito on persona.idDistrito=distrito.idDistrito
                        inner join provincia on provincia.idProvincia=distrito.idProvincia
                        inner join departamento on departamento.idDepartamento=provincia.idDepartamento where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento order by persona.nombres");
                    while($fila=mysqli_fetch_assoc($rs)){
                      $sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
                      inner join provincia on provincia.idProvincia=distrito.idProvincia
                      inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
                    $usuarioDept=mysqli_fetch_array($sqqqq);

                    $sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
                    $filaasa=mysqli_fetch_array($sq);

                    if (
                      ($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
                      ($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
                      ($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
                       ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
                       ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
                       (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759) && 
                       ( $fila['idDepartamento'] == 12  || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 ||  $fila['idEstacion'] == 39 || $fila['idEstacion'] ==  209 || $fila['idEstacion'] ==302 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256 || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146 || $fila['idEstacion'] == 44) ) ||
                       (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874) && 
                       ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] == 245) ) || 
                       ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
                       ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
                       ($filaasa['idDepartamento']==$usuarioDept['idDepartamento'])
                    ) {   
                ?>
                  <tr>
                    <td class="text-center">
                      <?php echo $fila['idPersona']; ?>
                      <input type="checkbox" hidden name="id[]" checked value="<?php echo $fila['idPersona'] ?>"  />
                    <?php 
                      $id[] = $fila['idPersona'];
                    ?> 
                    </td>
                    <td class="text-center">
                    <?php echo $fila['nombres'].' '.$fila['apellidoPaterno'].' '.$fila['apellidoMaterno']; ?>
                    </td>
                    <td class="text-center">
                      <select name="turno[]" class="form-control pull-right">
                        <?php 
                            $consulta = ejecutarQuery("SELECT * FROM turno_carta");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idTurno_carta']; ?>" 
                                <?php if ($eee['idTurno_carta']==$fila['idTurno_carta']) {
                                  echo "selected='selected'";
                                } 
                                ?>
                                >
                                <?php echo $eee['nombre']; ?>
                              </OPTION>    
                            <?php
                            }
                            ?> 
                      </select>
                    </td>
                  </tr>
                  <?php
                      }
                      }
                    ?>
              </tbody>
            </table>
          </div>
          <CENTER>
            <button type="submit" name="btnEliminar" class="btn btn-success btn-lg" value="Enviar" >CONFIRMAR</button>
          </CENTER>
          
          </form>
          <div class="box-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- /.row -->

  </section>
  <!-- /.content -->
  

</div>
<!-- FIN DEL CONTENIDO DE LA PAGINA-->


<?php include('footer.php'); ?>