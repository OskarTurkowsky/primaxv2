﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario         = $_SESSION['id'];
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Check List
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i>Formatos</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Requerimientos</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
            <div class="box-body" id="notall" >
              <table id="solotable" class="table-bordered table-hover">
                <thead>
                    <tr>
                      <th class="text-center" >DNI</th>
                      <th class="text-center" >POSTULANTE</th>
                      <th class="text-center" >DETALLE</th>
                      <th class="text-center" >OPCIONES</th>           
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT distinct * from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona inner join distrito on persona.idDistrito=distrito.idDistrito
                        inner join provincia on provincia.idProvincia=distrito.idProvincia
                        inner join departamento on departamento.idDepartamento=provincia.idDepartamento where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento order by persona.nombres");
                    while($fila=mysqli_fetch_assoc($rs)){
                      $sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
                      inner join provincia on provincia.idProvincia=distrito.idProvincia
                      inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
                    $usuarioDept=mysqli_fetch_array($sqqqq);

                    $sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
                    $filaasa=mysqli_fetch_array($sq);

                    if (
                      ($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
                      ($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
                      ($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
                       ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
                       ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
                       (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759 || $usuario== 15359362) && 
                       ( $fila['idDepartamento'] == 12  || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 ||  $fila['idEstacion'] == 39 || $fila['idEstacion'] ==  209 || $fila['idEstacion'] ==302 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256 || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146 || $fila['idEstacion'] == 44) ) ||
                       (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874) && 
                       ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] == 245) ) || 
                       ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
                       ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
                       ($filaasa['idDepartamento']==$usuarioDept['idDepartamento'])
                    ) {       
                    ?>         
                      <tr bgcolor="white">
                        <td class="text-center">
                          <?php echo $fila['idPersona']; ?>
                        </td>
                        <td class="text-center">
                          <?php echo $fila['nombres'].' '.$fila['apellidoPaterno'].' '.$fila['apellidoMaterno']; ?>
                        </td>
                        <td class="text-center">
                          <?php echo '01 COPIA DE HIJOS (-18 años) Y/O CONYUGUE (Partida de Matrimonio)'.': ';
                            if ($fila['hijo_conyugue']==null || $fila['hijo_conyugue']=='NO') {
                              echo "NO POSEE";
                            }
                            else
                              echo "SI POSEE";
                            ?>
                        </td>
                        <td class="text-center"> 
                          <a href="../php/poseeCopia.php?codigo=<?php echo $fila['idPersona'];?>">
                              <button class="btn btn-success btn-circle" type="button" title="SI POSEE"><i class="fa fa-check"></i>
                              </button>
                          </a>
                          <a href="../php/noPoseeCopia.php?codigo=<?php echo $fila['idPersona'];?>">
                              <button class="btn btn-danger btn-circle" type="button" title="NO POSEE"><i class="fa fa-close"></i>
                              </button>
                          </a>
                        </td>
                      </tr>
                    <?php
                      }
                      }
                    ?>
                </tbody>
                </table> 
                <a href="checklist.php?idReq=<?php echo $idRequerimiento;?>">
                  <center>
                    <button class="btn btn-primary btn-lg" form="large" type="button"> DESCARGAR CHECK LIST
                    </button>
                  </center>
                </a>
            </div>

              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>