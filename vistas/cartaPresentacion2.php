<?php
require('../fpdf.php');
require '../php/funciones.php';
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario 				 = $_SESSION['id'];

class PDF extends FPDF
{
	// Cabecera de p�gina
	function carta($fecha, $estacion, $trabajador, $cargo, $direccion, $cliente, $turno)
	{
		$this->Image('../imagenes/logo GRIS.jpg',22,20,80);
		$this->SetFont('Arial','',12);
		$this->Cell(0,0,$fecha,0,0,'R');
		$this->Ln(25);

		$this->SetFont('Arial','b',16);
		$this->Cell(0,0,$estacion,0,0,'L');
		$this->Ln(4.5);
		$this->SetFont('Arial','',12);
		$this->MultiCell(160,5,$direccion,0,'L',false);
		
		$this->Ln(10);
		$this->Cell(0,0,"Se�ores:",0,0,'L');
		$this->SetFont('Arial','B',13);
		$this->Ln(8);
		$this->Cell(0,0,$cliente,0,0,'L');
		$this->SetFont('Arial','',12);
		$this->Ln(8);
		$this->Cell(0,0,"Presente.-",0,0,'L');
		$this->Ln(12);
		$this->Cell(0,0,"Estimados Se�ores:",0,0,'L');
		$this->Ln(10);
		$this->Cell(0,0,"Por medio de la presente, tenemos el agrado de presentar a ",0,0,'L');
		$this->SetFont('Arial','B',16);
		$this->Ln(12);
		$this->Cell(0,0,$trabajador,0,1,'C');

		$this->SetFont('Arial','',12);
		$this->Ln(12);
		$this->Cell(0,0,"Quien ha sido evaluado para la posici�n de: ",0,1,'L');

		$this->SetFont('Arial','B',16);
		$this->Ln(12);
		$this->Cell(0,0,$cargo,0,1,'C');

		$this->SetFont('Arial','',12);
		$this->Ln(10);
		$this->Cell(0,0,"Aceptando el siguiente horario: ",0,1,'L');

		$this->SetFont('Arial','B',16);
		$this->Ln(12);
		$this->Cell(0,0,$turno." *",0,1,'C');

		$this->SetFont('Arial','',12);
		$this->Ln(9);
		$this->Cell(0,0,"Sin otro particular, agradecemos su atenci�n",0,1,'L');

		$this->SetFont('Arial','',9);
		$this->Ln(10);
		$this->Cell(0,0,"* El horario podr� variar de acuerdo a la necesidad de la operaci�n, previa indicaci�n del Administrador",0,1,'L');

		$this->SetFont('Arial','',12);
		$this->Ln(14);
		$this->Cell(0,0,"Atentamente",0,1,'L');

		$this->Ln(10);
		$this->Image('../imagenes/FIRMA CARMEN GRIS.jpg',72,217,75);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(0.3);
		$this->Line(63,255,210-63,255);
		$this->Ln(46);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'CARMEN PALACIOS CHAFALOTE',0,1,'C');
		$this->Ln(5.5);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Selecci�n de Personal T-Soluciona',0,1,'C');
	}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF('P','mm','A4');
	$pdf->setMargins(30, 30, 30);
	$pdf->AliasNbPages();
	date_default_timezone_set('America/Lima');
	setlocale(LC_TIME, 'spanish');
	$sql = ejecutarQuery("SELECT distinct *, requerimiento.idCliente as idCliente, detalle_requerimiento.idTurno_carta as turno from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
		inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona
		inner join distrito on persona.idDistrito=distrito.idDistrito
        inner join provincia on provincia.idProvincia=distrito.idProvincia
        inner join departamento on departamento.idDepartamento=provincia.idDepartamento
		where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento group by persona.idPersona");

		while($fila=mysqli_fetch_assoc($sql)){ 
			$sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
				inner join provincia on provincia.idProvincia=distrito.idProvincia
				inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
			$usuarioDept=mysqli_fetch_array($sqqqq);

			$sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
			$filaasa=mysqli_fetch_array($sq);

			$cl = ejecutarQuery("SELECT  cliente.nombre as cliente from cliente where idCliente=$fila[idCliente]");
			$clien=mysqli_fetch_array($cl);

			//Si departamento usuario es lima (15) 
			if (
				($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
				($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
				($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
				 ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
				 ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
				 (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759 || $usuario== 15359362) && 
				 ($fila['idDepartamento'] == 12  || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 ||  $fila['idEstacion'] == 39 || $fila['idEstacion'] ==  209 || $fila['idEstacion'] ==302 || $fila['idEstacion'] ==303 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256 || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146 || $fila['idEstacion'] == 44) ) ||
				 (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874 || $usuario==46534692 ) && 
				 ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] ==303 || $fila['idEstacion'] == 245) ) || 
				 ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
				 ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
				 ($filaasa['idDepartamento']==$usuarioDept['idDepartamento'])
				  ) {

						$departamento=strtolower($filaasa['departamento']);
						if ($filaasa['idDepartamento']==15 ||$filaasa['idDepartamento']==7) {
							$departamento='Lima';
						}

						$fecha = strftime("%e de %B del %Y", strtotime($fila['fechaAlta']));
						$trabajador = utf8_decode($fila['apellidoPaterno'].' '.$fila['apellidoMaterno'].', '.$fila['nombres']);

						$consulta = ejecutarQuery("SELECT nombre from cargo where idcargo=$fila[idcargo]");
						$getCargo = mysqli_fetch_assoc($consulta);
						$cargo=$getCargo['nombre'];
						
						$consulta1 = ejecutarQuery("SELECT * from estacion where idEstacion=$fila[idEstacion]");
						$getdireccion = mysqli_fetch_assoc($consulta1);

						$direccion=utf8_decode($getdireccion['direccion']);
						$estacion=utf8_decode($getdireccion['nombre']);

				if ($clien['cliente']=='PRIMAX') {
					$cliente='COESTI S.A.';
				}
				else
				{
					$cliente='PERUANA DE ESTACIONES DE SERVICIO S.A.C.';
				}
				$consulta2 = ejecutarQuery("SELECT nombre FROM turno_carta where idTurno_carta=$fila[turno]");
				$turn=mysqli_fetch_assoc($consulta2);
				$turno=utf8_decode($turn['nombre']);

						$pdf->AddPage();
						$pdf -> carta($fecha, $estacion, $trabajador, $cargo, $direccion, $cliente,$turno);
				}			
		}

	$pdf->Output();
?>