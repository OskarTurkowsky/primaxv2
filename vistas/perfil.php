<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>

<div class="content-wrapper">

	<section class="content-header">
        <h1>
          Bienvenido
          <small><?php echo $_SESSION['nombres'];?></small>
    	</h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-eye"></i> Perfil</a></li>
          <li class="active">T-Soluciona</li>
        </ol>
    </section>
    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-default">
          	<div class="box-header">
              <h3 class="box-title">Perfil del Usuario</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
          	</div>
        		<div class="box-body">
         			<div class="row">
            			<form class="form-signin" action="../php/perfil.php" method="POST" enctype="multipart/form-data">
            				<div class="col-md-4">
            					<div class="form-group">
                        <label>Nombres:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-user-circle-o"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="nombres" required="" value="<?php echo $_SESSION['nombres'];?>">
                        </div>
                    	</div>
                      <div class="form-group">
                        <label>Correo:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                          </div>
                          <input type="email" class="form-control pull-right" name="correo" required="" value="<?php echo $_SESSION['correo'];?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Cargo:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-get-pocket"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="cargo" required="" value="<?php echo $_SESSION['cargo'];?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Tipo de Cuenta:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-desktop"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="tipo" required="" value="Usuario" readonly="">
                        </div>
                      </div>
            				</div>
            				<div class="col-md-4">
            					<div class="form-group">
                        <label>Imagen actual:</label>
                        <center>
                          <img src="<?php echo $_SESSION['img'];?>" width="160" height="160">
                        </center>
                      </div><br style="line-height: 22px">
                      <div class="form-group">
                        <label>Cambiar imagen:</label>  
                        <div class="input-group">
                          <input type="file" name="imagen" class="form-control" accept="image/*">
                        </div>
                        <p class="help-block">*Cargar solos imagenes de 160px por 160px.</p>
                      </div>
            				</div>
            				<div class="col-md-4">
                      <br><br><br><br><br>
                      <center>
            				    <button type="submit" class="btn btn-primary">GUARDAR</button>   	
                      </center>
            				</div>
									</form>                
          		</div>
        		</div>
        		<div class="box-footer"></div>
          </div>
    		</div>
        <div class="col-xs-12">
          <div class="box box-default">
            <div class="box-header">
              <h3 class="box-title">Cambiar Contraseña</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                  <form class="form-signin" action="../php/contraseña.php" method="POST" enctype="multipart/form-data">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Contraseña actual:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                          </div>
                          <input type="password" class="form-control pull-right" name="actual" required="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Contraseña nueva:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                          </div>
                          <input type="password" class="form-control pull-right" name="nuevo" required="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Confirmar nueva contraseña:</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                          </div>
                          <input type="password" class="form-control pull-right" name="confirmarnuevo" required="" >
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary pull-right">GUARDAR</button>       
                    </div>
                  </form>                
              </div>
            </div>
            <div class="box-footer"></div>
          </div>
        </div>
    	</div>
    </section>

</div>

<?php include('footer.php'); ?>