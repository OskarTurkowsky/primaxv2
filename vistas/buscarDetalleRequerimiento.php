<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$detRequerimiento=$_GET['idDet'];
$idRequerimiento=$_GET['idReq'];

?>

<?php include('header.php'); ?>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1> Asignar Postulante </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i>Requerimientos</a></li>
          <li class="active">Asignar postulante</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Postulantes</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                    <th ></th>
                    <th class="text-center">OPCIONES</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center" style="min-width: 200px">NOMBRES</th>
                    <th class="text-center">FECHA NACIMIENTO</th>
                    <th class="text-center">SEXO</th>
                    <th class="text-center">DEPARTAMENTO</th>
                    <th class="text-center">PROVINCIA</th> 

                    <th class="text-center">DISTRITO<font color="white">--------------</font></th>
                    <th class="text-center">DIRECCIÓN<font color="white">------------</font></th>                    
                    <th class="text-center">TEL&Eacute;FONOS <font color="white">----------</font></th>
                    <th class="text-center" >ESTADO CIVIL <font color="white">--------</font></th>
                    <th>TALLA BOTAS  <font color="white">---------</font> </th>
                    <th>TALLA UNIFORME <font color="white">----</font></th>
                    <th>SPP <font color="white">---------------------</font></th>
                    <th>FECHA SPP  <font color="white">------------</font></th>
                    <th>CODIGO SPP  <font color="white">----------</font></th>
                    <th>OBSERVACIONES <font color="white">----</font></th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $idUsuario=$_SESSION['id'];
                    $consulta = ejecutarQuery("SELECT * FROM detalle_requerimiento where idDetalle_requerimiento=$detRequerimiento");
                    $result = mysqli_fetch_assoc($consulta);
                    $estadoDet=$result['estado'];
                    $genero=$result['idGenero'];
                    $estacion=$result['idEstacion'];
                    $admin=$_SESSION['admin'];
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                    $abcc=mysqli_fetch_assoc($aa); 

                    $consulta2 = ejecutarQuery("SELECT departamento.idDepartamento as idDepartamento FROM departamento INNER JOIN estacion ON estacion.idDepartamento=departamento.idDepartamento where estacion.idEstacion=$estacion");
                    $resulta = mysqli_fetch_assoc($consulta2);

                    $depto=$resulta['idDepartamento'];

                    $consulta3 = ejecutarQuery("SELECT idCliente from requerimiento where idRequerimiento=$idRequerimiento");
                    $rpt = mysqli_fetch_assoc($consulta3);

                    $consulta4 = ejecutarQuery("SELECT nombre from cliente where idCliente=$rpt[idCliente]");
                    $rpta = mysqli_fetch_assoc($consulta4);

                    if ($rpta['nombre']=='PRIMAX') {
                      //SI LA ESTACION ES
                      // ARGENTINA  | CASTAÑOS | DUEÑAS  | CARMELO | FLORA | GRANDA | HUIRACOCHA | IGARSA | MARINA | MEJIA | PANDO | SALAVERRY | LA PAZ | PERSHING | QUILCA
                      if ($estacion==105 || $estacion==144 || $estacion==111 || $estacion==112 || $estacion==141 || $estacion==195 || $estacion==115 || $estacion==116 || $estacion==204 || $estacion==185 || $estacion==119 || $estacion==122 || $estacion==118 || $estacion==120 || $estacion==121) 
                      {
                        if ($estadoDet==0) {
                          if ($genero==3) {
                            if ($admin==2) {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=7
                                inner join estacion on estacion.idDepartamento=15
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                inner join usuario on usuario.idUsuario=persona.idUsuario
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and persona.idDistrito=estacion_distrito.idDistrito and usuario.idDistrito=$abcc[idDistrito] "); 
                            }
                            else{
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=7
                                inner join estacion on estacion.idDepartamento=15
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and persona.idDistrito=estacion_distrito.idDistrito and persona.idUsuario=$idUsuario "); 
                            }                          
                          }
                          else{
                            if ($admin==2) {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=7
                                inner join estacion on estacion.idDepartamento=15
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                inner join usuario on usuario.idUsuario=persona.idUsuario
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and persona.idDistrito=estacion_distrito.idDistrito and usuario.idDistrito=$abcc[idDistrito] "); 
                            }
                            else
                            {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=7
                                inner join estacion on estacion.idDepartamento=15
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and persona.idDistrito=estacion_distrito.idDistrito and persona.idUsuario=$idUsuario"); 
                            }
                          }
                        }                    
                        else
                        {
                          if ($genero==3) 
                          {
                            if ($admin==2) 
                            {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=7 inner join estacion on estacion.idDepartamento=15 inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion inner join usuario on usuario.idUsuario=persona.idUsuario WHERE detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2) and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito] ");   
                            }
                            else
                            {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=7 inner join estacion on estacion.idDepartamento=15 inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion WHERE detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2) and persona.idUsuario=$idUsuario  and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona");   
                            }
                          }
                          else
                          {
                            if ($admin==2) 
                            {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=7 inner join estacion on estacion.idDepartamento=15 inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion inner join usuario on usuario.idUsuario=persona.idUsuario WHERE detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito]"); 
                            }
                            else
                            {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=7 inner join estacion on estacion.idDepartamento=15 inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion WHERE detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and persona.idUsuario=$idUsuario  and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona"); 
                            }
                          }                         
                        } 
                      }
                      else
                      {
                        if ($depto==15) {
                          if ($estadoDet==0) {
                            if ($genero==3) {
                              if ($admin==2) {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                inner join usuario on usuario.idUsuario=persona.idUsuario 
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and persona.idDistrito=estacion_distrito.idDistrito and usuario.idDistrito=$abcc[idDistrito]"); 
                              }
                              else
                              {
                              $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and persona.idUsuario=$idUsuario and persona.idDistrito=estacion_distrito.idDistrito");  
                                 }
                            }
                            else
                            {
                              if ($admin==2) {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                 inner join usuario on usuario.idUsuario=persona.idUsuario 
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and persona.idDistrito=estacion_distrito.idDistrito and usuario.idDistrito=$abcc[idDistrito]");
                              }
                              else
                              {
                                 $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and persona.idUsuario=$idUsuario and persona.idDistrito=estacion_distrito.idDistrito"); 
                              }                           
                            }
                          }
                          else{
                                if ($genero==3) {
                                  if ($admin==2) {
                                    $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion inner join usuario on usuario.idUsuario=persona.idUsuario where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2) and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito]");  
                                  }
                                  else
                                  {
                                  $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2) and persona.idUsuario=$idUsuario and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona");  
                                  } 
                                }
                                else{
                                  if ($admin==2) {
                                    $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion inner join usuario on usuario.idUsuario=persona.idUsuario where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito]"); 
                                  }
                                  else
                                  {
                                    $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and persona.idUsuario=$idUsuario and persona.idDistrito=estacion_distrito.idDistrito and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona"); 
                                  }                                
                                }                                          
                          }  
                        }
                        else
                        {
                          if ($estadoDet==0) {
                            if ($genero==3) {
                              if ($admin==2) {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                  inner join distrito on persona.idDistrito=distrito.idDistrito 
                                  inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                  inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                  inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                  inner join usuario on usuario.idUsuario=persona.idUsuario 
                                  where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and usuario.idDistrito=$abcc[idDistrito] "); 
                              }
                              else
                              {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                  inner join distrito on persona.idDistrito=distrito.idDistrito 
                                  inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                  inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                  inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                  where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and persona.idUsuario=$idUsuario");  
                                  } 
                            }
                            else
                            {
                              if ($admin==2) {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                  inner join distrito on persona.idDistrito=distrito.idDistrito 
                                  inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                  inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                  inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                  inner join usuario on usuario.idUsuario=persona.idUsuario 
                                  where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and usuario.idDistrito=$abcc[idDistrito] "); 
                              }
                              else
                              {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                  inner join distrito on persona.idDistrito=distrito.idDistrito 
                                  inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                  inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                  inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                  where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and persona.idUsuario=$idUsuario"); 
                              }
                                
                            }
                          }
                          else{
                              if ($genero==3) {
                                if ($admin==2) {
                                  $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join usuario on usuario.idUsuario=persona.idUsuario where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2)  and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito]");
                                }
                                else
                                {
                                  $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2) and persona.idUsuario=$idUsuario and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona");
                                }
                                     
                              }
                              
                              else
                              {
                                if ($admin==2) {
                                  $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join usuario on usuario.idUsuario=persona.idUsuario where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito] "); 
                                }
                                else
                                {
                                  $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and persona.idUsuario=$idUsuario and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona");
                                } 
                              }                                          
                          }  
                        }                     
                      } 
                    }
                    if ($rpta['nombre']=='PECSA') {
                      if ($estadoDet==0) {
                          if ($genero==3) {
                            $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                inner join usuario on usuario.idUsuario=persona.idUsuario
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero IN (1,2) and usuario.idDistrito=$abcc[idDistrito]");  
                          }
                          else
                          {
                            $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero 
                                inner join distrito on persona.idDistrito=distrito.idDistrito 
                                inner join provincia on provincia.idProvincia=distrito.idProvincia 
                                inner join departamento on departamento.idDepartamento=provincia.idDepartamento
                                inner join estacion on estacion.idDepartamento=departamento.idDepartamento
                                inner join usuario on usuario.idUsuario=persona.idUsuario
                                where estacion.idEstacion=$estacion and persona.estado IN (2,13,15) and genero.idGenero=$genero and usuario.idDistrito=$abcc[idDistrito]"); 
                          }
                        }
                        else{
                            if ($genero==3) {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join usuario on usuario.idUsuario=persona.idUsuario where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero IN (1,2) and persona.idUsuario=$idUsuario and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito]");
                            }
                            else
                            {
                                $rs=ejecutarQuery("SELECT persona.* FROM persona inner join genero on persona.idGenero=genero.idGenero inner join distrito on persona.idDistrito=distrito.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento inner join detalle_requerimiento on detalle_requerimiento.idEstacion=estacion.idEstacion inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join usuario on usuario.idUsuario=persona.idUsuario where detalle_requerimiento.idEstacion=$estacion and persona.estado IN (3,14,16) and genero.idGenero=$genero and persona.idUsuario=$idUsuario and asignado.idDetalle_requerimiento=$detRequerimiento and persona.idPersona=asignado.idPersona and usuario.idDistrito=$abcc[idDistrito]");
                            }                                          
                        } 
                    }
                    while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT departamento.nombre as dep, distrito.nombre as dis, provincia.nombre as pro FROM departamento inner join provincia on provincia.idDepartamento=departamento.idDepartamento inner join distrito on distrito.idProvincia=provincia.idProvincia where idDistrito='$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);
                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);
                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp='$row[idSpp]'");
                        $spp = mysqli_fetch_assoc($sp);
                      
                  ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center">
                          <?php 
                            if ($row['estado']==2 || $row['estado']==13 || $row['estado']==15) {
                              ?>
                              <a href="#asignar<?php echo $row['idPersona']; echo $detRequerimiento; echo $idRequerimiento?>" data-toggle="modal">
                                <button class="btn btn-success btn-sm" type="button" title="ASIGNAR" >
                                  <i class="fa fa-check"></i>
                                </button>
                              </a>
                              <?php 
                            }
                            else{
                              ?>
                                <a href="#desconfirm<?php echo $row['idPersona']; echo $detRequerimiento; echo $idRequerimiento?>" data-toggle="modal"><button title="REVERTIR" type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span></button></a>
                              <?php 
                            }
                           ?>
                        </td>
                        <td class="text-center"><?php echo $row['idPersona']; ?></td>
                        <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center"><?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>  </td>
                        <td class="text-center"><?php echo $gener['nombre']; ?></td>
                        <td class="text-center"><?php echo $prov['dep']; ?></td>
                        <td class="text-center"><?php echo $prov['pro']; ?></td>
                        <td class="text-center"><?php echo $prov['dis']; ?></td>
                        <td class="text-center"><?php echo strtoupper($row['direccion']); ?></td>
                        
                        <td class="text-center"><?php echo $row['telefono']; ?></td>
                        <td><?php echo $row['estadoCivil']; ?></td>
                        <td><?php echo $row['tallaBotas']; ?></td>
                        <td><?php echo $row['tallaUniforme']; ?></td>
                        <td><?php echo $spp['nombre']; ?></td>
                        <td>
                          <?php if(date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                        </td>
                        <td><?php echo $row['codigoSpp']; ?></td>
                        <td><?php echo strtoupper($row['observacion']); ?></td>
                      </tr>
                       
                        <!--Confirmar Modal -->
                      <div id="asignar<?php echo $row['idPersona']; echo $detRequerimiento; echo $idRequerimiento;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/asignarBuscarDetalleRequerimiento.php?codigoPersona=<?php echo $row['idPersona'];?>&detReq=<?php echo $detRequerimiento;?>&idDet=<?php echo $idRequerimiento;?>">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CONFIRMAR POSTULANTE</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigoPersona; ?>">
                                <p>Esta seguro de asignar al postulante <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!--DesConfirmar Modal -->
                      <div id="desconfirm<?php echo $row['idPersona']; echo $detRequerimiento; echo $idRequerimiento;?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/revertirPostulanteRequerimiento.php?codigo=<?php echo $row['idPersona'];?>&idDet=<?php echo $detRequerimiento;?>&idReq=<?php echo $idRequerimiento;?>">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">REVERTIR CONFIRMACIÓN DE POSTULANTE</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de revertir al postulante <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>YES</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php
                    }
                  ?>
                </tbody>
              </table> 
            </div>
            <div class="box-footer">
              
            </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    
<?php include('footer.php'); ?>