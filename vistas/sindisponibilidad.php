<?php
require '../php/funciones.php';


if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$nombrepsicologo = $_SESSION['nombres'];
?>


<?php include('header.php'); ?>
    

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Back Up
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-address-book-o"></i> Back-Up</a></li>
          <li class="active">Buscar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Registros</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="buscar" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th style="min-width: 100px">OPCIONES</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center" >NOMBRES</th>
                    <th class="text-center">DIRECCION</th>
                    <th class="text-center">TELEFONO</th>
                    <th class="text-center">CITADO POR</th>
                    <th class="text-center">FECHA EVALUACION</th>
                    <th class="text-center">OBSERVACIONES</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $idUsuario=$_SESSION['id'];
                    $admin=$_SESSION['admin'];
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                    $abcc=mysqli_fetch_assoc($aa);  
                    
                    if ($admin==1 || $admin==3) {
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup where backup.disponibilidad='NO' and persona.estado=0 "); 
                    }
                    else
                    if ($admin==2)
                    {
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup inner join usuario on usuario.idUsuario=persona.idUsuario where backup.disponibilidad='NO' and persona.estado=0 and usuario.idDistrito=$abcc[idDistrito]"); 
                    }  
                    else
                    if ($admin==0)
                    {
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup inner join usuario on usuario.idUsuario=persona.idUsuario where backup.disponibilidad='NO' and persona.estado=0 and persona.idUsuario=$idUsuario"); 
                    }                     
                    while($row=mysqli_fetch_assoc($rs)){
                      $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                      $prov = mysqli_fetch_assoc($pro);    
                      $co= ejecutarQuery("SELECT nombre from consultor where idConsultor='$row[idConsultor]'");
                      $cons = mysqli_fetch_assoc($co);                
                  ?>                
                      <tr>
                          <td class="text-center">
                          <a href="editarBackup.php?id=<?php echo $row['idPersona'];?>">
                            <button type='button' title="EDITAR" class='btn btn-warning btn-sm'>
                              <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                            </button>
                          </a>
                          <a href="#delete<?php echo $row['idPersona'];?>" data-toggle="modal"><button type='button' title="ELIMINAR" class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>
                          </a>
                        </td>
                          <td class="text-center">
                            <?php 
                              if (strlen($row['idPersona'])==7) {
                                echo '0'.$row['idPersona'];
                              }
                              else
                              {
                                echo $row['idPersona'];
                              }
                             ?>
                          </td>
                          <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                          <td><?php echo $prov['pro']."-".$prov['dis']; ?></td>
                          <td><?php echo $row['telefono']; ?></td>
                          <td><?php echo $cons['nombre']; ?></td>
                          <td><?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                          <td><?php echo strtoupper($row['observacion']); ?></td>
                      </tr>

                      <div id="delete<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarBackup3.php?id=<?php echo $row['idPersona'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                                <p>Esta seguro de eliminar Registro <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idPersona']; ?>,'example','../php/eliminarBackup3.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>OPCIONES</th>
                    <th>DNI</th>
                    <th>NOMBRES</th>
                    <th>DIRECCION</th>
                    <th>TELEFONO</th>
                    <th>CITADO POR</th>
                    <th>FECHA EVALUACION</th>
                    <th>OBSERVACIONES</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="box-footer">
            </div>
          </div>
        </div>
      </div>
          
      <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>