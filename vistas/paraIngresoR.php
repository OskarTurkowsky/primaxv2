<?php
require '../php/funciones.php';

if(!haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$idUsuario=$_SESSION['id'];
$admin=$_SESSION['admin'];
$idRequerimiento=$_GET['idRequerimiento'];
?>

<?php include('header.php'); ?>
    

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reporte de Exámen Médico
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-search"></i>Reporte de Exámen Médico</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Reporte de Exámen Médico</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="soloexport" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center">FECHA ALTA/INICIO</th>
                    <th class="text-center">TALLA DE BOTAS</th>
                    <th class="text-center">TALLA DE UNIFORME</th>
                    <th class="text-center">TELEFONO</th>
                    <th class="text-center">CORREO</th>
                    <th class="text-center">CARGO</th>
                    <th class="text-center">CONSULTORA</th>
                    <th class="text-center">EMPRESA</th>
                    <th class="text-center">CÓDIGO DE ESTACIÓN</th>
                    <th class="text-center">ESTACIÓN</th>
                    <th class="text-center">DEPARTAMENTO</th>
                    <th class="text-center">Apellido paterno trabajador</th>
                    <th class="text-center">Apellido materno trabajador</th>
                    <th class="text-center">Nombres trabajador</th>
                    <th class="text-center">Sexo</th>
                    <th class="text-center">Fec. Nac.</th>
                    <th class="text-center">Est. Civil</th>
                    <th class="text-center" style="min-width: 90px">Dirección</th>
                    <th class="text-center">Departamento</th>
                    <th class="text-center">Provincia</th>
                    <th class="text-center">Distrito</th>
                    <th class="text-center">Desc. Nivel. Educ.</th>
                    <th class="text-center">DNI</th> 
                    <th class="text-center">Sist. Prev. Pens.</th>
                    <th class="text-center">F. Afil. SPP</th>
                    <th class="text-center">Cod. Unico SPP</th>
                    <th class="text-center">OBSERVACIÓN</th>
                    <th class="text-center">Quantum</th>   
                  </tr>
                </thead>
                <tbody>
                  <?php  
                  $rs=ejecutarQuery("SELECT persona.*, asignado.idDetalle_requerimiento as idDet, persona.idPersona as idPersona, requerimiento.idCliente as idCliente FROM persona 
                      INNER JOIN asignado ON persona.idPersona=asignado.idPersona 
                      INNER JOIN detalle_requerimiento ON detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_requerimiento 
                      INNER JOIN requerimiento ON detalle_requerimiento.idRequerimiento=requerimiento.idRequerimiento 
                      INNER JOIN distrito on persona.idDistrito=distrito.idDistrito
                      INNER JOIN provincia on provincia.idProvincia=distrito.idProvincia
                      INNER JOIN departamento on departamento.idDepartamento=provincia.idDepartamento
                      INNER JOIN estacion on estacion.idEstacion=detalle_requerimiento.idEstacion
                      WHERE persona.estado IN (8,19,20, 27) and requerimiento.idRequerimiento='$idRequerimiento' group by persona.idPersona"); 
                                       
                  while($row=mysqli_fetch_assoc($rs)){
                        $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
                        $dtr = mysqli_fetch_assoc($dd);

                        $rr=ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento='$dtr[idRequerimiento]'"); 
                        $rq = mysqli_fetch_assoc($rr);
                        $idRequerimiento=$rq['idRequerimiento'];

                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);

                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
                        $spp = mysqli_fetch_assoc($sp);

                        $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
                        $cargo = mysqli_fetch_assoc($car);

                        $es= ejecutarQuery("SELECT nombre, codigo from estacion where idEstacion = '$dtr[idEstacion]' ");
                        $est = mysqli_fetch_assoc($es);

                        $ed= ejecutarQuery("SELECT nombre from nivelEducativo where idNivelEducativo = '$row[idNivelEducativo]' ");
                        $edu = mysqli_fetch_assoc($ed);
                        
                        $cl= ejecutarQuery("SELECT * from clinica where idClinica = '$dtr[idClinica]' ");
                        $cli = mysqli_fetch_assoc($cl);

                        $tur= ejecutarQuery("SELECT * from turno inner join persona on persona.idTurno=turno.idTurno where idPersona = '$row[idPersona]' ");
                        $turno = mysqli_fetch_assoc($tur);

                        $qua= ejecutarQuery("SELECT * from quantum where idQuantum = $row[idQuantum] ");
                        $quantum = mysqli_fetch_assoc($qua);
                  ?>                
                      <tr>
                        <td class="text-center"> <?php echo date("d/m/Y", strtotime($rq['fechaAlta'])); ?> </td>
                        <td class="text-center"> <?php echo $row['tallaBotas']; ?> </td>
                        <td class="text-center"> <?php echo $row['tallaUniforme']; ?> </td>
                        <td class="text-center"><?php echo $row['telefono']; ?></td>
                        <td class="text-center"><?php echo $row['email']; ?></td>
                        <td class="text-center"><?php echo $cargo['nombre']; ?></td>
                        <td class="text-center"><?php echo "T-SOLUCIONA" ?></td>
                        <td class="text-center"><?php 
                          if ($row['idCliente']==2055454574) {
                            echo "COESTI";
                          }
                          else
                            echo "PES";
                         ?></td>
                         <td class="text-center"> <?php echo $est['codigo']; ?></td>
                        <td class="text-center"><?php echo $est['nombre']; ?></td>
                        <td class="text-center"> <?php echo $prov['dep']; ?></td>
                        <td class="text-center"><?php echo $row['apellidoPaterno'];?></td>
                        <td class="text-center"> <?php echo $row['apellidoMaterno']; ?> </td>
                        <td class="text-center"> <?php echo $row['nombres']; ?> </td>
                        <td class="text-center"> <?php echo $gener['nombre']; ?> </td>
                        <td class="text-center"> <?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>        </td>
                        <td class="text-center"> <?php echo $row['estadoCivil']; ?> </td>
                        <td class="text-center"> <?php echo strtoupper($row['direccion']); ?></td>
                        <td class="text-center"> <?php echo $prov['dep']; ?></td>
                        <td class="text-center"> <?php echo $prov['pro']; ?></td>
                        <td class="text-center"> <?php echo $prov['dis']; ?> </td>
                        <td class="text-center"> <?php echo $edu['nombre']; ?> </td>
                        <td class="text-center"> <?php 
                          if (strlen($row['idPersona'])==7) {
                            echo '0'.$row['idPersona'];
                          }
                          else
                          {
                            echo $row['idPersona'];
                          }
                         ?> </td>
                        <td class="text-center"> <?php echo $spp['nombre']; ?> </td>
                        <td class="text-center"> <?php if(date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?> </td>
                        <td class="text-center"> <?php if ($row['codigoSpp']==null) {
                          echo 'NO REGISTRA';
                        }else echo $row['codigoSpp']; ?> </td>
                        <td class="text-center"> <?php echo strtoupper($row['observacion']); ?></td>
                        <td class="text-center"> <?php if ($quantum['nombre']==null) {
                          echo ' ';
                        }else echo $quantum['nombre']; ?> </td>
                      </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
              <CENTER>
                <a href="examenMedicoGeneralR.php?idRequerimiento=<?php echo $idRequerimiento?>">
                  <button type="submit" name="btnEliminar" class="btn btn-success btn-lg" value="Enviar" >DESCARGAR</button>
                </a>
              </CENTER>
            </div>
            <div class="box-footer">
            </div>
          </div>
          
        </div>
      </div>
          
      <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>