<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$admin=$_SESSION['admin'];
$idUsuario=$_SESSION['id'];
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Reporte de Estaciones
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-files-o"></i>Reporte de Estaciones</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row" id="reporteDistrito" >
          <div class="col-xs-12">
            <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Estaciones Probables</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <form method="POST" action="../php/reporteDepartamentoEstaciones.php">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Seleccionar Departamento:</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="departamento" class="form-control pull-right">
                            <?php 
                            $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                            $abcc=mysqli_fetch_assoc($aa);
                            $consulta = ejecutarQuery("SELECT * from departamento order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <br>
                        <button type="submit" class='btn btn-primary btn-md'>GENERAR
                        </button>
                    </div>   
                  </div>
                </form>
              </div>
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>

      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>