﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$idRequerimiento = $_GET['idReq'];
$idUsuario = $_SESSION['id'];
$consulta = ejecutarQuery("SELECT idCliente FROM requerimiento WHERE idRequerimiento=$idRequerimiento");
$aa = mysqli_fetch_assoc($consulta);
?>

<?php include('header.php'); ?>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Listar Postulantes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-book"></i> Requerimientos</a></li>
          <li class="active">Listar Postulantes</li>
        </ol>
      </section>
      <?php 
        $ree = ejecutarQuery("SELECT * FROM requerimiento where idRequerimiento=$idRequerimiento");
        $reqq = mysqli_fetch_assoc($ree);
       ?>
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Requerimiento</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/nuevoDetalleRequerimiento.php?idRequerimiento=<?php echo $idRequerimiento?>" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4"> 
                      <div class="form-group">
                        <label>Cliente</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                          </div>
                          <select class="form-control pull-right" name="cliente" id="cliente" disabled>
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM cliente where idCliente=$aa[idCliente]");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idCliente']; ?>" <?php if ($eee['idCliente']==$aa['idCliente']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Fecha de Alta</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="date" class="form-control pull-right" name="fechaAlta" value="<?php echo $reqq['fechaAlta'] ?>" disabled>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Fecha de Examen Médico</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaExamenMedico" value="<?php echo $reqq['fechaExamen'] ?>" disabled>
                        </div>
                      </div> 
                    </div>   
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Extra</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-users"></i>
                          </div>
                          <input class="form-control pull-right" value="<?php echo $reqq['extra'] ?>" disabled>
                        </div>
                      </div>  
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Tabla de Postulantes</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <table id="example" class="table-bordered table-hover">
                  <thead>
                    <tr>
                      <th ></th>
                      <th class="text-center">OPCIONES</th>
                      <th class="text-center">DNI</th>
                      <th class="text-center" style="min-width: 200px">NOMBRES</th>
                      <th class="text-center">SEXO</th>
                      <th class="text-center">CARGO</th>
                      <th class="text-center">ESTACIÓN</th>
                      <th class="text-center">CLINICA</th>   

                      <th>DEPARTAMENTO<font color="white">------</font></th>
                      <th class="text-center">PROVINCIA<font color="white">-------------</font></th>  
                      <th class="text-center">DISTRITO<font color="white">---------------</font></th>     
                      <th class="text-center">DIRECCIÓN<font color="white">-------------</font></th>    
                      <th class="text-center">FECHA NACIMIENTO<font color="white">-</font></th>
                      <th class="text-center">ESTADO CIVIL<font color="white">----------</font></th>                                 
                      <th>TALLA BOTAS  <font color="white">----------</font> </th>
                      <th>TALLA UNIFORME <font color="white">-----</font></th>
                      <th>TEL&Eacute;FONOS <font color="white">------------</font></th>
                      <th>SPP <font color="white">----------------------</font></th>
                      <th>FECHA SPP  <font color="white">-------------</font></th>
                      <th>CODIGO SPP  <font color="white">-----------</font></th>
                      <th>OBSERVACIONES <font color="white">-----</font></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                      $abcc=mysqli_fetch_assoc($aa); 
                      $rs=ejecutarQuery("SELECT persona.*, detalle_requerimiento.idDetalle_requerimiento as idDet FROM detalle_requerimiento INNER JOIN asignado ON asignado.idDetalle_requerimiento = detalle_requerimiento.idDetalle_requerimiento INNER JOIN persona ON persona.idPersona=asignado.idPersona  inner join usuario on usuario.idUsuario=persona.idUsuario  WHERE detalle_requerimiento.idRequerimiento= $idRequerimiento and persona.estado IN (3,4,14,16,17,18, 25, 19, 20 , 5, 27)  and usuario.idDistrito=$abcc[idDistrito] group by asignado.idPersona");   
                      while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);

                        $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                        $gener = mysqli_fetch_assoc($gen);

                        $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
                        $spp = mysqli_fetch_assoc($sp);

                        $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
                        $dtr = mysqli_fetch_assoc($dd);

                        $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
                        $cargo = mysqli_fetch_assoc($car);

                        $es= ejecutarQuery("SELECT nombre from estacion where idEstacion = '$dtr[idEstacion]' ");
                        $est = mysqli_fetch_assoc($es);

                        $cl= ejecutarQuery("SELECT nombre from clinica where idClinica = '$dtr[idClinica]' ");
                        $cli = mysqli_fetch_assoc($cl);
                    ?>                  
                        <tr bgcolor="white">
                          <td></td>
                          <td class="text-center">      
                            <?php if ($row['estado']==3 || $row['estado']==14 || $row['estado']==16 || $row['estado']==25): ?>
                            <a href="#desconfirm<?php echo $row['idPersona']; echo $row['idDet']; echo $idRequerimiento?>" data-toggle="modal"><button title="REVERTIR" type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span></button></a>
                            <?php endif ?>

                            <?php if ($dtr['estado']==2): echo "REQUERIMIENTO CULMINDADO";?>
                            <?php endif ?>

                            <?php if ($row['estado']==4 || $row['estado']==17 || $row['estado']==18 || $row['estado']==26 || $row['estado']==19 || $row['estado']==20 || $row['estado']==5 || $row['estado']==27): echo "REPORTE GENERADO";?>
                            <?php endif ?>

                         </td>
                          <td class="text-center"><?php echo $row['idPersona']; ?></td>
                          <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                          <td class="text-center"><?php echo $gener['nombre']; ?></td>
                          <td class="text-center"><?php echo $cargo['nombre']; ?></td>
                          <td class="text-center"><?php echo $est['nombre']; ?></td>
                          <td class="text-center"> <?php echo $cli['nombre']; ?> </td>
                          <td class="text-center"><?php echo $prov['dep']; ?></td>

                          <td><?php echo $prov['pro']; ?></td>
                          <td><?php echo $prov['dis']; ?></td>
                          <td><?php echo strtoupper($row['direccion']); ?></td>
                          <td><?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?> </td>
                          <td><?php echo $row['estadoCivil']; ?></td>                        
                          <td><?php echo $row['tallaBotas']; ?></td>
                          <td><?php echo $row['tallaUniforme']; ?></td>
                          <td><?php echo $row['telefono']; ?></td>
                          <td><?php echo $spp['nombre']; ?></td>
                          <td>
                            <?php if(date("d/m/Y", strtotime($row['fechaSpp'])) == '01/01/1970') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                          </td>
                          <td><?php echo $row['codigoSpp']; ?></td>
                          <td><?php echo strtoupper($row['observacion']); ?></td>
                        </tr>
                         
                        <!--DesConfirmar Modal -->
                        <div id="desconfirm<?php echo $row['idPersona']; echo $row['idDet']; echo $idRequerimiento?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">
                            <form method="post" action="../php/revertirPostulanteRequerimiento.php?codigo=<?php echo $row['idPersona'];?>&idDet=<?php echo $row['idDet'];?>&idReq=<?php echo $idRequerimiento;?>">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">REVERTIR REQUERIMIENTO DE POSTULANTE</h4>
                                </div>
                                <div class="modal-body">
                                  <p>¿Esta seguro de revertir al postulante <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres'];?>?</strong></p>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>SI</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                    <?php
                      }
                    ?>                  
                  </tbody>                
                </table> 
              <div class="box-footer">
                <center>
                  <a href="../php/listarDetalleRequerimientoLima.php?idReq=<?php echo $idRequerimiento;?>">
                    <button type="submit" class='btn btn-primary btn-lg'>REPORTE LIMA</button>
                  </a>                    
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="../php/listarDetalleRequerimientoOtros.php?idReq=<?php echo $idRequerimiento;?>">
                    <button type="submit" class='btn btn-primary btn-lg'>REPORTE OTROS</button>
                  </a>
                </center>
              </div>
              </div>
            </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>