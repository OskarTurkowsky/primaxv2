﻿<?php
require '../php/funciones.php';

if(!haIniciadoSesion())
{
 header('Location: ../index.php');
}
$admin=$_SESSION['admin'];
$departamento=$_GET['departamento'];
$consulta1 = ejecutarQuery("SELECT * FROM departamento where idDepartamento=$departamento");
$aa=mysqli_fetch_assoc($consulta1);
?>

<?php include('header.php'); ?>
    
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Reporte de Reclutadores
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-search"></i>Reporte de Reclutadores</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Departamento: <?php echo $aa['nombre']; ?></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="soloexport" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 80px">DNI</th>
                    <th class="text-center" style="width: 250px">NOMBRES</th>
                    <th class="text-center" >DISPONIBILIDAD</th>
                    <th class="text-center" >ANTECEDENTE POLICIAL</th>
                    <th class="text-center" >CARNET DE SANIDAD</th>
                    <th class="text-center" >DEPARTAMENTO</th>   
                    <th class="text-center" >PROVINCIA</th>   
                    <th class="text-center" style="width: 150px">DISTRITO</th>                                 
                    <th class="text-center" style="width: 200px">TELEFONOS</th>
                    <th class="text-center" >FECHA EVALUACIÓN</th>
                    <th class="text-center" style="width: 150px">OBSERVACIÓN</th>
                    <th class="text-center" style="width: 160px">FECHA EDICIÓN</th>
                    <th class="text-center" style="width: 160px">CONSULTOR</th>
                    <th class="text-center" style="width: 150px">USUARIO</th>   
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if ($departamento==26) {
                    $rs=ejecutarQuery(" SELECT backup.*, persona.idPersona as idPersona, persona.nombres, persona.apellidoPaterno as apellidoPaterno, persona.apellidoMaterno as apellidoMaterno, persona.idDistrito as idDistrito, persona.observacion as observacion, provincia.idProvincia as idProvincia, consultor.idConsultor as idConsultor, usuario.idUsuario as idUsuario, persona.telefono as telefono FROM persona inner join backup on persona.idPersona=backup.idBackup inner join consultor on consultor.idConsultor=backup.idConsultor inner join distrito on distrito.idDistrito=persona.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join usuario on usuario.idUsuario=persona.idUsuario where  backup.disponibilidad!='NO' and persona.estado IN (0,2,13,15) and (provincia.idProvincia=1505  or distrito.idDistrito IN(150126,150119) )");                   
                    while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as distrito, provincia.nombre as provincia, departamento.nombre as departamento FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = $row[idDistrito] ");
                        $prov = mysqli_fetch_assoc($pro);
                        $cons=ejecutarQuery("SELECT nombre from consultor where idConsultor=$row[idConsultor] ");
                        $consultor = mysqli_fetch_assoc($cons);
                        $us=ejecutarQuery("SELECT nombres from usuario where idUsuario=$row[idUsuario] ");
                        $usu = mysqli_fetch_assoc($us);
                  ?>                
                      <tr>
                        <td class="text-center" style="width: 80px"><?php echo $row['idPersona']; ?></td>
                        <td class="text-center" style="width: 250px"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center" ><?php echo $row['disponibilidad']; ?></td>
                        <td class="text-center" ><?php echo $row['policial']; ?></td>
                        <td class="text-center" ><?php echo $row['sanidad']; ?></td>
                        <td class="text-center" ><?php echo $prov['departamento']; ?></td>
                        <td class="text-center" ><?php echo $prov['provincia']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $prov['distrito']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $row['telefono']; ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($row['observacion']); ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y H:i:s", strtotime($row['fechaEdicion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($consultor['nombre']); ?></td>                        
                        <td class="text-center" ><?php echo strtoupper($usu['nombres']); ?></td>
                      </tr>
                  <?php
                    }
                  }
                  if($departamento==27){
                      $rs=ejecutarQuery(" SELECT backup.*, persona.idPersona as idPersona, persona.nombres, persona.apellidoPaterno as apellidoPaterno, persona.apellidoMaterno as apellidoMaterno, persona.idDistrito as idDistrito, persona.observacion as observacion, provincia.idProvincia as idProvincia, consultor.idConsultor as idConsultor, usuario.idUsuario as idUsuario, persona.telefono as telefono FROM persona inner join backup on persona.idPersona=backup.idBackup inner join consultor on consultor.idConsultor=backup.idConsultor inner join distrito on distrito.idDistrito=persona.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join usuario on usuario.idUsuario=persona.idUsuario where  backup.disponibilidad!='NO' and persona.estado IN (0,2,13,15) and (provincia.idProvincia IN (1506,1508)  or distrito.idDistrito=150201)");                   
                    while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as distrito, provincia.nombre as provincia, departamento.nombre as departamento FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = $row[idDistrito] ");
                        $prov = mysqli_fetch_assoc($pro);
                        $cons=ejecutarQuery("SELECT nombre from consultor where idConsultor=$row[idConsultor] ");
                        $consultor = mysqli_fetch_assoc($cons);
                        $us=ejecutarQuery("SELECT nombres from usuario where idUsuario=$row[idUsuario] ");
                        $usu = mysqli_fetch_assoc($us);
                  ?>                
                      <tr>
                        <td class="text-center" style="width: 80px"><?php echo $row['idPersona']; ?></td>
                        <td class="text-center" style="width: 250px"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center" ><?php echo $row['disponibilidad']; ?></td>
                        <td class="text-center" ><?php echo $row['policial']; ?></td>
                        <td class="text-center" ><?php echo $row['sanidad']; ?></td>
                        <td class="text-center" ><?php echo $prov['departamento']; ?></td>
                        <td class="text-center" ><?php echo $prov['provincia']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $prov['distrito']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $row['telefono']; ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($row['observacion']); ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y H:i:s", strtotime($row['fechaEdicion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($consultor['nombre']); ?></td>                        
                        <td class="text-center" ><?php echo strtoupper($usu['nombres']); ?></td>
                      </tr>
                  <?php
                    }
                  }
                  if($departamento!=26 || $departamento!=27){
                    $rs=ejecutarQuery(" SELECT backup.*, persona.idPersona as idPersona, persona.nombres, persona.apellidoPaterno as apellidoPaterno, persona.apellidoMaterno as apellidoMaterno, persona.idDistrito as idDistrito, persona.observacion as observacion, provincia.idProvincia as idProvincia, departamento.idDepartamento as idDepartamento, consultor.idConsultor as idConsultor, usuario.idUsuario as idUsuario, persona.telefono as telefono FROM persona inner join backup on persona.idPersona=backup.idBackup inner join consultor on consultor.idConsultor=backup.idConsultor inner join distrito on distrito.idDistrito=persona.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento inner join usuario on usuario.idUsuario=persona.idUsuario where departamento.idDepartamento=$departamento and persona.estado IN (0,2,13,15) and backup.disponibilidad!='NO' ");                   
                    while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as distrito, provincia.nombre as provincia, departamento.nombre as departamento FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = $row[idDistrito] ");
                        $prov = mysqli_fetch_assoc($pro);
                        $cons=ejecutarQuery("SELECT nombre from consultor where idConsultor=$row[idConsultor] ");
                        $consultor = mysqli_fetch_assoc($cons);
                        $us=ejecutarQuery("SELECT nombres from usuario where idUsuario=$row[idUsuario] ");
                        $usu = mysqli_fetch_assoc($us);
                  ?>                
                      <tr>
                        <td class="text-center" style="width: 80px"><?php echo $row['idPersona']; ?></td>
                        <td class="text-center" style="width: 250px"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center" ><?php echo $row['disponibilidad']; ?></td>
                        <td class="text-center" ><?php echo $row['policial']; ?></td>
                        <td class="text-center" ><?php echo $row['sanidad']; ?></td>
                        <td class="text-center" ><?php echo $prov['departamento']; ?></td>
                        <td class="text-center" ><?php echo $prov['provincia']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $prov['distrito']; ?></td>
                        <td class="text-center" style="width: 150px"><?php echo $row['telefono']; ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($row['observacion']); ?></td>
                        <td class="text-center" ><?php echo date("d/m/Y H:i:s", strtotime($row['fechaEdicion'])); ?></td>
                        <td class="text-center" ><?php echo strtoupper($consultor['nombre']); ?></td>
                        <td class="text-center" ><?php echo strtoupper($usu['nombres']); ?></td>
                      </tr>
                  <?php
                    }
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <div class="box-footer">
              <?php 
              $cons=ejecutarQuery(" SELECT count(*) as contador FROM persona inner join backup on persona.idPersona=backup.idBackup  inner join consultor on consultor.idConsultor=backup.idConsultor inner join distrito on distrito.idDistrito=persona.idDistrito inner join provincia on provincia.idProvincia=distrito.idProvincia inner join departamento on departamento.idDepartamento=provincia.idDepartamento  where departamento.idDepartamento=$departamento and persona.estado IN (0,2,13,15) "); 
              $consult=mysqli_fetch_assoc($cons);
               ?>
              <!--<h4> Total:
               <?php echo $consult['contador']; ?> </h4>-->
            </div>
          </div>
          
        </div>
      </div>
      </section>

    </div>
    
<?php include('footer.php'); ?>