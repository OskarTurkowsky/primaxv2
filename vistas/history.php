<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$admin = $_SESSION['admin'];

?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Buscar Postulantes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-male"></i> Buscar Postulantes</a></li>
          <li class="active">Buscar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Buscar</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form name="form1">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" name="dni" class="form-control pull-right" id="dni" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="modal-header">
                            <button type="submit" class="btn btn-warning">BUSCAR</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            
            <div class="box" >
              <div class="box-header">
                <h3 class="box-title">Historial</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body" value="midiv" id="midiv">
    <?php 
if (!empty($_GET)) {
  $miid = $_GET['dni'];
  $cont = ejecutarQuery("SELECT count(*) as contador from persona_ocurrencia where idPersona=$miid");
  $result = mysqli_fetch_assoc($cont);
  $auto = $result['contador'];
  if ($auto==0) {
  ?>
    <h4>No existe </h4>
    <?php 
  }
  else
  {
    ?>
    <table id="buscar" class="display" style="width:100%">
      <thead>
        <tr>
          <th class="text-center" style="min-width: 80px">DNI</th>
          <th class="text-center">ESTADO</th>
          <th class="text-center">OCURRENCIA</th>
          <th class="text-center" style="min-width: 200px">NOMBRES</th>
          <th class="text-center">USUARIO</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          $sql = ejecutarQuery("SELECT * from persona_ocurrencia where idPersona=$miid");
          while ($row=mysqli_fetch_assoc($sql)) {
            $oc = ejecutarQuery("SELECT nombre from ocurrencia_tipo where idOcurrencia_tipo=$row[idOcurrencia_tipo]");
            $ocurrencia = mysqli_fetch_assoc($oc);
            $user = ejecutarQuery("SELECT nombres from usuario where idUsuario=$row[idUsuario]");
            $usuario = mysqli_fetch_assoc($user);
        ?>
        <tr bgcolor="white">
          <td class="text-center"><?php 
          if (strlen($row['idPersona'])==7) {
                echo '0'.$row['idPersona'];
              }
          else
              {
                echo $row['idPersona'];
              }
          ?></td>
          <td class="text-center">
          <?php 
            switch ($row['estado']) {
              case 0:
                    echo "BACK UP";
                    break;
              case 1:
                    echo "POSTULANTE ELIMINADO";
                    break;
              case 2:
                    echo "POSTULANTE REGISTRADO";
                    break;
              case 3:
              case 4:
                    echo "ASIGNADO A REQUERIMIENTO";
                    break;
              case 5:
              case 6:
              case 7:
                    echo "SELECCIONADO PARA ANTECEDENTES";
                    break;   
              case 13;
              case 14:
              case 17:
              case 19:
              case 8:
              case 24:
                    echo "SIN ANTECEDENTES";
                    break;         
              case 9:
                    echo "CON ANTECEDENTES: ";
                    echo $row['tipoAntecedente'];
                    break;  
              case 10:
              case 15:
              case 16:
              case 18:
              case 20:
                    echo "EXAMEN MÉDICO APROBADO";
                    break;  
              case 11:
                    echo "EXAMEN MÉDICO DESAPROBADO";
                    break;  
              case 12:
                    echo "TRABAJADOR";
                    break; 
              case 21:
                    echo "EXAMEN MEDICO: NO APTO";  
                    break;
              case 22:
                    echo "PASAR ANTECEDENTES SIN REQUERIMIENTO";  
                    break; 
              case 23:
                    echo "CONFIRMAR ANTECEDENTES SIN REQUERIMIENTO";  
                    break;               
              case 24:
                    echo "SIN ANTECEDENTES (NO ASIGNADO A REQUERIMIENTO)";  
                    break; 
              case 25:
                    echo "SIN ANTECEDENTES (ASIGNADO A REQUERIMIENTO)";  
                    break; 
              case 27:
                    echo "SIN ANTECEDENTES (CONFIRMAR REQUERIMIENTO)";  
                    break; 
              }
          ?>
          </td>
          <td class="text-center"><?php echo $ocurrencia['nombre']; ?></td>
          <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
          <td class="text-center"><?php echo $usuario['nombres'] ?></td>
        </tr>
        <?php } ?>
      </tbody>
    
    </table> 
  <?php 
  } 
}?>
            </div>

            <div class="box-footer">
            </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>