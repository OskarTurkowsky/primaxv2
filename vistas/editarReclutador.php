﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$admin=$_SESSION['admin'];
$dni = $_GET['id'];
?>


<?php include('header.php'); ?>
    
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Postulantes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i> Postulantes</a></li>
          <li class="active">Editar</li>
        </ol>
      </section>

      <?php  
        $ok  = ejecutarQuery("SELECT * FROM persona where idPersona=$dni");
        $oks = mysqli_fetch_assoc($ok);

        $ab  = ejecutarQuery("SELECT * FROM backup where idBackup=$dni");
        $abc = mysqli_fetch_assoc($ab);

        $asd = $oks['idDistrito'];
        $gen = $oks['idGenero'];
        $var = ejecutarQuery("SELECT p.idProvincia, dep.idDepartamento FROM distrito d INNER JOIN provincia p on d.idProvincia = p.idProvincia INNER JOIN departamento dep on p.idDepartamento = dep.idDepartamento WHERE d.idDistrito = $asd");
        $varr = mysqli_fetch_assoc($var);
        
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Formulario de Edicion</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/editarPostulantes.php?dni=<?php echo $dni?>" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required="" value="<?php echo $dni?>" disabled >
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Paterno</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="paterno" required="" value="<?php echo $oks['apellidoPaterno']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Materno</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="materno" required="" value="<?php echo $oks['apellidoMaterno']?>">
                        </div>                       
                      </div>
                      <div class="form-group">
                        <label>Nombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required="" value="<?php echo $oks['nombres']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha de Nacimiento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaNacimiento" required="" value="<?php echo $oks['fechaNacimiento']?>">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Sexo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-venus-mars"></i>
                          </div>
                          <select class="form-control pull-right" name="sexo">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM genero order by idGenero LIMIT 2");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idGenero']; ?>" <?php if ($eee['idGenero']==$oks['idGenero']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>   
                      </div>
                      <div class="form-group">
                        <label>Codigo SPP</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-barcode"></i>
                          </div>
                          <input type="text" class="form-control pull-right" name="codigoSpp" style="text-transform:uppercase;" value="<?php echo $oks['codigoSpp']?>" >
                        </div>   
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Estado Civil</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-bullseye"></i>
                          </div>
                          <?php 
                        switch ($oks['estadoCivil']) {
                          case 'SOLTERO(A)':
                        ?>
                            <select class="form-control pull-right" name="estadoCivil">
                              <option selected="selected">SOLTERO(A)</option>
                              <option>CASADO(A)</option>
                              <option>VIUDO(A)</option>
                              <option>DIVORCIADO(A)</option>
                            </select>
                        <?php
                            break;
                          case 'CASADO(A)':
                        ?>
                            <select class="form-control pull-right" name="estadoCivil">
                              <option>SOLTERO(A)</option>
                              <option selected="selected">CASADO(A)</option>
                              <option>VIUDO(A)</option>
                              <option>DIVORCIADO(A)</option>
                            </select>
                        <?php
                            break;
                          case 'VIUDO(A)':
                        ?>
                            <select class="form-control pull-right" name="estadoCivil">
                              <option>SOLTERO(A)</option>
                              <option>CASADO(A)</option>
                              <option selected="selected">VIUDO(A)</option>
                              <option>DIVORCIADO(A)</option>
                            </select>
                        <?php
                            break;
                          default:
                        ?>
                            <select class="form-control pull-right" name="estadoCivil">
                              <option>SOLTERO(A)</option>
                              <option>CASADO(A)</option>
                              <option>VIUDO(A)</option>
                              <option selected="selected">DIVORCIADO(A)</option>
                            </select>
                        <?php
                            break;
                        }
                        ?>
                        </div>   
                      </div>
                      <div class="form-group">
                        <label>Tel&eacute;fonos</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input  name="telefono" class="form-control pull-right" value="<?php echo $oks['telefono']?>" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Correo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-envelope-o"></i>
                          </div>
                          <input type="email" name="email" class="form-control pull-right" value="<?php echo $oks['email']?>"  onkeypress="return validaremail(event)" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Talla de Botas</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-arrows-h"></i>
                          </div>
                          <input type="number" name="tallaBotas" class="form-control pull-right" value="<?php echo $oks['tallaBotas']?>" required>
                        </div>                        
                      </div>
                      <div class="form-group">
                        <label>Talla de Uniforme</label>
                        <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-child"></i>
                        </div>
                        <?php 
                        switch ($oks['tallaUniforme']) {
                          case 'S':
                        ?>
                            <select name="tallaUniforme" class="form-control pull-right" required="">
                              <option selected="selected">S</option>
                              <option>M</option>
                              <option>L</option>
                              <option>XL</option>
                            </select>  
                        <?php
                            break;
                          case 'M':
                        ?>
                            <select name="tallaUniforme" class="form-control pull-right" required="">
                              <option>S</option>
                              <option selected="selected">M</option>
                              <option>L</option>
                              <option>XL</option>
                            </select>  
                        <?php
                            break;
                          case 'L':
                        ?>
                            <select name="tallaUniforme" class="form-control pull-right" required="">
                              <option>S</option>
                              <option>M</option>
                              <option selected="selected">L</option>
                              <option>XL</option>
                            </select>  
                        <?php
                            break;
                          default:
                        ?>
                            <select name="tallaUniforme" class="form-control pull-right" required="">
                              <option>S</option>
                              <option>M</option>
                              <option>L</option>
                              <option selected="selected">XL</option>
                            </select>  
                        <?php    
                            break;
                        }
                        ?>
                    </div>
                      </div>
                      <div class="form-group">
                        <label>Deseable nivel educativo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-graduation-cap"></i>
                          </div>
                          <select class="form-control pull-right" name="nivelEducativo">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM nivelEducativo order by idNivelEducativo ");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idNivelEducativo']; ?>" <?php if ($eee['idNivelEducativo']==$oks['idNivelEducativo']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>                        
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Observaciones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="observacion"  value="<?php echo $oks['observacion']?>" >
                          </textarea>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Departamento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="departamento" id="" onchange="from(document.form1.departamento.value,'midiv','../php/provincias.php');">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>" 
                                <?php if ($eee['idDepartamento']==$varr['idDepartamento']) {
                                  echo "selected='selected'";
                                } 
                                ?>
                              >
                                <?php echo $eee['nombre']; ?>
                              </OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Provincia</label>
                        <div class="input-group" id="midiv">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="provincia" id="provincias" onchange="from(document.form1.provincia.value,'midiv2','../php/distritos.php');">
                            <?php 
                            $subconsulta = "(select idDepartamento from provincia where idProvincia =".$varr['idProvincia'].")";
                            $consulta = ejecutarQuery("SELECT * FROM provincia where idDepartamento = $subconsulta order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idProvincia']; ?>" <?php if ($eee['idProvincia']==$varr['idProvincia']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>  
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Distrito</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="distrito" id="distritos">
                            <?php 
                            $subconsulta = "(select idProvincia from distrito where idDistrito =".$oks['idDistrito'].")";
                            $consulta = ejecutarQuery("SELECT * FROM distrito where idProvincia = $subconsulta order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDistrito']; ?>" <?php if ($eee['idDistrito']==$oks['idDistrito']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>  
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Direcci&oacute;n</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="direccion" required="" value="<?php echo $oks['direccion']?>">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Sistema Privado de Pensiones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-check"></i>
                          </div>
                          <select name="spp" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM spp order by idSpp");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idSpp']; ?>" <?php if ($eee['idSpp']==$oks['idSpp']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>   
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Fecha SPP</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="fechaSpp" value="<?php echo $oks['fechaSpp']?>" >
                        </div>   
                      </div>

                      <div class="form-group">
                        <label>Disponibilidad</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select class="form-control pull-right" name="disponibilidad">
                            <?php 
                              if ($abc['disponibilidad']=='POR DEFINIR') {
                                echo "<option selected='selected'>POR DEFINIR</option>";
                                echo "<option>NO</option>";
                                echo "<option>SI</option>";
                              } else {
                                if ($abc['disponibilidad']=='NO') {
                                  echo "<option>POR DEFINIR</option>";
                                  echo "<option selected='selected'>NO</option>";
                                  echo "<option>SI</option>";
                                } else {
                                  echo "<option>POR DEFINIR</option>";
                                  echo "<option>NO</option>";
                                  echo "<option selected='selected'>SI</option>";
                                }
                                
                              }
                            ?>
                          </select>
                        </div>
                      </div>

                    <?php if ( ($admin==1 || $admin==3) && $oks['estado']>7  ) {
                      ?>
                      <div class="form-group">
                        <label>Antecedentes</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-gavel"></i>
                          </div>
                          <select name="antecedente" id="antecedentes" class="form-control pull-right">
                            <OPTION  VALUE="0" >SIN ANTECEDENTES</OPTION>
                            <OPTION VALUE="1" >CON ANTECEDENTES</OPTION>
                          </select>
                          <input type="text" placeholder="Tipo de antecedente" id="razon" name="razon" style="display: none">
                        </div>   
                      </div>
                      <?php
                    } ?>                     

                      <br>
                      <center> <button type="submit" class="btn btn-primary pull-center">EDITAR</button> </center>     
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>