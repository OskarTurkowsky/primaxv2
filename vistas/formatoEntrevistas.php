<?php
require('../fpdf.php');
require '../php/funciones.php';
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario 				 = $_SESSION['id'];

class PDF extends FPDF
{
	// Cabecera de p�gina
	function plantilla($cliente)
	{
		$this->Rect(5, 5, 60, 15, '');
		$this->Rect(65, 5, 92, 15, '');
		$this->Rect(157, 5, 45, 15, '');

		$this->Rect(5, 73, 60, 15, '');
		$this->Rect(65, 73, 92, 15, '');
		$this->Rect(157, 73, 45, 15, '');

		$this->Rect(5, 141, 60, 15, '');
		$this->Rect(65, 141, 92, 15, '');
		$this->Rect(157, 141, 45, 15, '');

		$this->Rect(5, 209, 60, 15, '');
		$this->Rect(65, 209, 92, 15, '');
		$this->Rect(157, 209, 45, 15, '');

		$this->Image('../imagenes/logo GRIS.jpg',7,7,57);
		$this->Image('../imagenes/logo GRIS.jpg',7,75,57);
		$this->Image('../imagenes/logo GRIS.jpg',7,143,57);
		$this->Image('../imagenes/logo GRIS.jpg',7,211,57);

		if ($cliente==2055454574) {
			$this->Image('../imagenes/logo_primax gris.jpg',158,7,42);
			$this->Image('../imagenes/logo_primax gris.jpg',158,75,42);
			$this->Image('../imagenes/logo_primax gris.jpg',158,143,42);
			$this->Image('../imagenes/logo_primax gris.jpg',158,211,42);
		}

		if ($cliente==2025903307) {
			$this->Image('../imagenes/logo_pecsa GRIS.jpg',158,7,42);
			$this->Image('../imagenes/logo_pecsa GRIS.jpg',158,75,42);
			$this->Image('../imagenes/logo_pecsa GRIS.jpg',158,143,42);
			$this->Image('../imagenes/logo_pecsa GRIS.jpg',158,211,42);
		}

		$this->Rect(5, 20, 42, 10, '');
	  	$this->Rect(5, 88, 42, 10, '');
	  	$this->Rect(5, 156, 42, 10, '');
	  	$this->Rect(5, 224, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 20, 155, 10, 'DF');
	  	$this->Rect(47, 88, 155, 10, 'DF');
	  	$this->Rect(47, 156, 155, 10, 'DF');
	  	$this->Rect(47, 224, 155, 10, 'DF');

	  	$this->Rect(5, 30, 42, 10, '');
	  	$this->Rect(5, 98, 42, 10, '');
	  	$this->Rect(5, 166, 42, 10, '');
	  	$this->Rect(5, 234, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 30, 46, 10, 'DF');
	  	$this->Rect(47, 98, 46, 10, 'DF');
	  	$this->Rect(47, 166, 46, 10, 'DF');
	  	$this->Rect(47, 234, 46, 10, 'DF');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(138, 30, 64, 10, 'DF');
	  	$this->Rect(138, 98, 64, 10, 'DF');
	  	$this->Rect(138, 166, 64, 10, 'DF');
	  	$this->Rect(138, 234, 64, 10, 'DF');

	  	$this->Rect(5, 40, 42, 10, '');
	  	$this->Rect(5, 108, 42, 10, '');
	  	$this->Rect(5, 176, 42, 10, '');
	  	$this->Rect(5, 244, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 40, 46, 10, 'DF');
	  	$this->Rect(47, 108, 46, 10, 'DF');
	  	$this->Rect(47, 176, 46, 10, 'DF');
	  	$this->Rect(47, 244, 46, 10, 'DF');

	  	$this->Rect(93, 40, 46, 10, '');
	  	$this->Rect(93, 108, 46, 10, '');
	  	$this->Rect(93, 176, 46, 10, '');
	  	$this->Rect(93, 244, 46, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(138, 40, 64, 10, 'DF');
	  	$this->Rect(138, 108, 64, 10, 'DF');
	  	$this->Rect(138, 176, 64, 10, 'DF');
	  	$this->Rect(138, 244, 64, 10, 'DF');

	  	$this->Rect(5, 50, 42, 10, '');
	  	$this->Rect(5, 118, 42, 10, '');
	  	$this->Rect(5, 186, 42, 10, '');
	  	$this->Rect(5, 254, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 50, 155, 10, 'DF');
	  	$this->Rect(47, 118, 155, 10, 'DF');
	  	$this->Rect(47, 186, 155, 10, 'DF');
	  	$this->Rect(47, 254, 155, 10, 'DF');

	  	$this->Rect(5, 60, 42, 13, '');
	  	$this->Rect(5, 128, 42, 13, '');
	  	$this->Rect(5, 196, 42, 13, '');
	  	$this->Rect(5, 264, 42, 13, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 60, 155, 13, 'DF');
	  	$this->Rect(47, 128, 155, 13, 'DF');
	  	$this->Rect(47, 196, 155, 13, 'DF');
	  	$this->Rect(47, 264, 155, 13, 'DF');

			$this->SetFont('Arial','',14);
			$this->Cell(12);
	  	$this->Cell(0,7,'FORMATO DE ENVIO A ENTREVISTA',0,1,'C');
	  	
	  	$this->Ln(8);
	  	$this->SetFont('Arial','',12);
			$this->Cell(-3);
	  	$this->Cell(0,0,'Postulante',0,1,'L');
	  	$this->Ln(10);
			$this->Cell(-3);
	  	$this->Cell(0,0,'Fecha',0,1,'L');
			$this->Cell(85);
	  	$this->Cell(0,0,'Cargo al que postula',0,1,'L');
	  	$this->Ln(10);	  	
			$this->Cell(-3);
	  	$this->Cell(0,0,'Entrevistador',0,1,'L');
			$this->Cell(85);
	  	$this->Cell(0,0,'Hora',0,1,'L');
	  	$this->Ln(10);
			$this->Cell(-3);
	  	$this->Cell(0,0,'Lugar de entrevista',0,1,'L');
	  	$this->Ln(12);
			$this->Cell(-3);
	  	$this->Cell(0,0,'Observaciones',0,1,'L');

	  	$this->SetFont('Arial','',14);
	  	$this->Ln(11);
		$this->Cell(12);
	  	$this->Cell(0,7,'FORMATO DE ENVIO A ENTREVISTA',0,1,'C');
	  	$this->SetFont('Arial','',12);
	  	$this->Ln(8);
	  	$this->Cell(-3);
	  	$this->Cell(0,0,'Postulante',0,1,'L');
	  	$this->Ln(10);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Fecha',0,1,'L');
	  	$this->Cell(85);
	  	$this->Cell(0,0,'Cargo al que postula',0,1,'L');
	  	$this->Ln(10);	  	
		$this->Cell(-3);
	  	$this->Cell(0,0,'Entrevistador',0,1,'L');
		$this->Cell(85);
	  	$this->Cell(0,0,'Hora',0,1,'L');
	  	$this->Ln(10);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Lugar de entrevista',0,1,'L');
	  	$this->Ln(12);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Observaciones',0,1,'L');

	  	$this->SetFont('Arial','',14);
	  	$this->Ln(11);
		$this->Cell(12);
	  	$this->Cell(0,7,'FORMATO DE ENVIO A ENTREVISTA',0,1,'C');
	  	$this->SetFont('Arial','',12);
	  	$this->Ln(8);
	  	$this->Cell(-3);
	  	$this->Cell(0,0,'Postulante',0,1,'L');
	  	$this->Ln(10);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Fecha',0,1,'L');
	  	$this->Cell(85);
	  	$this->Cell(0,0,'Cargo al que postula',0,1,'L');
	  	$this->Ln(10);	  	
		$this->Cell(-3);
	  	$this->Cell(0,0,'Entrevistador',0,1,'L');
		$this->Cell(85);
	  	$this->Cell(0,0,'Hora',0,1,'L');
	  	$this->Ln(10);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Lugar de entrevista',0,1,'L');
	  	$this->Ln(12);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Observaciones',0,1,'L');

	  	$this->SetFont('Arial','',14);
	  	$this->Ln(11);
		$this->Cell(12);
	  	$this->Cell(0,7,'FORMATO DE ENVIO A ENTREVISTA',0,1,'C');
	  	$this->SetFont('Arial','',12);
	  	$this->Ln(8);
	  	$this->Cell(-3);
	  	$this->Cell(0,0,'Postulante',0,1,'L');
	  	$this->Ln(10);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Fecha',0,1,'L');
	  	$this->Cell(85);
	  	$this->Cell(0,0,'Cargo al que postula',0,1,'L');
	  	$this->Ln(10);	  	
		$this->Cell(-3);
	  	$this->Cell(0,0,'Entrevistador',0,1,'L');
		$this->Cell(85);
	  	$this->Cell(0,0,'Hora',0,1,'L');
	  	$this->Ln(10);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Lugar de entrevista',0,1,'L');
	  	$this->Ln(12);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Observaciones',0,1,'L');
	}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$contador1=0;
	$n=1;
	
	$sql = ejecutarQuery("SELECT distinct *, requerimiento.idCliente as idCliente from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
		inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona
		inner join distrito on persona.idDistrito=distrito.idDistrito
        inner join provincia on provincia.idProvincia=distrito.idProvincia
        inner join departamento on departamento.idDepartamento=provincia.idDepartamento
		where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento group by persona.idPersona");
		while($fila=mysqli_fetch_array($sql)){ 
			$sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
				inner join provincia on provincia.idProvincia=distrito.idProvincia
				inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
			$usuarioDept=mysqli_fetch_array($sqqqq);

			$sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
			$filaasa=mysqli_fetch_array($sq);

			$cliente=$fila['idCliente'];

			if ( ($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
				($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
				($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
				 ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
				 ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
				 (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759 || $usuario== 15359362) && 
				 ( $fila['idDepartamento'] == 12  || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 || $fila['idEstacion'] == 39 || $fila['idEstacion'] ==  209  || $fila['idEstacion'] == 44 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==302 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256 || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146) ) ||
				 (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874) && 
				 ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==303 ||  $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] == 245) ) || 
				 ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
				 ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
				 ($filaasa['idDepartamento']==$usuarioDept['idDepartamento']) ) {
					$nombre[]=$fila['nombres'];
					$apellidoPaterno[]=$fila['apellidoPaterno'];
					$apellidoMaterno[]=$fila['apellidoMaterno'];
					$fechaAlta[]=$fila['fechaAlta'];
					$sqla = ejecutarQuery("SELECT nombre as cargo from cargo where idcargo=$fila[idcargo]");
					$filaa=mysqli_fetch_array($sqla);
					$cargo[]=$filaa['cargo'];
					$sq = ejecutarQuery("SELECT departamento.idDepartamento as idDepartamento, estacion.nombre as estacion from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
					$filaasa=mysqli_fetch_array($sq);
					$departamento[]=$filaasa['idDepartamento'];
					$estacion[]=$filaasa['estacion'];
					$contador1=$n+$contador1;
				}
		}
	$contador=$contador1;
	for ($i=0; $i <$contador; $i=$i+4) { 
		$pdf->AddPage();
		$pdf->plantilla($cliente);
		$pdf->SetXY(50,25);
		$pdf->Write(0,utf8_decode($nombre[0+$i].' '.$apellidoPaterno[0+$i].' '.$apellidoMaterno[0+$i]));
		$pdf->SetXY(50,35);
		$pdf->Write(0,date("d/m/Y", strtotime($fechaAlta[0+$i])));
		$pdf->SetXY(140,35);
		$pdf->Write(0,$cargo[0+$i]);
		$pdf->SetXY(50,45);
		if ($departamento[0+$i]==15 || $departamento[0+$i]==7) {
			if ($cliente=='2055454574') 
				{
					$pdf->Write(0,'Cecilia Or�');
				}
				//PECSA
			if ($cliente=='2025903307')
				{
					$pdf->Write(0,'Esperanza Guillen');
				}
		}
		else
		{
			if ($departamento[0+$i]==13) {
				$pdf->Write(0,'Claudia Quintana');
			}
			if ($departamento[0+$i]==20) {
				$pdf->Write(0,'Diana morales');
			}
			if ($departamento[0+$i]==14) {
				$pdf->Write(0,'Maria Diaz');
			}
		}
		$pdf->SetXY(50,55);
		if ($departamento[0+$i]==15 || $departamento[0+$i]==7) {
				if ($cliente=='2055454574') {
					$pdf->Write(0,'');
				}
				//PECSA
				if ($cliente=='2025903307')
				{
					$pdf->Write(0,'');
				}				
		}
		else
		{
			if ($departamento[0+$i]==13) {
				$pdf->Write(0,'E/S LARCO');
			}
			else
			{
				if ($departamento[0+$i]==20) {
				$pdf->Write(0,'E/S MEGA');
				}
				else
				{
					if ($departamento[0+$i]==14) {
						$pdf->Write(0,'E/S SANTA ELENA');
					}
					else
					{
						$pdf->Write(0,utf8_decode($estacion[0+$i]));
					}
				}
			}
		}
		$pdf->SetXY(140,45);
		if ($departamento[0+$i]==15 || $departamento[0+$i]==7) {
				if ($cliente=='2055454574') {
					$pdf->Write(0,'08:30 am');
				}
				if ($cliente=='2025903307')
				{
					$pdf->Write(0,'08:30 am');
				}
		}
		else
		{
			$pdf->Write(0,'08:30 am');
		}
		//--------------------------------------------
		$pdf->SetXY(50,93);
		if (1+$i<$contador) {
			$pdf->Write(0,utf8_decode($nombre[1+$i].' '.$apellidoPaterno[1+$i].' '.$apellidoMaterno[1+$i]));
			$pdf->SetXY(50,103);
			$pdf->Write(0,date("d/m/Y", strtotime($fechaAlta[1+$i])));
			$pdf->SetXY(140,103);
			$pdf->Write(0,$cargo[1+$i]);
			$pdf->SetXY(50,113);
			if ($departamento[1+$i]==15 || $departamento[1+$i]==7) {
					//$pdf->Write(0,'Ana Valverde');
				if ($cliente=='2055454574') 
					{
						$pdf->Write(0,'Cecilia Or�');
					}
				//PECSA
				if ($cliente=='2025903307')
					{
						$pdf->Write(0,'Esperanza Guillen');
					}
			}
			else
			{
				if ($departamento[1+$i]==13) {
					$pdf->Write(0,'Claudia Quintana');
				}
				if ($departamento[1+$i]==20) {
					$pdf->Write(0,'Diana morales');
				}
				if ($departamento[1+$i]==14) {
					$pdf->Write(0,'Maria Diaz');
				}
			}
			$pdf->SetXY(140,113);
			if ($departamento[1+$i]==15 || $departamento[1+$i]==7) {
					if ($cliente=='2055454574') {
						$pdf->Write(0,'08:30 am');
					}
					if ($cliente=='2025903307')
					{
						$pdf->Write(0,'08:30 am');
					}
			}
			else
			{
				$pdf->Write(0,'08:30 am');
			}
			$pdf->SetXY(50,123);
			if ($departamento[1+$i]==15 || $departamento[1+$i]==7) {
					if ($cliente=='2055454574') {
						$pdf->Write(0,'');
					}
					//PECSA
					if ($cliente=='2025903307')
					{
						$pdf->Write(0,'');
					}	
			}
			else
			{
				if ($departamento[1+$i]==13) {
					$pdf->Write(0,'E/S LARCO');
				}
				else
				{
					if ($departamento[1+$i]==20) {
						$pdf->Write(0,'E/S MEGA');
					}
					else
					{
						if ($departamento[1+$i]==14) {
							$pdf->Write(0,'E/S SANTA ELENA');
						}
						else
						{
							$pdf->Write(0,utf8_decode($estacion[1+$i]));
						}
					}					
				}
			}
		}
		//--------------------------------------------
		$pdf->SetXY(50,161);
		if (2+$i<$contador) {
			$pdf->Write(0,utf8_decode($nombre[2+$i].' '.$apellidoPaterno[2+$i].' '.$apellidoMaterno[2+$i]));
			$pdf->SetXY(50,171);
			$pdf->Write(0,date("d/m/Y", strtotime($fechaAlta[2+$i])));
			$pdf->SetXY(140,171);
			$pdf->Write(0,$cargo[2+$i]);
			$pdf->SetXY(50,181);
			if ($departamento[2+$i]==15 || $departamento[2+$i]==7) {
					if ($cliente=='2055454574') 
					{
						$pdf->Write(0,'Cecilia Or�');
					}
				//PECSA
				if ($cliente=='2025903307')
					{
						$pdf->Write(0,'Esperanza Guillen');
					}
			}
			else
			{
				if ($departamento[2+$i]==13) {
					$pdf->Write(0,'Claudia Quintana');
				}
				if ($departamento[2+$i]==20) {
					$pdf->Write(0,'Diana morales');
				}
				if ($departamento[2+$i]==14) {
					$pdf->Write(0,'Maria Diaz');
				}
			}
			$pdf->SetXY(140,181);
			if ($departamento[2+$i]==15 || $departamento[2+$i]==7) {
					if ($cliente=='2055454574') {
						$pdf->Write(0,'08:30 am');
					}
					if ($cliente=='2025903307')
					{
						$pdf->Write(0,'08:30 am');
					}
			}
			else
			{
				$pdf->Write(0,'08:30 am');
			}
			$pdf->SetXY(50,191);
			if ($departamento[2+$i]==15 || $departamento[2+$i]==7) {
					if ($cliente=='2055454574') {
						$pdf->Write(0,'');
					}
					//PECSA
					if ($cliente=='2025903307')
					{
						$pdf->Write(0,'');
					}	
				//$pdf->Write(0,'E/S TAVIRSA');
			}
			else
			{
				if ($departamento[2+$i]==13) {
					$pdf->Write(0,'E/S LARCO');
				}
				else
				{
					if ($departamento[2+$i]==20) {
						$pdf->Write(0,'E/S MEGA');
					}
					else
					{
						if ($departamento[2+$i]==14) {
							$pdf->Write(0,'E/S SANTA ELENA');
						}
						else
						{
							$pdf->Write(0,utf8_decode($estacion[2+$i]));
						}
					}
				}
			}
		}
		//--------------------------------------------
		$pdf->SetXY(50,229);
		if (3+$i<$contador) {
			$pdf->Write(0,utf8_decode($nombre[3+$i].' '.$apellidoPaterno[3+$i].' '.$apellidoMaterno[3+$i]));
			$pdf->SetXY(50,239);
			$pdf->Write(0,date("d/m/Y", strtotime($fechaAlta[3+$i])));
			$pdf->SetXY(140,239);
			$pdf->Write(0,$cargo[3+$i]);
			$pdf->SetXY(50,249);
			if ($departamento[3+$i]==15 || $departamento[3+$i]==7) {
					if ($cliente=='2055454574') 
					{
						$pdf->Write(0,'Cecilia Or�');
					}
				//PECSA
				if ($cliente=='2025903307')
					{
						$pdf->Write(0,'Esperanza Guillen');
					}
			}
			else
			{
				if ($departamento[3+$i]==13) {
					$pdf->Write(0,'Claudia Quintana');
				}
				if ($departamento[3+$i]==20) {
					$pdf->Write(0,'Diana morales');
				}
				if ($departamento[3+$i]==14) {
					$pdf->Write(0,'Maria Diaz');
				}
			}
			$pdf->SetXY(140,249);
			if ($departamento[3+$i]==15 || $departamento[3+$i]==7) {
					if ($cliente=='2055454574') {
						$pdf->Write(0,'08:30 am');
					}
					if ($cliente=='2025903307')
					{
						$pdf->Write(0,'08:30 am');
					}
			}
			else
			{
				$pdf->Write(0,'08:30 am');
			}
			$pdf->SetXY(50,259);
			if ($departamento[3+$i]==15 || $departamento[3+$i]==7) {
					if ($cliente=='2055454574') {
						$pdf->Write(0,'');
					}
					//PECSA
					if ($cliente=='2025903307')
					{
						$pdf->Write(0,'');
					}
			}
			else
			{
				if ($departamento[3+$i]==13) {
					$pdf->Write(0,'E/S LARCO');
				}
				else
				{
					if ($departamento[3+$i]==20) {
						$pdf->Write(0,'E/S MEGA');
					}
					else
					{
						if ($departamento[3+$i]==14) {
							$pdf->Write(0,'E/S SANTA ELENA');
						}
						else
						{
							$pdf->Write(0,utf8_decode($estacion[3+$i]));
						}
					}
				}
			}
		}
	}
	$pdf->Output();
?>
