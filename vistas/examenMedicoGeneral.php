<?php
require '../php/funciones.php';
@session_start();
$idUsuario=$_SESSION['id'];
$admin=$_SESSION['admin'];
$fechaAlta=$_GET['fechaAlta'];

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
  die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("T - Soluciona")
               ->setLastModifiedBy("T - Soluciona")
               ->setTitle("Reporte Ingreso")
               ->setSubject("Reporte ingreso")
               ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
               ->setKeywords("office 2007 openxml php")
               ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('C4', 'N°')
            ->setCellValue('D4', 'FECHA DE ALTA/INICIO')
            ->setCellValue('E4', 'TALLA DE BOTAS')
            ->setCellValue('F4', 'TALLA DE UNIFORME')
            ->setCellValue('G4', 'TELEFONO')
            ->setCellValue('H4', 'CORREO')
            ->setCellValue('I4', 'CARGO')
            ->setCellValue('J4', 'CONSULTORA')
            ->setCellValue('K4', 'EMPRESA')
            ->setCellValue('L4', 'CÓDIGO DE ESTACIÓN')
            ->setCellValue('M4', 'ESTACION')
            ->setCellValue('N4', 'DEPARTAMENTO')
            ->setCellValue('O4', 'Apellido parterno trabajador')
            ->setCellValue('P4', 'Apellido materno trabajador')
            ->setCellValue('Q4', 'Nombres trabajador')
            ->setCellValue('R4', 'Sexo')
            ->setCellValue('S4', 'Fec.Nac.')
            ->setCellValue('T4', 'Est.Civil')
            ->setCellValue('U4', 'Dirección')
            ->setCellValue('V4', 'Departamento')
            ->setCellValue('W4', 'Provincia')
            ->setCellValue('X4', 'Distrito')
            ->setCellValue('Y4', 'Desc.Nivel.Educ')
            ->setCellValue('Z4', 'DNI')
            ->setCellValue('AA4', 'Sist.Prev.Pens.')
            ->setCellValue('AB4', 'F.Afil.SPP')
            ->setCellValue('AC4', 'Cód.Unico SPP')
            ->setCellValue('AD4', 'TIPO DE CONTRATO')
            ->setCellValue('AE4', 'TURNO')
            ->setCellValue('AF4', 'OBSERVACION')
            ->setCellValue('AG4', 'VERDE')
            ->setCellValue('AH4', 'AMBAR')
            ->setCellValue('AI4', 'ROJO')
            
            ->setCellValue('AG3', 'QUANTUM');
            
            


$styleArray = array(
  'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 9,
        'name'  => 'Calibri'
  ),
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);

$objPHPExcel->getActiveSheet()->getStyle('AG3:AI3')->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('C4:AI4')->applyFromArray($styleArray);
unset($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('C4:AI4')->getAlignment()->setWrapText(true); 

/*

$styleArray1 = array(
  'font'  => array(
        'color' => array('rgb' => '44546A')
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);

$objPHPExcel->getActiveSheet()->getStyle('C4:F4')->applyFromArray($styleArray1);
unset($styleArray1);
*/
//borde celdas
$styleBorder = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
);

//---------------autosize------------
foreach(range('C','Z') as $columnID) { 
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID) 
     ->setAutoSize(true); 
} 

$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true); 
//---------------END autosize------------

//------ ANCHO DE FILA
$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(33.75);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false); 
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false); 
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13.43);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false); 
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false); 
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10.71);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(false); 
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(37);
//---- END ANCHO FILA

//-------COMBINAR CELDAS
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AG3:AI3');
//-----END COMBINAR CELDAS

//-------------COLOR DE RELLENO
$objPHPExcel->getActiveSheet()->getStyle('M4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

$objPHPExcel->getActiveSheet()->getStyle('D4:K4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

$objPHPExcel->getActiveSheet()->getStyle('O4:AE4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF00');

//-------------END COLOR DE RELLENO

$query = ejecutarQuery("SELECT count(distinct persona.idPersona) as contador FROM persona 
                      INNER JOIN asignado ON persona.idPersona=asignado.idPersona 
                      INNER JOIN detalle_requerimiento ON detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_requerimiento 
                      INNER JOIN requerimiento ON detalle_requerimiento.idRequerimiento=requerimiento.idRequerimiento 
                      INNER JOIN distrito on persona.idDistrito=distrito.idDistrito
                      INNER JOIN provincia on provincia.idProvincia=distrito.idProvincia
                      INNER JOIN departamento on departamento.idDepartamento=provincia.idDepartamento
                      INNER JOIN estacion on estacion.idEstacion=detalle_requerimiento.idEstacion
                      WHERE persona.estado IN (8,19,20, 27) and requerimiento.fechaAlta='$fechaAlta'");
$result = mysqli_fetch_assoc($query);
$contador=$result['contador']+5;
$i=5;
$query1 = ejecutarQuery("SELECT persona.*, asignado.idDetalle_requerimiento as idDet, persona.idPersona as idPersona, requerimiento.idCliente as idCliente FROM persona 
                      INNER JOIN asignado ON persona.idPersona=asignado.idPersona 
                      INNER JOIN detalle_requerimiento ON detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_requerimiento 
                      INNER JOIN requerimiento ON detalle_requerimiento.idRequerimiento=requerimiento.idRequerimiento 
                      INNER JOIN distrito on persona.idDistrito=distrito.idDistrito
                      INNER JOIN provincia on provincia.idProvincia=distrito.idProvincia
                      INNER JOIN departamento on departamento.idDepartamento=provincia.idDepartamento
                      INNER JOIN estacion on estacion.idEstacion=detalle_requerimiento.idEstacion
                      WHERE persona.estado IN (8,19,20, 27) and requerimiento.fechaAlta='$fechaAlta' group by persona.idPersona");

while($row=mysqli_fetch_assoc($query1)){
  $dd=ejecutarQuery("SELECT * FROM detalle_requerimiento WHERE idDetalle_requerimiento='$row[idDet]' "); 
  $dtr = mysqli_fetch_assoc($dd);

  $rr=ejecutarQuery("SELECT * FROM requerimiento WHERE idRequerimiento='$dtr[idRequerimiento]'"); 
  $rq = mysqli_fetch_assoc($rr);
  $idRequerimiento=$rq['idRequerimiento'];

  $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro, departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia WHERE distrito.idDistrito = '$row[idDistrito]' ");
   $prov = mysqli_fetch_assoc($pro);

  $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
  $gener = mysqli_fetch_assoc($gen);

  $sp= ejecutarQuery("SELECT nombre from spp where idSpp = '$row[idSpp]' ");
  $spp = mysqli_fetch_assoc($sp);

  $car= ejecutarQuery("SELECT nombre from cargo where idcargo = '$dtr[idcargo]' ");
  $cargo = mysqli_fetch_assoc($car);

  $es= ejecutarQuery("SELECT nombre,codigo from estacion where idEstacion = '$dtr[idEstacion]' ");
  $est = mysqli_fetch_assoc($es);

  $ed= ejecutarQuery("SELECT nombre from nivelEducativo where idNivelEducativo = '$row[idNivelEducativo]' ");
  $edu = mysqli_fetch_assoc($ed);
                        
  $cl= ejecutarQuery("SELECT * from clinica where idClinica = '$dtr[idClinica]' ");
  $cli = mysqli_fetch_assoc($cl);

  $qua= ejecutarQuery("SELECT * from quantum where idQuantum=$row[idQuantum]");
  $quantum = mysqli_fetch_assoc($qua);

  $tur= ejecutarQuery("SELECT * from turno_carta inner join detalle_requerimiento on detalle_requerimiento.idTurno_carta=turno_carta.idTurno_carta where detalle_requerimiento.idDetalle_requerimiento = '$dtr[idDetalle_requerimiento]' ");
  $turno = mysqli_fetch_assoc($tur);

  $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue("C$i", $i-4)
      ->setCellValue("D$i", date("d/m/Y", strtotime($rq['fechaAlta'])))
      ->setCellValue("E$i", $row['tallaBotas'])
      ->setCellValue("F$i", $row['tallaUniforme'])
      ->setCellValue("G$i", $row['telefono'])
      ->setCellValue("H$i", strtolower($row['email']))
      ->setCellValue("I$i", $cargo['nombre'])
      ->setCellValue("J$i", "T-SOLUCIONA")
      ->setCellValue("K$i", ($row['idCliente'] == 2055454574 )?"COESTI":"PES")
      ->setCellValue("L$i", $est['codigo'])
      ->setCellValue("M$i", $est['nombre'])
      ->setCellValue("N$i", $prov['dep'])
      ->setCellValue("O$i", $row['apellidoPaterno'])
      ->setCellValue("P$i", $row['apellidoMaterno'])
      ->setCellValue("Q$i", $row['nombres'])
      ->setCellValue("R$i", $gener['nombre'])
      ->setCellValue("S$i", date("d/m/Y", strtotime($row['fechaNacimiento'])))
      ->setCellValue("T$i", $row['estadoCivil'])
      ->setCellValue("U$i", strtoupper($row['direccion']))
      ->setCellValue("V$i", $prov['dep'])
      ->setCellValue("W$i", $prov['pro'])
      ->setCellValue("X$i", $prov['dis'])
      ->setCellValue("Y$i", $edu['nombre'])
      ->setCellValue("Z$i", (strlen($row['idPersona'])==7)?'0'.$row['idPersona']:$row['idPersona'])
      ->setCellValue("AA$i", $spp['nombre'])
      ->setCellValue("AB$i", (date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001')?"NO REGISTRA":date("d/m/Y", strtotime($row['fechaSpp'])))
      ->setCellValue("AC$i", ($row['codigoSpp']==null)?"NO REGISTRA":$row['codigoSpp'])
      ->setCellValue("AD$i", " ")
      ->setCellValue("AE$i", $turno['nombre'])
      ->setCellValue("AF$i", strtoupper($row['observacion']))
      ->setCellValue("AG$i", ($quantum['nombre']=='VERDE')?"X":"")
      ->setCellValue("AH$i", ($quantum['nombre']=='AMBAR')?"X":"")
      ->setCellValue("AI$i", ($quantum['nombre']=='ROJO')?"X":"");
      $i++;
}

$contador=$contador-1;
$objPHPExcel->getActiveSheet()->getStyle("C5:AI$contador")->applyFromArray($styleBorder);
unset($styleBorder);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Reporte Ingreso');
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte Ingreso.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
