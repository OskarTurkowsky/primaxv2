<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>T-SOLUCIONA</title>
  <link rel="shortcut icon" type="image/x-icon" href="../img/logo.ico" />       
  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" href="../css/custom.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <!--DATATABLES-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-yellow sidebar-mini">
  <div class="wrapper">
    <!-- MENU HORIZONTAL -->
    <header class="main-header">
      <!-- Logo -->
      <a href="home.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>T</b>-S</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>T-SOLUCIONA</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo $_SESSION['img']; ?>" class="user-image" alt="Image">
                <span class="hidden-xs"><?php echo $_SESSION['nombres']; ?></span>
              </a>
              <ul class="dropdown-menu">
                  <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo $_SESSION['img']; ?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['nombres'];?>
                      <small><?php echo $_SESSION['cargo'];?></small>
                    </p>
                </li>
                <?php 
                $psicologo = $_SESSION['correo'];
                 ?>
                  <!-- Menu Body -->
                
                  <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="perfil.php" class="btn btn-default btn-flat">Perfil</a>
                  </div>
                  <div class="pull-right">
                    <a href="../php/cerrar-sesion.php" class="btn btn-default btn-flat">Cerrar Sesi&oacuten</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            
          </ul>
        </div>
        
        <div id="reloj">
          <p id="diaSemana"></p>            
          <p id="dia"></p>
          <p>de </p>
          <p id="mes"></p>
          <p>de </p>
          <p id="year"></p>&nbsp;&nbsp;&nbsp;&nbsp;
          <p id="horas"></p>
          <p>:</p>
          <p id="minutos"></p>
          <p>:</p>
          <p id="segundos"></p>
          <p id="ampm"></p>
          <p></p>
        </div>
        
        
      </nav>
    </header>
    <!-- FIN MENU HORIZONTAL -->
    
       <!-- MENU VERTICAL -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo $_SESSION['img']; ?>" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo $_SESSION['nombres']?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU DE NAVEGACI&oacuteN</li>
          <li>
            <a href="home.php">
              <i class="fa  fa-angellist"></i>
              <span>HOME</span>
            </a>
          </li>
          
          <li class="treeview">
            <a href="#">
              <i class="fa fa-address-card"></i>
              <span>Reclutamiento</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
              <ul class="treeview-menu">
                <li><a href="nuevoReferido.php">
                  <i class="fa fa-circle-o"></i>Registrar</a>
                </li>
                <li><a href="controlAsistencia.php">
                  <i class="fa fa-circle-o"></i>Control de asistencia</a>
                </li>
                <li><a href="listarReferido.php">
                  <i class="fa fa-circle-o"></i>Editar</a>
                </li>
                <li><a href="controlReprogramacion.php">
                  <i class="fa fa-circle-o"></i>Reprogramación</a>
                </li>
                <li><a href="reporteControlAsistencia.php">
                  <i class="fa fa-circle-o"></i>Reporte control de asistencia</a>
                </li>
                <li><a href="reporteDetalles.php">
                  <i class="fa fa-circle-o"></i>Detalles control de asistencia</a>
                </li>
              </ul> 
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-address-book-o"></i>
              <span>Back Up</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
              <ul class="treeview-menu">
                <li><a href="nuevo.php">
                  <i class="fa fa-circle-o"></i>Registrar</a>
                </li>
                <li><a href="backup.php">
                  <i class="fa fa-circle-o"></i>Reporte</a>
                </li>
                <li><a href="buscar.php">
                  <i class="fa fa-circle-o"></i>Buscar</a>
                </li>
                <li><a href="sindisponibilidad.php">
                  <i class="fa fa-circle-o"></i>Sin disponibilidad</a>
                </li>
              </ul> 
            </a>
          </li>
          <li>
            <a href=requerimientos.php>
              <i class="fa fa-book"></i>
              <span>Requerimientos</span>
            </a>
          </li>

          <li>
            <a href=postulantes.php>
              <i class="fa fa-thumbs-up"></i>
              <span>Postulantes</span>
            </a>
          </li>
          <?php if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3') 
          {
            ?>
            <li>
              <a href=postulantesGeneral.php>
                <i class="fa fa-users"></i>
                <span>Postulantes General</span>
              </a>
            </li>
            <?php
          }
          ?>
          <li>
            <a href="reporteVerificacion.php">
              <i class="fa fa-search"></i>
              <span>Reporte de Verificacion</span>
            </a>
          </li>
          
          <?php if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3') {
          ?>
            <li>
              <a href="../php/estadoAntecedentes.php">
                <i class="fa fa-file-text-o"></i>
                <span>Para Antecedentes</span>
              </a>
            </li>
            <!--
          <li>
            <a href=examenRequerimiento.php>
              <i class="fa fa-book"></i>
              <span>Para Examen médico</span>
            </a>
          </li>
          !-->
          <li>
            <a href=paraIngreso.php>
              <i class="fa fa-book"></i>
              <span>Para Ingreso</span>
            </a>
          </li>

          <?php 
          } 
          ?>

          <li class="treeview">
            <a href="#">
              <i class="fa fa-eye"></i>
              <span>Verificaciones Pendientes</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="pendientesAntecedentes.php">
                  <i class="fa fa-circle-o"></i>Antecedentes
                </a>
              </li>
              <!--
              <li>
                <a href="examenMedico.php">
                  <i class="fa fa-circle-o"></i>Examenes Médicos
                </a>
              </li>
              --> 
            </ul> 
          </li>
                   
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Trabajadores</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3' || $_SESSION['id'] == '46587753') {
              ?> 
              <li>
                <a href="elegirTrabajadores.php">
                  <i class="fa fa-street-view"></i>
                  <span>Elegir Trabajadores</span>
                </a>
              </li>
              <?php }
              else
                {?>
              <li>
                <a href="listarTrabajadores.php">
                  <i class="fa fa-street-view"></i>
                  <span>Postulantes en proceso</span>
                </a>
              </li>
              <?php } ?>
              <li>
                <a href="reporteTrabajadores.php">
                  <i class="fa fa-child"></i>
                  <span>Reporte de Trabajadores</span>
                </a>
              </li>
            </ul> 
          </li>
          
          <li>
            <a href="historial.php">
              <i class="fa fa-male"></i>
              <span>Buscar Postulantes</span>           
            </a>
          </li>

          <?php if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3' || $_SESSION['idDistrito'] == '150101') {
            ?> 
          <li>
            <a href="distritosEstaciones.php">
              <i class="fa fa-building"></i>
              <span>Estaciones</span> 

            </a>
          </li>
        <?php } ?>

        <?php if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3') 
          {
            ?>
            <li>
              <a href=contrato.php>
                <i class="fa fa-clipboard"></i>
                <span>Tipo contrato</span>
              </a>
            </li>
            <?php
          }
          ?>


          <?php if ($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3' || $_SESSION['admin'] == '2') {
            ?>           
          <li class="treeview">
            <a href="#">
              <i class="fa fa-user-plus"></i>
              <span>Reclutadores</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="reporteReclutadores.php">
                  <i class="fa fa-files-o"></i>
                  <span>Reporte de Reclutadores</span>
                </a>
              </li>
              <?php if($_SESSION['admin'] == '1' || $_SESSION['admin'] == '3'){
                ?>
                <li>
                  <a href="configuracionReclutador.php">
                    <i class="fa fa-cog"></i>
                    <span>Configuración</span>
                  </a>
                </li>  
                <?php
              }
               ?>                         
            </ul> 
          </li>
          <?php }?>

          <?php if ($_SESSION['admin'] == '3' || $_SESSION['admin'] == '1') {
            ?>  
          <li class="treeview">
            <a href="#">
              <i class="fa fa-gavel"></i>
              <span>Antecedentes</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="verificarAntecedentes.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Para Antecedentes</span>
                </a>
              </li>
              <li>
                <a href="confirmarAntecedentes.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Verificar Antecedentes</span>
                </a>
              </li>
              <li>
                <a href="reporteAntecedentes.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Reporte de Antecedentes</span>
                </a>
              </li>
              <li>
                <a href="estadoAntecedentes.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Estado de Antecedentes</span>
                </a>
              </li>
            </ul> 
          </li>
         
           <!--
          <li class="treeview">
            <a href="#">
              <i class="fa fa-book"></i>
              <span>Reporte Pecsa</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="reportePecsaLima.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Lima y Callao</span>
                </a>
              </li>
              <li>
                <a href="reportePecsaOtros.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Otras provincias</span>
                </a>
              </li> 
            </ul> 
          </li>
          -->

          <?php }?>

          <li class="treeview">
            <a href="#">
              <i class="fa fa-folder-open"></i>
              <span>Formatos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>  
              </span>
            </a>
            <ul class="treeview-menu">
              <!--
              <li>
                <a href="formatoCartaPresentacion.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Carta de presentación</span>
                </a>
              </li>
            -->
              <li>
                <a href="formatoCartaPresentacion2.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Carta de presentación</span>
                </a>
              </li>
              <li>
                <a href="formatoCheckList.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Check List</span>
                </a>
              </li>
              <li>
                <a href="formatoEnvioEntrevistas.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Formato de envío a entrevista</span>
                </a>
              </li> 
              <li>
                <a href="formatoEnvioEntrevistasCP.php">
                  <i class="fa fa-circle-o"></i>
                  <span>Nuevo formato de entrevista</span>
                </a>
              </li> 
            </ul> 
          </li>

          <?php if ($_SESSION['correo'] == 'rperez@t-soluciona.com.pe') {
            ?> 
          <li>
            <a href="reporteGeneral.php">
              <i class="fa fa-building"></i>
              <span>Reporte general</span>             
            </a>
          </li>
          <?php } ?>

          <li>
            <a href="video.php">
              <i class="fa fa-video-camera"></i>
              <span>Video</span>             
            </a>
          </li>
         
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
    <!-- FIN MENU VERTICAL-->