﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$dni = $_GET['id'];
?>


<?php include('header.php'); ?>
    
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Referido
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i>Referido</a></li>
          <li class="active">Editar</li>
        </ol>
      </section>

      <?php  
        $ok = ejecutarQuery("SELECT * FROM persona where idPersona='$dni'");
        $oks = mysqli_fetch_assoc($ok);
        $bac = ejecutarQuery("SELECT * FROM backup where idBackup='$dni'");
        $back = mysqli_fetch_assoc($bac);
        $ref = ejecutarQuery("SELECT * FROM referido where idReferido='$dni'");
        $referido = mysqli_fetch_assoc($ref);
        $asd = $oks['idDistrito'];
        $gen = $oks['idGenero'];
        $var = ejecutarQuery("SELECT p.idProvincia, dep.idDepartamento FROM distrito d INNER JOIN provincia p on d.idProvincia = p.idProvincia INNER JOIN departamento dep on p.idDepartamento = dep.idDepartamento WHERE d.idDistrito = $asd");
        $varr = mysqli_fetch_assoc($var);
        $divTelefone = explode(" / ", $oks['telefono']);
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Formulario de Edicion</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/editarReferido.php?dni=<?php echo $dni?>" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required value="<?php 
                            if (strlen($dni)==7) {
                              echo '0'.$dni;
                            }
                            else
                            {
                              echo $dni;
                            }
                          ?>" disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Paterno</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="paterno" required value="<?php echo $oks['apellidoPaterno']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Apellido Materno</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="materno" required value="<?php echo $oks['apellidoMaterno']?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Nombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required value="<?php echo $oks['nombres']?>">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Tel&eacute;fono 1</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono1" class="form-control pull-right" required="" value="<?php echo $divTelefone[0] ?>">
                        </div>
                      </div>                     
                    </div>
                    <div class="col-md-4">
                      
                      <div class="form-group">
                        <label>Tel&eacute;fono 2</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono2" class="form-control pull-right" value="<?php echo $divTelefone[1] ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Tel&eacute;fono 3</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" name="telefono3" class="form-control pull-right" value="<?php echo $divTelefone[2] ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Departamento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="departamento" id="" onchange="from(document.form1.departamento.value,'midiv','../php/provincias.php');">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM departamento order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>" 
                                <?php if ($eee['idDepartamento']==$varr['idDepartamento']) {
                                  echo "selected='selected'";
                                } 
                                ?>
                              >
                                <?php echo $eee['nombre']; ?>
                              </OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Provincia</label>
                        <div class="input-group" id="midiv">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="provincia" id="provincias" onchange="from(document.form1.provincia.value,'midiv2','../php/distritos.php');">
                            <?php 
                            $subconsulta = "(select idDepartamento from provincia where idProvincia =".$varr['idProvincia'].")";
                            $consulta = ejecutarQuery("SELECT * FROM provincia where idDepartamento = $subconsulta order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idProvincia']; ?>" <?php if ($eee['idProvincia']==$varr['idProvincia']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>  
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label>Distrito</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="distrito" id="distritos">
                            <?php 
                            $subconsulta = "(select idProvincia from distrito where idDistrito =".$oks['idDistrito'].")";
                            $consulta = ejecutarQuery("SELECT * FROM distrito where idProvincia = $subconsulta order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDistrito']; ?>" <?php if ($eee['idDistrito']==$oks['idDistrito']) {
                                echo "selected='selected'";
                              } 
                               ?>><?php echo $eee['nombre']; ?></OPTION>  
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>           
                    </div>
                    <div class="col-md-4">
                      <!--<div class="form-group">
                        <label>Puesto al que postula</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="puesto">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM puesto");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idPuesto']; ?>" <?php if ($eee['idPuesto']==$referido['idPuesto']) {
                                echo "selected='selected'";
                              } 
                               ?> > 
                                <?php echo $eee['nombre']; ?>
                                </OPTION>  
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div> -->
                      
                      <div class="form-group">
                        <label>Persona que da inducción</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="induccion" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM consultor where estado=1 order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idConsultor']; ?>" <?php if ($eee['idConsultor']==$referido['idInduccion']){?> selected="selected" <?php } ?> > <?php echo strtoupper($eee['nombre']); ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Psicologo entrevistador</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="psicologo" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM consultor where estado=1 order by nombre");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idConsultor']; ?>" <?php if ($eee['idConsultor']==$referido['idEntrevistador']){?> selected="selected" <?php } ?> > <?php echo strtoupper($eee['nombre']); ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <!--<div class="form-group">
                        <label>Aptitud</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="resultado" id="aptitud" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM resultado");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idResultado']; ?>" > 
                                <?php echo $eee['nombre']; ?>
                                </OPTION>     
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group" id="motivo"  style="display: none">
                        <label>Motivo</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <input type="text" class="form-control pull-right"  placeholder="Motivo"  name="motivo">
                        </div>
                      </div>-->
                      <div class="form-group">
                        <label>Observaciones</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-eye"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase; resize: vertical;min-height: 60px;" name="observacion" value="<?php echo $oks['observacion']?>" >
                        </div>
                      </div>
                      <br>
                      <center> <button type="submit" class="btn btn-primary pull-center">EDITAR</button> </center>  
                    </div>
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>