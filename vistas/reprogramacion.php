﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$dni = $_GET['id'];
?>


<?php include('header.php'); ?>
    
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Referido
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i>Referido</a></li>
          <li class="active">Editar</li>
        </ol>
      </section>

      <?php  
        $ok = ejecutarQuery("SELECT * FROM persona where idPersona='$dni'");
        $oks = mysqli_fetch_assoc($ok);
        $bac = ejecutarQuery("SELECT * FROM backup where idBackup='$dni'");
        $back = mysqli_fetch_assoc($bac);
        $ref = ejecutarQuery("SELECT * FROM referido where idReferido='$dni'");
        $referido = mysqli_fetch_assoc($ref);
        $asd = $oks['idDistrito'];
        $gen = $oks['idGenero'];
        $var = ejecutarQuery("SELECT p.idProvincia, dep.idDepartamento FROM distrito d INNER JOIN provincia p on d.idProvincia = p.idProvincia INNER JOIN departamento dep on p.idDepartamento = dep.idDepartamento WHERE d.idDistrito = $asd");
        $varr = mysqli_fetch_assoc($var);
        $divTelefone = explode(" / ", $oks['telefono']);
        $tr= ejecutarQuery("SELECT turnos.idTurno as turno from entrevista_referido 
            inner join referido on entrevista_referido.idReferido=referido.idReferido 
            inner join entrevista on entrevista.idEntrevista=entrevista_referido.idEntrevista
            inner join fecha on entrevista.idFecha=fecha.idFecha
            inner join turnos on entrevista.idTurno=turnos.idTurno
            where referido.idReferido='$referido[idReferido]' and entrevista_referido.estado=1");
        $turno = mysqli_fetch_assoc($tr);
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header">
                <h3 class="box-title">Formulario de Edicion</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/editarReprogramacion.php?dni=<?php echo $dni?>" method="POST" enctype="multipart/form-data" name="form1">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required value="<?php 
                            if (strlen($dni)==7) {
                              echo '0'.$dni;
                            }
                            else
                            {
                              echo $dni;
                            }
                          ?>" disabled>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">      
                      <div class="form-group">
                        <label>Turno *</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-play"></i>
                          </div>
                          <select name="turno" id="turno" class="form-control pull-right">
                            <?php                           
                            $consulta = ejecutarQuery("SELECT * from turnos");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idTurno']; ?>"  <?php if ($eee['idTurno']==$turno['turno']) {
                                  echo "selected='selected'";
                                } 
                                 ?> ><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>       
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Fecha de Entrevista *</label>
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="date" class="form-control pull-right" name="from_date" required="" value="<?php echo $back['fechaEvaluacion']?>" id="from_date">
                        </div>
                      </div> 
                      <br>
                      <div class="form-group" id="details" hidden="">
                        <center>
                          <h4> 
                            <span class="label label-danger">
                              <label id="result"></label>
                            </span>
                          </h4>
                          <input type="text" name="resultado" id="resultado" hidden="">
                        </center>                        
                      </div>   
                    </div>
                    <center> <button type="submit" class="btn btn-primary pull-center">EDITAR</button> </center>  
                  </form>
                </div>
              </div>
              <div class="box-footer"></div>
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){  

    document.getElementById('from_date').addEventListener('change', function(){
        var from_date = $('#from_date').val();  
        var turno = $('#turno').val(); 
        if(turno != '' && from_date != '')  
        {  
          $.ajax({  
                url:"filter.php",  
                method:"POST",  
                data:{from_date:from_date, turno:turno},  
                success:function(data)  
                {  
                  $('#resultado').val(data);
                  if (data.trim()=='EMPTY' || data.trim()=='OK' || data.trim()=='EMPTY ENTREVISTA') {
                    $("#details").hide();
                  }
                  else
                  {
                    $("#details").show();
                    $('#result').html(data);
                  }
                }  
          });  
        }  
      }
    );

  document.getElementById('turno').addEventListener('change', function(){
        var from_date = $('#from_date').val();  
        var turno = $('#turno').val(); 
        
        if(turno != '' && from_date != '')  
        {  
          $.ajax({  
                url:"filter.php",  
                method:"POST",  
                data:{from_date:from_date, turno:turno},  
                success:function(data)  
                {  
                  $('#resultado').val(data);
                  if (data.trim()=='EMPTY' || data.trim()=='OK' || data.trim()=='EMPTY ENTREVISTA') {
                    $("#details").hide();
                  }
                  else
                  {
                    $("#details").show();
                    $('#result').html(data);
                  }
                }  
          });  
        }  
      }
  );  
  });  
</script>