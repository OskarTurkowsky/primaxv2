<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
<link href='../fullcalendar/dist/core/main.css' rel='stylesheet' />
<link href='../fullcalendar/dist/daygrid/main.css' rel='stylesheet' />
<link href='../fullcalendar/dist/timegrid/main.css' rel='stylesheet' />
<script src='../fullcalendar/dist/core/main.js'></script>
<script src='../fullcalendar/dist/interaction/main.js'></script>
<script src='../fullcalendar/dist/daygrid/main.js'></script>
<script src='../fullcalendar/dist/timegrid/main.js'></script>
<script src='../fullcalendar/core/locales-all.js'></script>
<script src='../fullcalendar/list/main.js'></script>
<link href='../fullcalendar/node_modules/dragula/dist/dragula.css' rel='stylesheet' />
<script src='../fullcalendar/node_modules/dragula/dist/dragula.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  var day;
  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var initialLocaleCode = 'es';

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      defaultDate: '2019-02-12',
      locale: initialLocaleCode,
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      select: function(arg) {
        day=arg.start.toLocaleDateString("zh-Hans-CN");
        //console.log(day);
        document.getElementById("fecha").innerHTML = day;

      /*var title = prompt('Event Title:');
        if (title) {
          calendar.addEvent({
            title: title,
            start: arg.start,
            end: arg.end,
            allDay: arg.allDay
          })
        }*/
        calendar.unselect()
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: []
    });

    calendar.render();
  });
</script>
<style>

  #external-events {
    float: left;
    width: 120px;
    padding: 0 5px;
    border: 1px solid #ccc;
    background: #eee;
    text-align: left;
    margin-top: 100px;
    margin-left: 10px;
  }

  #external-events h4 {
    font-size: 18px;
    margin-top: 0;
    padding-top: 1em;
  }

  #external-events .fc-event {
    margin: 10px 0;
    cursor: pointer;
    font-size: 16px;
  }

  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
  }

  #external-events p input {
    margin: 0;
    vertical-align: middle;
  }

  #calendar {
    margin-top: 20px;
    float: right;
    width: 900px;
  }

</style>
  <div class="content-wrapper">

    <div id='external-events'>
      <center>
        <h4> <b>Fecha: </b><label id="fecha"></label></h4>
        <input type="text" id="fechas" name="fecha" hidden>
      </center>
      
      <div id='external-events-list'>
        <?php                   
          $consulta = ejecutarQuery("SELECT * FROM turnos order by idTurno");
          while($turnos=mysqli_fetch_assoc($consulta)){
            ?>
            <button class="fc-event" id="turno" value="<?php echo $turnos['idTurno']; ?>">
              <?php echo $turnos['nombre']; ?>
            </button>   
            <?php
          }
        ?>
          <h4>Cupos:
            <label id="respuesta"></label>
          </h4>
      </div>
      <center>
        <h4>
          
        </h4>
      </center>
      
    </div>
    <div id='calendar'></div>

    <div style='clear:both'></div>

  </div>
<?php include('footer.php'); ?>
<script >
   $("button").click(function() {
        var from_date = $("#fecha").attr("id");
        console.log(from_date);
        var turno = $('#turno').val(); 
        console.log(turno);
        if(turno != '' && from_date != '')  
        {  
          $.ajax({  
                url:"filter.php",  
                method:"POST",  
                data:{from_date:from_date, turno:turno},  
                success:function(data)  
                {  
                  $('#respuesta').val(data);
                  if (data.trim()=='EMPTY' || data.trim()=='OK' || data.trim()=='EMPTY ENTREVISTA') {
                    $("#details").show();
                  }
                  else
                  {
                    $("#details").show();
                    $('#respuesta').html(data);
                    
                  }
                }  
          });  
        }
        }
        );  
</script>