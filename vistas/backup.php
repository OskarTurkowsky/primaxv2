<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}


?>

<?php include('header.php'); ?>
    <script type="text/javascript">
    function validaremail(e) { // 1
        tecla = (document.all) ? e.keyCode : e.which; // 2
        if (tecla==8) return true; // 3
        patron =/[A-Za-z\s0-9@._-]/; // 4
        te = String.fromCharCode(tecla); // 5
        return patron.test(te); // 6
    }
    </script>
    <script type="text/javascript">
    function validar(e) { // 1
        tecla = (document.all) ? e.keyCode : e.which; // 2
        if (tecla==8) return true; // 3
        patron =/[A-Za-z\s]/; // 4
        te = String.fromCharCode(tecla); // 5
        return patron.test(te); // 6
    }
    </script>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Back up
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-thumbs-up"></i> Back up</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Back up</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                    <th></th>
                    <th class="text-center" style="min-width: 100px">OPCIONES</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center" style="min-width: 200px">NOMBRES</th>
                    <th class="text-center">SEXO</th>
                    <th class="text-center">TELEFONO</th>
                    <th class="text-center">DIRECCION </th> 
                     <th class="text-center">CITADO POR</th>

                    <th class="text-center">FECHA EVALUACION</th>   
                    <th class="text-center">A. POLICIAL</th>
                    <th class="text-center">C. SANIDAD</th>
                    <th class="text-center">DISPONIBILIDAD</th>                    
                    <th>SPP <font color="white">---------------------</font></th>
                    <th>FECHA SPP  <font color="white">-----------</font></th>
                    <th>CODIGO SPP  <font color="white">---------</font></th>
                    <th class="text-center">OBSERVACIONES</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $idUsuario=$_SESSION['id'];
                    $admin=$_SESSION['admin'];
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                    $abcc=mysqli_fetch_assoc($aa);

                    if ($admin==1 || $admin==3) {
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup where backup.disponibilidad!='NO' and persona.estado=0");
                    }
                    if ($admin==2){ 
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup inner join usuario on usuario.idUsuario=persona.idUsuario where backup.disponibilidad!='NO' and persona.estado=0 and usuario.idDistrito=$abcc[idDistrito]");
                    }
                    if ($admin==0){                    
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup where backup.disponibilidad!='NO' and persona.estado=0 and persona.idUsuario=$idUsuario"); 
                    }
                    while($row=mysqli_fetch_assoc($rs)){
                      $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                      $prov = mysqli_fetch_assoc($pro);

                      $gen= ejecutarQuery("SELECT nombre from genero where idGenero='$row[idGenero]'");
                      $gener = mysqli_fetch_assoc($gen);

                      $co= ejecutarQuery("SELECT nombre from consultor where idConsultor='$row[idConsultor]'");
                      $cons = mysqli_fetch_assoc($co);

                      $sp= ejecutarQuery("SELECT nombre from spp inner join persona on persona.idSpp=spp.idSpp where persona.idPersona='$row[idPersona]'");
                      $spp = mysqli_fetch_assoc($sp);

                  ?>                  
                      <tr bgcolor="white">
                        <td> </td>
                        <td class="text-center">
                          <a href="editarBackup.php?id=<?php echo $row['idPersona'];?>">
                            <button type='button' title="EDITAR" class='btn btn-warning btn-sm'>
                              <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                            </button>
                          </a>
                          <a href="#delete<?php echo $row['idPersona'];?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></a>
                        </td>
                        <td class="text-center"><?php 
                          if (strlen($row['idPersona'])==7) {
                            echo '0'.$row['idPersona'];
                          }
                          else
                          {
                            echo $row['idPersona'];
                          }
                         ?></td>
                        <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                        <td class="text-center"><?php echo $gener['nombre']; ?></td>
                        <td class="text-center"><?php echo $row['telefono']; ?></td>
                        <td class="text-center"><?php echo $prov['pro']."-".$prov['dis']; ?></td>
                        <td class="text-center"><?php echo $cons['nombre']; ?></td>
                        <td><font color="white">-</font><?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                        
                        <td><font color="white">------------</font><?php echo $row['policial']; ?></td>
                        <td><font color="white">------------</font><?php echo $row['sanidad']; ?></td>
                        <td><font color="white">-----</font><?php echo $row['disponibilidad']; ?></td>
                        <td><?php echo $spp['nombre']; ?></td>
                        <td><?php if(date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?></td>
                        <td><?php if($row['codigoSpp']=='') {echo 'NO REGISTRA'; } else  echo  $row['codigoSpp']; ?></</td>
                        <td><font color="white">-----</font><?php echo strtoupper($row['observacion']); ?></td>
                        
                      </tr>
                       <!--Eliminar Modal -->
                      <div id="delete<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarBackup.php?id=<?php echo $row['idPersona'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar Registro <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idPersona']; ?>,'example','../php/eliminarBackup.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                        <!--Confirmar Modal -->
                      <div id="confirm<?php echo $row['id'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/confirmar.php?codigo=<?php echo $row['id'];?>">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">CONFIRMAR POSTULANTE</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                                <p>Esta seguro de confirmar al postulante <strong><?php echo $row['id'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>YES</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!--DesConfirmar Modal -->
                      <div id="desconfirm<?php echo $row['id'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" action="../php/desconfirmar.php?codigo=<?php echo $row['id'];?>">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">REVERTIR CONFIRMACI�0�7N DE POSTULANTE</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                                <p>Esta seguro de revertir al postulante <strong><?php echo $row['id'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>YES</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>

                  <?php
                    }
                  ?>
                  
                  
                </tbody>
                
              </table> 

              <div class="modal fade" id="edit" >
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Editar Registro</h4>
                    </div>
                    <?php $id = $_GET['codigo'] ?>
                    <h1><?php echo $id; ?></h1>
                    <form method="POST" id="edit-form"> 
                      <div class="modal-body">
                              <input type="hidden" name="id" value="" id="id">
                              
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary" id="btn-editar">Guardar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> 
            </div>
            <div class="box-footer">
              
            </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>