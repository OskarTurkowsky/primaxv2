<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}


?>


<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          VIDEO PRIMAX
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-video-camera"></i>Video</a></li>
          
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Ver</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <center> 
                <video width="850" height="550" controls >
                  <source src="../Inducción Primax vf.mp4" type="video/mp4">
                  <source src="../Inducción Primax vf.ogg" type="video/ogg">
                  Your browser does not support the video tag.
                </video>
              </div>
            </div>
            <div class="box-footer"></div>
          </div>
          
        </div>
     

        <!-- /.row -->

      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>