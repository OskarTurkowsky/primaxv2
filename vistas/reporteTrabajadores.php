﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Reporte de Trabajadores
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-search"></i>Reporte de trabajadores</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Rango mensual</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form method="POST" action="../php/fechaTrabajadores.php">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Fecha de Alta</label>
                        <br>
                        Desde: 
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="date" class="form-control pull-right" name="fechaInicio" required>
                        </div>
                        <br>
                        Hasta: 
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                        <input type="date" class="form-control pull-right" name="fechaFin" required>
                        </div>
                      </div>
                      </div> 
                    </div>
                    <div class="col-md-4">
                        <center>
                          <button type="submit" class='btn btn-primary btn-md'>GENERAR
                        </button>
                        </center>
                    </div>   
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>