<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}

$nombrepsicologo = $_SESSION['nombres'];
?>

<?php include('header.php'); ?>
    

    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Back Up
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-address-book-o"></i> Back-Up</a></li>
          <li class="active">Buscar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de Registros</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
            <div class="box-body">
              <table id="buscar" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th class="text-center" style="min-width: 120px">OPCIONES</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center" >NOMBRES</th>
                    <th class="text-center" style="min-width: 90px">DIRECCION</th>
                    <th class="text-center">TELEFONO</th>
                    <th class="text-center">CITADO POR</th>
                    <th class="text-center">FECHA EVALUACION</th>
                    <th class="text-center">OBSERVACIONES</th>
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $idUsuario=$_SESSION['id'];
                    $admin=$_SESSION['admin'];
                    $aa=ejecutarQuery("SELECT * from usuario where idUsuario=$idUsuario");
                    $abcc=mysqli_fetch_assoc($aa);  
                    if ($admin==0){  
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup inner join usuario on usuario.idUsuario=persona.idUsuario where backup.disponibilidad!='NO' and persona.estado=0 and persona.idUsuario=$idUsuario"); 
                    }
                    else{
                      $rs=ejecutarQuery("SELECT backup.*, persona.* FROM persona  inner join backup on persona.idPersona=backup.idBackup inner join usuario on usuario.idUsuario=persona.idUsuario where backup.disponibilidad!='NO' and persona.estado=0 and usuario.idDistrito=$abcc[idDistrito]");
                    } 
                      
                
                    while($row=mysqli_fetch_assoc($rs)){
                      $pro=ejecutarQuery("SELECT distrito.nombre as dis, provincia.nombre as pro FROM distrito inner join provincia on  distrito.idProvincia=provincia.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                      $prov = mysqli_fetch_assoc($pro);
                      $co= ejecutarQuery("SELECT nombre from consultor where idConsultor='$row[idConsultor]'");
                      $cons = mysqli_fetch_assoc($co);
                      $sp= ejecutarQuery("SELECT nombre from spp inner join persona on persona.idSpp=spp.idSpp where persona.idPersona='$row[idPersona]'");
                      $spp = mysqli_fetch_assoc($sp);
                  ?>                
                      <tr>
                        <td class="text-center" <?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>>
                          <?php 
                            if ($row['policial']=='SI' and $row['sanidad']=='SI')
                            { ?> 
                              <a href="../php/seleccionarBackup.php?codigo=<?php echo $row['idPersona'];?>">
                                <button class="btn btn-success btn-circle" type="button" title="SELECCIONAR"><i class="fa fa-check"></i>
                                </button>
                              </a>
                          <?php } ?>                           
                          <a href="editarBackup.php?id=<?php echo $row['idPersona'];?>">
                            <button type='button' title="EDITAR" class='btn btn-warning btn-circle'>
                              <span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                            </button>
                          </a>
                          <a href="#delete<?php echo $row['idPersona'];?>" data-toggle="modal"><button type='button' title="ELIMINAR" class='btn btn-danger btn-circle'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>
                          </a>
                          <a href="../vistas/estaciones.php?id=<?php echo $row['idPersona'];?>">
                              <button class="btn  btn-success" type="button" title="Estaciones" ><i class="fa fa-check-circle"></i>
                              </button>
                          </a>  
                        </td>
                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                          <?php 
                            if (strlen($row['idPersona'])==7) {
                              echo '0'.$row['idPersona'];
                            }
                            else
                            {
                              echo $row['idPersona'];
                            }
                           ?>
                          </td>

                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                            <?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?>
                            </td>

                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                          <?php echo $prov['pro']." - ".$prov['dis']; ?>
                          </td>
                          
                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                          <?php echo $row['telefono']; ?></td>
                          
                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                          <?php echo $cons['nombre']; ?></td>
                          
                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                          <?php echo date("d/m/Y", strtotime($row['fechaEvaluacion'])); ?></td>
                          
                          <td class="text-center"<?php if ($row['policial']=='SI' and $row['sanidad']=='SI') 
                          { ?> style="background-color: #CFF1CA" <?php } 
                          else 
                            {?> style="background-color: white"<?php } ?>> 
                          <?php echo strtoupper($row['observacion']); ?></td>
                      </tr>

                      <div id="delete<?php echo $row['idPersona'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarBackup2.php?id=<?php echo $row['idPersona'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <p>Esta seguro de eliminar a <strong><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idPersona']; ?>,'example','../php/eliminarBackup2.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                  <?php
                  }
                  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>OPCIONES</th>
                    <th>DNI</th>
                    <th>NOMBRES</th>
                    <th>DIRECCION</th>
                    <th>TELEFONO</th>
                    <th>CITADO POR</th>
                    <th>FECHA EVALUACION</th>
                    <th>OBSERVACIONES</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="box-footer">
            </div>
          </div>
        </div>
      </div>
          
      <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>