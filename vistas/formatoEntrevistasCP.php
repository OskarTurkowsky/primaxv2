<?php
require('../fpdf.php');
require '../php/funciones.php';
@session_start();
$idRequerimiento = $_GET['idReq'];
$usuario 				 = $_SESSION['id'];

class PDF extends FPDF
{
	// Cabecera de p�gina
	function carta($fecha, $estacion, $trabajador, $cargo, $direccion, $cliente, $departamento)
	{
		//-------logo y fecha alta
		// Logo de la empresa
		$this->Image('../imagenes/logo GRIS.jpg',10,20,80);
		// Arial bold 15
		$this->SetFont('Arial','',12);
		// Movernos a la derecha
		$this->Cell(80);
		// logo derecha
		if ($cliente==2055454574) {
			$this->Image('../imagenes/logo_primax gris.jpg',140,23,50);
		}

		if ($cliente==2025903307) {
			$this->Image('../imagenes/logo_pecsa GRIS.jpg',140,23,50);
		}
		//$this->Cell(150,30,$fecha,0,0,'C');
		// Salto de l�nea
		$this->Ln(35);


		//titulo general
		// Arial bold 15
		$this->SetFont('Arial','b',22);
		// Movernos a la derecha
		$this->Cell(85);
		// FECHA DE alta
		 $this->Cell(20,30,'FORMATO DE ENVIO A ENTREVISTA',0,1,'C');
	
		// Salto de l�nea
		
		$this->Line(20, 70, 210-20, 70); // 20mm from each edge
		$this->Line(50, 70, 210-50, 70); // 50mm from each edge
	  
		$this->SetDrawColor(188,188,188);
		$this->Line(20,71,210-20,71);
		$this->Ln(20);

		// estacioon
		$this->SetFont('Arial','B',14);
		$this->Cell(9);
		$this->Cell(0,0,"Postulante:",0,1,'L');
		$this->Cell(65);
		$this->SetFont('Arial','',14);
	  $this->Cell(0,0,$trabajador,0,1,'L');
		$this->Ln(25);

		//TEXTO
		$this->Cell(9);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'Fecha:',0,1,'L');
		$this->Cell(65);
		$this->SetFont('Arial','',14);
	  $this->Cell(0,0,$fecha,0,1,'L');

		//NOMBRE DEL POSTULANTE
		$this->Ln(25);
		$this->Cell(9);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'Cargo al que postula:',0,1,'L');
		$this->Cell(65);
		$this->SetFont('Arial','',14);
	  $this->Cell(0,0,$cargo,0,1,'L');

		$this->Ln(25);
		$this->Cell(9);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'Hora:',0,1,'L');
		$this->Cell(65);
		$this->SetFont('Arial','',14);
		if ($cliente=='2055454574') {
			$this->Cell(0,0,'08:30 am',0,1,'L');
		}
		if ($cliente=='2025903307')
		{
			$this->Cell(0,0,'08:30 am',0,1,'L');
		}

		$this->Ln(25);
		$this->Cell(9);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'Lugar de entrevista:',0,1,'L');
		$this->Cell(65);
		$this->SetFont('Arial','',14);
		if ($departamento=='Lima') {
				if ($cliente=='2055454574') {
					$this->Cell(0,0,'',0,1,'L');
				}
				//PECSA
				if ($cliente=='2025903307')
				{
					$this->Cell(0,0,'',0,1,'L');
				}				
		}
		else
		{
			if ($departamento=='La libertad') {
				$this->Cell(0,0,'E/S LARCO',0,1,'L');
			}
			else
			{
				if ($departamento=='Piura') {
					$this->Cell(0,0,'E/S MEGA',0,1,'L');
				}
				else
				{
					if ($departamento=='Lambayeque') {
						$this->Cell(0,0,'E/S SANTA ELENA',0,1,'L');
					}
					else
					{
						$this->Cell(0,0,$estacion,0,1,'L');
					}
				} 
			}
		}

		$this->Ln(40);
		$this->Cell(13);
		$this->SetFont('Times','BI',14);
		$this->MultiCell(165,5,'"La satisfacci�n del cliente es nuestra garant�a por ser los aliados estrat�gicos con mejor eficiencia en el mercado laboral."',0,'C',false);
	}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	date_default_timezone_set('America/Lima');
	setlocale(LC_TIME, 'spanish');
	$sql = ejecutarQuery("SELECT distinct *, requerimiento.idCliente as idCliente from requerimiento inner join detalle_requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
		inner join asignado on asignado.idDetalle_requerimiento=detalle_requerimiento.idDetalle_requerimiento inner join persona on persona.idPersona=asignado.idPersona
		inner join distrito on persona.idDistrito=distrito.idDistrito
        inner join provincia on provincia.idProvincia=distrito.idProvincia
        inner join departamento on departamento.idDepartamento=provincia.idDepartamento
		where persona.estado IN (3,4,5,6,7,8,10,12,14,16,17,18,19,20,27,25) and requerimiento.idRequerimiento=$idRequerimiento group by persona.idPersona");

		while($fila=mysqli_fetch_assoc($sql)){ 
			$cliente=$fila['idCliente'];
			$sqqqq = ejecutarQuery("SELECT  departamento.idDepartamento as idDepartamento from usuario inner join distrito on distrito.idDistrito=usuario.idDistrito
				inner join provincia on provincia.idProvincia=distrito.idProvincia
				inner join departamento on departamento.idDepartamento=provincia.idDepartamento where usuario.idUsuario=$usuario");
			$usuarioDept=mysqli_fetch_array($sqqqq);

			$sq = ejecutarQuery("SELECT departamento.nombre as departamento, departamento.idDepartamento as idDepartamento from departamento inner join estacion on estacion.idDepartamento=departamento.idDepartamento where idEstacion=$fila[idEstacion]");
			$filaasa=mysqli_fetch_array($sq);

			//Si departamento usuario es lima (15) 
			if ( ($usuarioDept['idDepartamento']==15 && ( $filaasa['idDepartamento']==15 || $filaasa['idDepartamento']==7) ) || 
				($filaasa['idDepartamento']==11 &&  $usuarioDept['idDepartamento']==14) ||
				($filaasa['idDepartamento']==6 &&  $usuarioDept['idDepartamento']==20) ||
				 ($filaasa['idDepartamento']==2 &&  $usuarioDept['idDepartamento']==20) || 
				 ($filaasa['idDepartamento']==12 &&  $usuarioDept['idDepartamento']==14) || 
				 (($usuario == 77172595 || $usuario ==70495763 || $usuario ==70803227 || $usuario ==77037740 || $usuario ==73929809 || $usuario ==72693759 || $usuario== 15359362) && 
				 ( $fila['idDepartamento'] == 12 || $filaasa['idDepartamento']==2 || $fila['idEstacion'] == 300 ||  $fila['idEstacion'] == 208 || $fila['idEstacion'] == 39 || $fila['idEstacion'] ==  209  || $fila['idEstacion'] == 44 || $fila['idEstacion'] ==303 || $fila['idEstacion'] ==302 || $fila['idEstacion'] == 211 || $fila['idEstacion'] ==253 || $fila['idEstacion'] ==215 || $fila['idEstacion'] ==210 || $fila['idEstacion'] ==301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 183 || $fila['idEstacion'] == 301 || $fila['idEstacion'] == 254 || $fila['idEstacion'] == 216 || $fila['idEstacion'] == 256 || $fila['idEstacion'] == 148 || $fila['idEstacion'] == 146) ) ||
				 (($usuario == 45304357 || $usuario ==47251072 || $usuario ==76536187 || $usuario ==77037740 || $usuario==72425999 || $usuario ==48282874) && 
				 ($fila['idEstacion'] == 246 ||  $fila['idEstacion'] == 247 || $fila['idEstacion'] ==303 ||  $fila['idEstacion'] ==  108 || $fila['idEstacion'] ==123 || $fila['idEstacion'] == 245) ) || 
				 ($filaasa['idDepartamento']==12 && $usuario==75527358) ||
				 ($filaasa['idDepartamento']==11 && $usuario==48648498) ||
				 ($filaasa['idDepartamento']==$usuarioDept['idDepartamento']) ){

						$departamento=strtolower($filaasa['departamento']);
						if ($filaasa['idDepartamento']==15 ||$filaasa['idDepartamento']==7) {
							$departamento='Lima';
						}

						$fecha = date("d/m/Y", strtotime($fila['fechaAlta']));
						$trabajador = utf8_decode($fila['apellidoPaterno'].' '.$fila['apellidoMaterno'].', '.$fila['nombres']);

						$consulta = ejecutarQuery("SELECT nombre from cargo where idcargo=$fila[idcargo]");
						$getCargo = mysqli_fetch_assoc($consulta);
						$cargo=$getCargo['nombre'];
						
						$consulta1 = ejecutarQuery("SELECT * from estacion where idEstacion=$fila[idEstacion]");
						$getdireccion = mysqli_fetch_assoc($consulta1);

						$direccion='Direcci�n: '.utf8_decode($getdireccion['direccion']);
						$estacion=utf8_decode($getdireccion['nombre']);

						$pdf->AddPage();
						$pdf -> carta($fecha, $estacion, $trabajador, $cargo, $direccion, $cliente, $departamento);
				}			
		}

	$pdf->Output();
?>