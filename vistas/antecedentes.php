<?php
require '../php/funciones.php';
if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>


<?php 
  include('header.php'); 
?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      
      <section class="content-header">
        <h1>
          Reportes
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-file-text-o"></i> Reportes</a></li>
          <li class="active">Reporte de Antecedentes</li>
        </ol>
      </section>

      <section class="content">
        <div class="row">
          <form method="POST" action="../php/confirmarAntecedente.php" >
          <div class="col-xs-12">
            <div class="box box-default ">
              <div class="box-header with-border">
                <h3 class="box-title">Tabla de datos para Antecedentes</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>  
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <table id=soloexport class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 150px">FECHA DE SOLICITUD</th>
                        <th class="text-center" style="width: 150px">NOMBRE DE LA EMPRESA</th>
                        <th class="text-center" style="width: 200px">PERSONA SOLICITANTE</th>
                        <th class="text-center" style="width: 200px">APELLIDOS Y NOMBRES</th>
                        <th class="text-center" style="width: 120px">DNI</th>
                        <th class="text-center" style="width: 120px">DISTRITO</th>
                        <th class="text-center" style="width: 120px">CONSULTOR</th>
                        <th class="text-center" style="width: 120px">FECHA ASIGNACION</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $rs=ejecutarQuery("SELECT * FROM persona WHERE estado=6");
                      while($row=mysqli_fetch_assoc($rs)){
                        $dpt=ejecutarQuery("SELECT departamento.nombre as dep FROM departamento inner join provincia on departamento.idDepartamento=provincia.idDepartamento inner join distrito on provincia.idProvincia=distrito.idProvincia where distrito.idDistrito='$row[idDistrito]' ");
                        $dept = mysqli_fetch_assoc($dpt);
                        $us= ejecutarQuery("SELECT * from usuario where idUsuario=77037740");
                        $usar = mysqli_fetch_assoc($us);
                        $dist= ejecutarQuery("SELECT * from distrito where idDistrito=$row[idDistrito]");
                        $distrito = mysqli_fetch_assoc($dist);
                        $usss= ejecutarQuery("SELECT * from usuario where idUsuario='$row[idUsuario]'");
                        $usuaaaa = mysqli_fetch_assoc($usss);
                        $cli= ejecutarQuery("SELECT cliente.* from asignado inner join detalle_requerimiento on detalle_requerimiento.idDetalle_requerimiento=asignado.idDetalle_requerimiento
                          inner join requerimiento on requerimiento.idRequerimiento=detalle_requerimiento.idRequerimiento
                          inner join cliente on cliente.idCliente=requerimiento.idCliente where asignado.idPersona='$row[idPersona]' ");
                        $cliente = mysqli_fetch_assoc($cli);
                        date_default_timezone_set('America/Lima');
                        $hoy = date("Y-m-d H:i:s");
                      ?>
                        <tr>
                          <td class="text-center" style="width: 150px"><?php echo date("d/m/Y", strtotime($hoy)); ?></td>
                          <td class="text-center" style="width: 150px"><?php echo $cliente['nombre']; ?></td>
                          <td class="text-center" style="width: 200
                        0px"><?php echo strtoupper($usar['nombres']); ?></td>
                          <td class="text-center" style="width: 200px"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                          <td class="text-center" style="width: 120px"><?php 
                            if (strlen($row['idPersona'])==7) {
                                echo '0'.$row['idPersona'];
                              }
                              else
                              {
                                echo $row['idPersona'];
                              } 
                           ?></td>
                          <td class="text-center" style="width: 120px"><?php echo strtoupper($distrito['nombre']); ?></td>
                          <td class="text-center" style="width: 120px"><?php echo strtoupper($usuaaaa['nombres']); ?></td>
                          <td class="text-center" style="width: 150px"><?php echo date("d/m/Y H:i:s", strtotime($row['fecha_asignacion_antecedente'])); ?></td>
                        </tr>
                      <?php
                      }
                      ?>
                    </tbody>
                  </table>
                  <center>
                    <a href="../php/confirmarAntecedente.php">
                     <button type="submit" class='btn btn-primary btn-lg'>CONFIRMAR</button>
                    </a>
                  </center>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          </form>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>

    </div>
    
<?php include('footer.php'); ?>