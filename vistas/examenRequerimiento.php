<?php
require '../php/funciones.php';
if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>

<!-- CONTENIDO DE LA PAGINA -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-thumbs-up"></i>Para Examen Médico</a></li>
    </ol>
  </section><br>

  <section class="content-header">
    <div class="box">
        <div class="box-header">
            <tittle class="box-title">Para Examen Médico</tittle>
            <br><br>
            <select class="form-control" id="mostrar">
              <option value="activos">Fecha Alta</option>
              <option value="todos">Requerimientos</option>
            </select>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
      </div>
    </div>
  </section>
  <!-- Main content -->
  <section class="content" id="notall">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Fecha de Alta  &nbsp;&nbsp;&nbsp;</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
            <div class="box-body" >
              <table id="example" class="table-bordered table-hover">
                <thead>
                    <tr>
                      <th></th>
                      <th class="text-center">OPCIONES </th>
                      <th class="text-center">FECHA ALTA</th>
                      <th  class="text-center">EXTRA</th>
                      <th class="text-center">ESTADO</th>                   
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT * FROM requerimiento where estado=1 group by fechaAlta");             
                    while($row=mysqli_fetch_assoc($rs)){
                    ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center" style="min-width: 160px">
                          <a href="paraExamenMedicoLima.php?fechaAlta=<?php echo $row['fechaAlta'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">Lima y Callao</button>
                          </a>
                          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                          <a href="paraExamenMedicoOtros.php?fechaAlta=<?php echo $row['fechaAlta'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">Otras provincias</button>
                          </a>
                          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                          <a href="paraExamenMedico.php?fechaAlta=<?php echo $row['fechaAlta'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">General</button>
                          </a>
                          <!--<a href="especial.php?fechaAlta=<?php echo $row['fechaAlta']?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">Especial</button>
                          </a>
                        -->
                        </td>
                        <td class="text-center"><?php echo date("d/m/Y", strtotime($row['fechaAlta'])); ?></td>
                        <td class="text-center"><?php echo $row['extra']; ?></td>  
                        <td style="text-align: center">
                          <?php 
                            if ($row['estado']==1){ echo '<a class="btn btn-sm  btn-success ">ACTIVO </a>';}
                            if ($row['estado']==2){ echo '<a class="btn btn-sm  btn-primary ">CULMINADO </a>';}
                          ?>
                        </td>  
                      </tr>
                    <?php
                      }
                    ?>
                </tbody>
                </table>            
            </div>

              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
  </section>
  
  <section class="content-header" id="all" hidden>
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Requerimientos  &nbsp;&nbsp;&nbsp;</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
            <div class="box-body" >
              <table id="solotable" class="table-bordered table-hover">
                <thead>
                    <tr>
                      <th></th>
                      <th class="text-center"   style="min-width: 350px">OPCIONES </th>
                      <th class="text-center">FECHA ALTA</th>
                      <th class="text-center">FECHA EXAMEN</th>
                      <th class="text-center">CLIENTE</th>
                      <th class="text-center">EXTRA</th>
                      <th class="text-center">ESTADO</th>                   
                    </tr>
                  </thead>
                <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT * FROM requerimiento where estado=1");             
                    while($row=mysqli_fetch_assoc($rs)){
                      $consulta = ejecutarQuery("SELECT * FROM cliente WHERE idCliente=$row[idCliente]");
                      $aa = mysqli_fetch_assoc($consulta);
                    ?>                  
                      <tr bgcolor="white">
                        <td></td>
                        <td class="text-center" style="min-width: 160px">
                          <a href="paraExamenMedicoLimaR.php?idRequerimiento=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">Lima y Callao</button>
                          </a>
                          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                          <a href="paraExamenMedicoOtrosR.php?idRequerimiento=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">Otras provincias</button>
                          </a>
                          &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                          <a href="paraExamenMedicoR.php?idRequerimiento=<?php echo $row['idRequerimiento'];?>">
                              <button class="btn btn-secondary btn-circle" type="button" title="REPORTE">General</button>
                          </a>
                        </td>
                        <td class="text-center"><?php echo date("d/m/Y", strtotime($row['fechaAlta'])); ?></td>
                        <td class="text-center"><?php echo date("d/m/Y", strtotime($row['fechaExamen'])); ?></td>
                        <td class="text-center"> <?php echo $aa['nombre']; 
                        ?></td>
                        <td class="text-center"><?php echo $row['extra']; ?></td>  
                        <td style="text-align: center">
                          <?php 
                            if ($row['estado']==1){ echo '<a class="btn btn-sm  btn-success ">ACTIVO </a>';}
                            if ($row['estado']==2){ echo '<a class="btn btn-sm  btn-primary ">CULMINADO </a>';}
                          ?>
                        </td>  
                      </tr>
                    <?php
                      }
                    ?>
                </tbody>
                </table>            
            </div>

              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
  </section>
  

</div>
<!-- FIN DEL CONTENIDO DE LA PAGINA-->


<?php include('footer.php'); ?>