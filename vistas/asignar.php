<?php
require '../php/funciones.php';
if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
$dni = $_GET['id'] ;
?>

<?php include('header.php'); ?>


<!-- CONTENIDO DE LA PAGINA -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-thumbs-up"></i> ASIGNAR REQUERMIENTO</a></li>
      <li class="active">ESCOGER</li>
    </ol>
  </section><br>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">DATOS DEL POSTULANTE</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>  
          </div>
          <div class="box-body">
            <form method="POST" action="../php/enviar.php" enctype="multipart/form-data">
              <table id="example" class="table-bordered table-hover">
                <thead>
                  <tr>
                    <th></th>
                    <th class="text-center" style="min-width: 60px">DNI</th>
                    <th class="text-center" style="min-width: 180px">NOMBRES</th>
                    <th class="text-center">TEL&Eacute;FONOS</th>
                    <th class="text-center">FECHA NACIMIENTO</th>
                    <th class="text-center">SEXO</th>
                    <th class="text-center">ESTADO CIVIL</th>
                    <th class="text-center">DEPARTAMENTO</th>

                    <th class="text-center">PROVINCIA<font color="white">-------------</font></th>
                    <th>DISTRITO <font color="white">--------------</font></th>
                    <th>DIRECCIÓN <font color="white">------------</font></th>
                    <th>TALLA BOTAS<font color="white">----------</font></th>
                    <th>TALLA UNIFORME <font color="white">----</font></th>
                    <th>SPP <font color="white">---------------------</font></th>
                    <th>FECHA SPP  <font color="white">------------</font></th>
                    <th>CODIGO SPP  <font color="white">----------</font></th>
                    <th>OBSERVACIONES <font color="white">----</font></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $rs=ejecutarQuery("SELECT * FROM persona where idPersona = $dni");
                  $row=mysqli_fetch_assoc($rs);
                  $pro=ejecutarQuery("
                    SELECT 
                      distrito.nombre as dis, provincia.nombre as pro, departamento.idDepartamento as iddep, departamento.nombre as depnombre, distrito.idDistrito as idDistrito 
                    FROM 
                      distrito 
                      inner join provincia on distrito.idProvincia=provincia.idProvincia
                      inner join departamento on provincia.idDepartamento = departamento.idDepartamento
                    WHERE 
                      distrito.idDistrito='$row[idDistrito]' 
                  ");
                  $prov = mysqli_fetch_assoc($pro);
                  $us=ejecutarQuery("SELECT * FROM usuario where idUsuario='$row[idUsuario]'"); 
                  $usu = mysqli_fetch_assoc($us);
                  $gen= ejecutarQuery("SELECT nombre, idGenero from genero where idGenero='$row[idGenero]'");
                  $gener = mysqli_fetch_assoc($gen);
                  $sp= ejecutarQuery("SELECT nombre from spp where idSpp='$row[idSpp]'");
                  $spp = mysqli_fetch_assoc($sp);
                  ?>
                  <tr>
                    <td></td>
                    <td class="text-center"><?php 
                      if (strlen($row['idPersona'])==7) {
                            echo '0'.$row['idPersona'];
                          }
                          else
                          {
                            echo $row['idPersona'];
                          }
                     ?></td>
                    <td class="text-center"><?php echo $row['apellidoPaterno']." ".$row['apellidoMaterno']." ".$row['nombres']; ?></td>
                    <td class="text-center"><?php echo $row['telefono']; ?></td>
                    <td class="text-center">
                      <?php 
                      if(date("d/m/Y", strtotime($row['fechaNacimiento']))=='31/12/1969'){ echo 'NO REGISTRA'; } 
                      else  echo  date("d/m/Y", strtotime($row['fechaNacimiento'])); 
                      ?>                        
                      </td>
                    <td class="text-center"><?php echo $gener['nombre']; ?></td>
                    <td class="text-center"><?php echo $row['estadoCivil']; ?></td>
                    <td class="text-center"><?php echo $prov['depnombre']; ?></td>
                    <td class="text-center"><?php echo $prov['pro']; ?></td>
                    <td><?php echo $prov['dis']; ?></td>
                    <td><?php echo strtoupper($row['direccion']); ?></td>
                    <td><?php echo $row['tallaBotas']; ?></td>
                    <td><?php echo $row['tallaUniforme']; ?></td>  
                    <td><?php echo $spp['nombre']; ?></td>
                    <td>
                      <?php if(date("d/m/Y", strtotime($row['fechaSpp']))=='30/11/-0001') {echo 'NO REGISTRA'; } else  echo  date("d/m/Y", strtotime($row['fechaSpp'])); ?>
                    </td>
                    <td><?php echo $row['codigoSpp']; ?></td>
                    <td><?php echo strtoupper($row['observacion']); ?></td>
                  </tr>
                  
                </tbody>
              </table>
            </form>
          </div>
          <div class="box-footer">
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="content-header">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">ASIGNAR REQUERIMIENTO  </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div> 
          </div>  
          <div class="box-body">
            <table id="solotable" class="table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="text-center">OPCIONES</th>
                    <th class="text-center">CLIENTE</th>
                    <th class="text-center">FECHA ALTA</th>
                    <th class="text-center">FECHA EXAMEN</th>
                    <th class="text-center">CARGO</th>
                    <th class="text-center">ESTACION</th>
                    <th class="text-center">CLINICA</th>
                    <th class="text-center">SEXO</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $distrito = $prov['idDistrito'];
                  $departamento = $prov['iddep'];
                  $gg = $gener['idGenero'];
                  $per= $row['idPersona'];

                  //lima callao
                  if ($departamento==15 || $departamento==7 ) {
                    $consulta = ejecutarQuery("
                    SELECT distinct detalle_requerimiento.*, requerimiento.fechaAlta, requerimiento.fechaExamen, requerimiento.idCliente as idCliente, estacion.nombre as estacion, cargo.nombre as cargo,  genero.nombre as genero, detalle_requerimiento.idDetalle_requerimiento as detReq, requerimiento.idRequerimiento as idReq FROM detalle_requerimiento 
                      inner join requerimiento  on detalle_requerimiento.idRequerimiento = requerimiento.idRequerimiento 
                      inner join estacion on detalle_requerimiento.idEstacion = estacion.idEstacion
                      inner join estacion_distrito on estacion_distrito.idEstacion=estacion.idEstacion
                      inner join cargo on detalle_requerimiento.idCargo = cargo.idCargo
                      inner join genero  on detalle_requerimiento.idGenero = genero.idGenero
                    WHERE detalle_requerimiento.estado = 0 and estacion_distrito.idDistrito = $distrito and detalle_requerimiento.idGenero IN(3,$gg);
                    ");
                  }
                  else
                  {
                    $consulta = ejecutarQuery("
                    SELECT distinct detalle_requerimiento.*, requerimiento.fechaAlta, requerimiento.fechaExamen, estacion.nombre as estacion, cargo.nombre as cargo, requerimiento.idCliente as idCliente, genero.nombre as genero, detalle_requerimiento.idDetalle_requerimiento as detReq, requerimiento.idRequerimiento as idReq,detalle_requerimiento.idClinica as idClinica  FROM detalle_requerimiento 
                      inner join requerimiento  on detalle_requerimiento.idRequerimiento = requerimiento.idRequerimiento 
                      inner join estacion on detalle_requerimiento.idEstacion = estacion.idEstacion
                      inner join cargo on detalle_requerimiento.idCargo = cargo.idCargo
                      inner join genero  on detalle_requerimiento.idGenero = genero.idGenero
                    WHERE detalle_requerimiento.estado = 0 and estacion.idDepartamento = $departamento and detalle_requerimiento.idGenero IN(3,$gg);
                    ");
                  }                  
                  
                  while($eee=mysqli_fetch_assoc($consulta) ) {
                    $consultaa3 = ejecutarQuery("SELECT nombre FROM clinica where idClinica=$eee[idClinica]");
                    if ($eee['idClinica']!=null) {
                      $aaa = mysqli_fetch_assoc($consultaa3);
                    }

                    $consulta2 = ejecutarQuery("SELECT count(*) as contador FROM asignado where idPersona=$per");
                    $result = mysqli_fetch_assoc($consulta2);
                    $auto = $result['contador'];
                    if ($auto==0 || $auto==1) {
                  ?>
                  <tr>
                    <td class="text-center">
                      <!--<a href="#asignar<?php echo $row['idPersona']; echo $eee['detReq'];echo $eee['idReq']?>" data-toggle="modal">
                        <button class="btn btn-success btn-sm" type="button" title="ASIGNAR" >
                          <i class="fa fa-check"></i>
                        </button>
                      </a>-->
                      <button class="btn btn-success btn-sm" id="<?php  echo $eee['detReq'];?>" type="button" title="ASIGNAR" >
                        <i class="fa fa-check"></i>
                      </button>
                    </td>
                    <td class="text-center"><?php 
                      if ($eee['idCliente']==2055454574) {
                        echo "PRIMAX";
                      }
                      if ($eee['idCliente']==2025903307) {
                        echo "PECSA";
                      }
                     ?></td>
                    <td class="text-center"><?php echo date("d/m/Y", strtotime($eee['fechaAlta']))?></td>
                    <td class="text-center"> 
                      <?php if(date("d/m/Y", strtotime($eee['fechaExamen'])) == '30/11/-0001') {echo '-'; } else  echo  date("d/m/Y", strtotime($eee['fechaExamen'])); ?>
                    </td>
                    <td class="text-center"><?php echo $eee['cargo']; ?></td>
                    <td class="text-center"><?php echo $eee['estacion']; ?></td>
                    <td class="text-center"><?php if ($eee['idClinica']==null) {
                      echo "-";
                    } 
                    else
                      {echo $aaa['nombre'];} ?></td>
                    <td class="text-center"><?php echo $eee['genero']; ?></td>
                  </tr>
                  
                  <span id="error_message" class="text-danger"></span>  
                  <?php 
                  } }
                  ?>
                </tbody>
            </table>  
            <div class="alert alert-success" id="success_message" hidden role="alert">
              El postulante ha sido asignado correctamente.
            </div>        
          </div>
          <div class="box-footer">
          </div>
        </div>
      </div>
    </div>
  </section>
  

</div>
<!-- FIN DEL CONTENIDO DE LA PAGINA-->


<?php include('footer.php'); ?>

 <script>  
 
 $(document).ready(function(){  
  var detalle_requerimiento;
  var idPersona=<?php echo $dni?>;
      $("button").click(function() {
          detalle_requerimiento=this.id;
          if(idPersona == '' || detalle_requerimiento == '')  
           {  
              $('#error_message').html("Error! La asignacion no se ha realizado, intentar de nuevo.");  
           }  
           else  
           {  
                $.ajax({  
                     url:"../php/asignarRequerimiento.php",  
                     method:"POST",  
                     data:{detalle_requerimiento:detalle_requerimiento, idPersona:idPersona},  
                     success:function(data){  
                          $('#success_message').fadeIn();  
                          setTimeout(function(){  
                               $('#success_message').fadeOut("Slow"); 
                          }, 2000);
                          setTimeout(function(){ window.location = 'postulantes.php'; }, 2500);

                     }  
                });
                
           }
      });
 });  
 
 </script>  