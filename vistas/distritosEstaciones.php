﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
  
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    
      <section class="content-header">
        <h1>
          Requerimientos
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-building"></i>Estaciones</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Distritos aledaños</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <form method="POST" action="../php/nuevoEstacionDistrito.php" name="form1">
                  <div class="row">                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Departamento</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                          </div>
                          <select class="form-control pull-right" name="departamento" id="departamento"  onchange="from(document.form1.departamento.value,'midiv','../php/provincias.php');">
                            <option>Elige una opción</option>
                            <?php 
                            $consulta = ejecutarQuery("SELECT * FROM departamento where idDepartamento IN (7,15)");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDepartamento']; ?>"><?php echo $eee['nombre']; ?></OPTION>    
                            <?php
                            }
                            ?> 
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label>Provincia</label>
                        <div class="input-group" id="midiv">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="provincia" id="provincias">
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Distrito</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select class="form-control pull-right" name="distrito" id="distritos">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Estacion</label>
                        <div class="input-group" >
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <input style="width:270px; height:30px" list="listado" id="estaciones" > 
                        </div>
                        <div class="input-group" id="midiv3">                          
                          <datalist id="listado">
                          </datalist>
                          <input type="hidden"  name="estacion" id="estaciones-hidden" >
                        </div>
                      </div> 
                    </div>                 
                    <div class="col-md-4">
                      <center>
                        <br>
                        <br>
                        <button type="submit" class='btn btn-primary btn-md'>AÑADIR
                        </button>
                      </center>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </section>
    </div>
    
<?php include('footer.php'); ?>