﻿<?php
require '../php/funciones.php';

if(! haIniciadoSesion() )
{
 header('Location: ../index.php');
}
?>

<?php include('header.php'); ?>
    <!-- CONTENIDO DE LA PAGINA -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Listar Reclutadores
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-user-plus"></i> Reclutadores</a></li>
          <li class="active">Configuración</li>
        </ol>
      </section>
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default collapsed-box">
              <div class="box-header">
                <h3 class="box-title">Formulario de Registro</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <form class="form-signin" autocomplete="off" action="../php/agregarReclutador.php" method="POST" name="form1" enctype="multipart/form-data">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>DNI</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-newspaper-o"></i>
                          </div>
                          <input type="number" class="form-control pull-right" name="dni" required="">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Nombres</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-male"></i>
                          </div>
                          <input type="text" class="form-control pull-right" style="text-transform:uppercase;" name="nombres" required="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Distrito</label>
                        <div class="input-group" id="midiv2">
                          <div class="input-group-addon">
                            <i class="fa fa-globe"></i>
                          </div>
                          <select name="distrito" class="form-control pull-right">
                            <?php 
                            $consulta = ejecutarQuery("SELECT idDistrito, nombre from distrito where nombre IN ('LIMA', 'TRUJILLO', 'CHICLAYO', 'PIURA')");
                            while($eee=mysqli_fetch_assoc($consulta)){
                            ?>
                              <OPTION VALUE="<?php echo $eee['idDistrito']; ?>"><?php echo $eee['nombre']; ?></OPTION>
                            <?php
                            }
                            ?>
                            <OPTION VALUE="0">NO REGISTRA</OPTION> 
                          </select>
                        </div>
                      </div> 
                      <center> <button type="submit" class="btn btn-primary pull-center">REGISTRAR</button> </center>    
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
              <div class="box box-default ">
                <div class="box-header with-border">
                  <h3 class="box-title">Tabla de Reclutadores</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>  
                </div>
              <div class="box-body">
                <table id="example" class="table-bordered table-hover">
                  <thead>
                    <tr>
                      <th ></th>
                      <th class="text-center">OPCIONES</th>
                      <th class="text-center">DNI</th>
                      <th class="text-center">NOMBRES</th>
                      <th class="text-center">DISTRITO</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                      $rs=ejecutarQuery("SELECT * from consultor where estado=1 order by nombre");   
                      while($row=mysqli_fetch_assoc($rs)){
                        $pro=ejecutarQuery("SELECT distrito.nombre as dis, distrito.idDistrito as idDistrito from distrito WHERE distrito.idDistrito = '$row[idDistrito]' ");
                        $prov = mysqli_fetch_assoc($pro);
                    ?>                  
                        <tr bgcolor="white">
                          <td></td>
                          <td class="text-center"> 
                            <a href="editarPostulantes.php?id=<?php echo $row['idPersona'];?>">
                              <button title="EDITAR" type='button' class='btn btn-warning btn-circle'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
                              </button>
                            </a>
                            <a href="#delete<?php echo $row['idConsultor'];?>" data-toggle="modal">
                              <button type='button'title="ELIMINAR" class='btn btn-danger btn-circle'>
                                <span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
                              </button>
                            </a>                             
                          </td>
                          <td class="text-center"><?php echo $row['idConsultor']; ?></td>
                          <td class="text-center"><?php echo $row['nombre']; ?></td>
                          <td class="text-center"><?php 
                          if ($prov['idDistrito']==0) {
                            echo "NO REGISTRA";
                          }
                          else
                          {echo $prov['dis'];}
                           ?> </td>
                        </tr>

                      <div id="delete<?php echo $row['idConsultor'];?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <form method="post" id="form2" action="../php/eliminarConsultor.php?id=<?php echo $row['idConsultor'];?>" >
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">ELIMINAR REGISTRO</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                                <p>Esta seguro de eliminar a: <strong><?php echo $row['nombre'];?>?</strong></p>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" name="btnEliminar" class="btn btn-danger" onclick="from(<?php echo $row['idConsultor']; ?>,'example','../php/eliminarConsultor.php');"> <span class="glyphicon glyphicon-trash"></span>SI</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                         
                    <?php
                      }
                    ?>                  
                  </tbody>                
                </table> 
              </div>
            </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
      

    </div>
    <!-- FIN DEL CONTENIDO DE LA PAGINA-->
    
<?php include('footer.php'); ?>