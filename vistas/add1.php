<?php 
  @session_start();
  require_once 'modules/config.php';
?>

<style>
.vl {
  left: 50%;
  border-left: 1px solid #e5e5e5;
  height: 255px;
}
</style>
<?php include 'header.html'; ?>
<style type="text/css">
  .alert{
    display:none;
  }
</style>

        <!-- Begin Page Content -->
<div class="container-fluid">

          <!-- Page Heading -->
  <h1 class="h3 mb-4 text-gray-800">Registrar Datos Partida</h1>

    <form class="form-signin"  id="submit_form">
            
          <div class="row">
                          
            <div class="col-lg-12">

              <!-- Datos persona -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Datos Personales del Bautizado</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Nombres</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" autocomplete="off" id="nombre_persona" name="nombre_persona" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Pais</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="pais_persona" id="paises">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM pais");
                              $queryResult->execute();
                              while($pais = $queryResult->fetch(PDO::FETCH_ASSOC)){
                            ?>
                          <option value="<?php echo $pais['idPais']?>" <?php if ($pais['idPais']==173): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($pais['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Distrito</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="distrito_persona" id="distritos" required>
                            <option>Seleccione una opción</option>
                          </select>
                        </div>
                      </div>                        
                    </div>
                    <div class="col-md-6 col-lg-4">   
                      <div class="form-group">
                        <label class="medium font-weight-bold">Apellidos</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="apellidos_persona" id="apellidos_persona"  required>
                        </div>
                      </div>
                      <div class="form-group" id="departamentos_hide">
                        <label class="medium font-weight-bold">Departamento</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="departamento_persona" id="departamentos">
                            <?php 
                            $queryResult = $pdo->prepare("SELECT * FROM departamento where idPais=173");
                              $queryResult->execute();
                              while($departamento = $queryResult->fetch(PDO::FETCH_ASSOC)){
                            ?>
                              <option value="<?php echo $departamento['idDepartamento']?>">
                                <?php echo $departamento['nombre']?>
                              </option>
                            <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>                  
                    </div>
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Fecha Nacimiento</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-calendar-alt"></i></span>
                          </div>
                          <input type="date" class="form-control" aria-describedby="basic-addon1" name="fecha_nacimiento_persona" id="fecha_nacimiento_persona" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Provincia</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="provincia_persona" id="provincias" required>
                            <option>Seleccione una opción</option>
                          </select>
                        </div>
                      </div>                 
                    </div>
                  </div>
                </div>
              </div>

              <!-- Datos bautizo -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Datos de Bautizo</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Fecha Bautizo</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-calendar-alt"></i></span>
                          </div>
                          <input type="date" class="form-control" aria-describedby="basic-addon1" name="fecha_bautizo" id="fecha_bautizo" required>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="medium font-weight-bold">Folio</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-book-open"></i></span>
                          </div>
                          <input type="number" class="form-control" aria-describedby="basic-addon1" name="folio" id="folio" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Parroquia</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-church"></i></span>
                          </div>
                          <select class="form-control" id="parroquia" name="parroquia">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM parroquia");
                              $queryResult->execute();
                              while($parroquia = $queryResult->fetch(PDO::FETCH_ASSOC)){
                                  echo $parroquia['idParroquia'];
                            ?>
                          <option value="<?php echo $parroquia['idParroquia']?>">
                            <?php echo utf8_encode($parroquia['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>                       
                    </div>
                    <div class="col-md-6 col-lg-4">   
                      <div class="form-group">
                        <label class="medium font-weight-bold">Sacerdote</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-male"></i></span>
                          </div>
                          <select class="form-control" id="id_sacerdote" name="id_sacerdote">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM sacerdote");
                              $queryResult->execute();
                              while($sacerdote = $queryResult->fetch(PDO::FETCH_ASSOC)){
                                  echo $sacerdote['idSacerdote'];
                            ?>
                          <option value="<?php echo $sacerdote['idSacerdote']?>">
                            <?php echo utf8_encode($sacerdote['nombres']." ".$sacerdote['apellidos'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">N° Margen</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-bookmark"></i></span>
                          </div>
                          <input type="number" class="form-control" aria-describedby="basic-addon1" name="margen" id="margen" required>
                        </div>
                      </div>                        
                    </div>
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Libro</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-book"></i></span>
                          </div>
                          <input type="number" class="form-control" aria-describedby="basic-addon1" name="libro" id="libro" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Nota marginal</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-info-circle"></i></span>
                          </div>
                          <select class="form-control" id="nota_marginal" name="nota_marginal">
                            <option>No</option>
                            <option>Si</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert-hide">
                <strong>Error! </strong>El número de margen ya se encuentra registrado en el Libro bautismal.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              
              <!-- Datos Padres -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Datos Padres</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Nombres</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="nombre_padre1" id="nombre_padre1" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Pais</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="pais_padre1"  id="paises_padre1">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM pais");
                              $queryResult->execute();
                              while($pais = $queryResult->fetch(PDO::FETCH_ASSOC)){
                            ?>
                          <option value="<?php echo $pais['idPais']?>" <?php if ($pais['idPais']==173): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($pais['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="medium font-weight-bold">Distrito</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="distrito_padre1" id="distritos1" required>
                            <option>Seleccione una opción</option>
                          </select>
                        </div>
                      </div>                        
                    </div>
                    <div class="col-md-6 col-lg-4">   
                      <div class="form-group">
                        <label class="medium font-weight-bold">Apellidos</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="apellidos_padre1" id="apellidos_padre1" required>
                        </div>
                      </div>
                      <div class="form-group" id="departamentos_hide1">
                        <label class="medium font-weight-bold">Departamento</label>
                        <div class="input-group mb-3" id="midiv">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="departamento_padre1" id="departamentos1">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM departamento where idPais=173");
                              $queryResult->execute();
                              while($departamento = $queryResult->fetch(PDO::FETCH_ASSOC)){
                            ?>
                          <option value="<?php echo $departamento['idDepartamento']?>" <?php if ($departamento['idDepartamento']==13): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($departamento['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>          
                    </div>
                    <div class="col-md-6 col-lg-4">                      
                      <div class="form-group">
                        <label class="medium font-weight-bold">Genero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-male"></i></span>
                          </div>
                          <select class="form-control"  id="genero_padre1" name="genero_padre1">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM genero");
                              $queryResult->execute();
                              while($genero = $queryResult->fetch(PDO::FETCH_ASSOC)){
                                  echo $genero['idGenero'];
                            ?>
                          <option value="<?php echo $genero['idGenero']?>">
                            <?php echo utf8_encode($genero['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Provincia</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="provincia_padre1" id="provincias1" required>
                            <option>Seleccione una opción</option>
                          </select>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <hr class="sidebar-divider">
                  <div class="row">
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Nombres</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="nombre_padre2" id="nombre_padre2">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Pais</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="pais_padre2"  id="paises_padre2">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM pais");
                              $queryResult->execute();
                              while($pais = $queryResult->fetch(PDO::FETCH_ASSOC)){
                            ?>
                          <option value="<?php echo $pais['idPais']?>" <?php if ($pais['idPais']==173): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($pais['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="medium font-weight-bold">Distrito</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="distrito_padre2" id="distritos2">
                          </select>
                        </div>
                      </div>                        
                    </div>
                    <div class="col-md-6 col-lg-4">   
                      <div class="form-group">
                        <label class="medium font-weight-bold">Apellidos</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="apellidos_padre2" id="apellidos_padre2">
                        </div>
                      </div> 
                      <div class="form-group" id="departamentos_hide2">
                        <label class="medium font-weight-bold">Departamento</label>
                        <div class="input-group mb-3" id="midiv">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="departamento_padre2" id="departamentos2">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM departamento where idPais=173");
                              $queryResult->execute();
                              while($departamento = $queryResult->fetch(PDO::FETCH_ASSOC)){
                            ?>
                          <option value="<?php echo $departamento['idDepartamento']?>" <?php if ($departamento['idDepartamento']==13): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($departamento['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>          
                    </div>
                    <div class="col-md-6 col-lg-4">
                      
                      <div class="form-group">
                        <label class="medium font-weight-bold">Genero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-female"></i></span>
                          </div>
                          <select class="form-control" name="genero_padre2" id="genero_padre2">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM genero");
                              $queryResult->execute();
                              while($genero = $queryResult->fetch(PDO::FETCH_ASSOC)){
                                  echo $genero['idGenero'];
                            ?>
                          <option value="<?php echo $genero['idGenero']?>" <?php if ($genero['idGenero']==2): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($genero['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                        <div class="form-group">
                        <label class="medium font-weight-bold">Provincia</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe-americas"></i></span>
                          </div>
                          <select class="form-control" name="provincia_padre2" id="provincias2">
                          </select>
                        </div>
                      </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>

              <!-- Datos Padrinos -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Datos Padrinos</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Nombres</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="nombre_padrino1" id="nombre_padrino1" required>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="medium font-weight-bold">Apellidos</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="apellidos_padrino1" id="apellidos_padrino1" required>
                        </div>
                      </div>     
                      <div class="form-group">
                        <label class="medium font-weight-bold">Genero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-male"></i></span>
                          </div>
                          <select class="form-control" id="genero_padrino1" name="genero_padrino1">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM genero");
                              $queryResult->execute();
                              while($genero = $queryResult->fetch(PDO::FETCH_ASSOC)){
                                  echo $genero['idGenero'];
                            ?>
                          <option value="<?php echo $genero['idGenero']?>">
                            <?php echo utf8_encode($genero['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>      
                    </div>
                    <div class="vl"></div>
                    <div class="col-md-6 col-lg-4">
                      <div class="form-group">
                        <label class="medium font-weight-bold">Nombres</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="nombre_padrino2" id="nombre_padrino2">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="medium font-weight-bold">Apellidos</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                          </div>
                          <input type="text" class="form-control" aria-describedby="basic-addon1" name="apellidos_padrino2" id="apellidos_padrino2">
                        </div>
                      </div>     
                      <div class="form-group">
                        <label class="medium font-weight-bold">Genero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-female"></i></span>
                          </div>
                          <select class="form-control" id="genero_padrino2" name="genero_padrino2">
                            <?php
                              $queryResult = $pdo->prepare("SELECT * FROM genero");
                              $queryResult->execute();
                              while($genero = $queryResult->fetch(PDO::FETCH_ASSOC)){
                                  echo $genero['idGenero'];
                            ?>
                          <option value="<?php echo $genero['idGenero']?>" <?php if ($genero['idGenero']==2): ?>
                            selected="selected"
                          <?php endif ?>>
                            <?php echo utf8_encode($genero['nombre'])?>
                          </option>
                          <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>                  
                    </div>
                  </div>  
                </div>
              </div>

              <div class="alert alert-success" role="alert" id="success_message">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Completado!</strong> El bautizo ha sido registrado correctamente.
              </div>

              <div class="alert alert-danger alert-dismissible fade show" role="alert" id="error_message">
                <strong>Error! </strong>El bautizo no se ha guardado correctamente, por favor verificar los datos.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="card-action" align="right">
                  <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  
                  <button class="btn btn-danger">Cancelar</button>
              </div>

            </div>
          
          </div>

    </form>

</div>
        <!-- end of page content-->

<?php include 'footer.html'; ?>    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

      $('#submit').click(function(){  
           var nombre_persona = $('#nombre_persona').val();  
           var apellidos_persona = $('#apellidos_persona').val();  
           var fecha_nacimiento_persona = $('#fecha_nacimiento_persona').val();
           var fecha_bautizo = $('#fecha_bautizo').val(); 
           var distritos = $('#distritos').val(); 
           var folio = $('#folio').val(); 
           var parroquia = $('#parroquia').val(); 
           var id_sacerdote = $('#id_sacerdote').val(); 
           var margen = $('#margen').val();  
           var libro = $('#libro').val();  
           var nota_marginal = $('#nota_marginal').val();  
           var nombre_padre1 = $('#nombre_padre1').val();  
           var distritos1 = $('#distritos1').val();  
           var apellidos_padre1 = $('#apellidos_padre1').val();  
           var genero_padre1 = $('#genero_padre1').val();  
           var nombre_padre2 = $('#nombre_padre2').val();
           var distritos2 = $('#distritos2').val();
           var apellidos_padre2 = $('#apellidos_padre2').val();
           var genero_padre2 = $('#genero_padre2').val();
           var nombre_padrino1 = $('#nombre_padrino1').val(); 
           var apellidos_padrino1 = $('#apellidos_padrino1').val();
           var genero_padrino1 = $('#genero_padrino1').val();
           var nombre_padrino2 = $('#nombre_padrino2').val(); 
           var apellidos_padrino2 = $('#apellidos_padrino2').val(); 
           var genero_padrino2 = $('#genero_padrino2').val(); 
           if(nombre_persona == '' || apellidos_persona == '')  
           {  
                $('#error_message').fadeIn();  
                  setTimeout(function(){  
                  $('#error_message').fadeOut("Slow");  
                }, 2000);   
           }  
           else  
           {  
                $('#error_message').fadeIn();  
                  setTimeout(function(){  
                  $('#error_message').fadeOut("Slow");  
                }, 2000);   
                $.ajax({  
                     url:"save.php",  
                     method:"POST",  
                     data:{nombre_persona:nombre_persona, apellidos_persona:apellidos_persona, fecha_nacimiento_persona:fecha_nacimiento_persona,fecha_bautizo:fecha_bautizo,  distrito_persona:distritos, folio:folio, parroquia:parroquia, id_sacerdote:id_sacerdote, margen:margen, libro:libro, nota_marginal:nota_marginal, nombre_padre1:nombre_padre1, distrito_padre1:distritos1, apellidos_padre1:apellidos_padre1, genero_padre1:genero_padre1, nombre_padre2: nombre_padre2, distrito_padre2:distritos2, apellidos_padre2:apellidos_padre2, genero_padre2:genero_padre2, nombre_padrino1:nombre_padrino1, apellidos_padrino1:apellidos_padrino1, genero_padrino1:genero_padrino1, nombre_padrino2:nombre_padrino2, apellidos_padrino2:apellidos_padrino2, genero_padrino2:genero_padrino2},  
                     success:function(data){  
                      console.log(data);
                          $("form").trigger("reset");
                          //$("#success_message").fadeTo(2000, 500).slideUp(500, function(){
                            //  $("#success_message").alert('close');
                          //});  
                          $('#success_message').fadeIn();  
                          setTimeout(function(){  
                               $('#success_message').fadeOut("Slow");  
                          }, 2000);  
                     }  
                });  
           }  
      });  

      var from_date = $('#from_date').val();  
      var turno = $('#turno').val(); 
      var consulta;
      $("#margen").keyup(function(e){
          consulta = $("#margen").val();
          var libro = $('#libro').val();  
          var folio = $('#folio').val(); 
          if(libro != '' && folio != '' && consulta != '')  
          {  
            $.ajax({  
                  url:"validatePage.php",  
                  method:"POST",  
                  data:{libro:libro, folio:folio, margen:consulta},  
                  success:function(data)  
                  {  
                    if (data.trim()=='SI') {
                      $("#alert-hide").hide().show('medium');
                    }
                    else{
                      $("#alert-hide").hide();
                    }
                  }  
            });  
          }        
      });

      $("#paises").change(function(){
        var consulta = $("#paises").val();
        if (consulta!=173) {
          $("#departamentos_hide").hide();
        }
        else
        {
          $("#departamentos_hide").show();
        }
      });

      $("#paises_padre1").change(function(){
        var consulta = $("#paises_padre1").val();
        if (consulta!=173) {
          $("#departamentos_hide1").hide();
        }
        else
        {
          $("#departamentos_hide1").show();
        }
      });

      $("#paises_padre2").change(function(){
        var consulta = $("#paises_padre2").val();
        if (consulta!=173) {
          $("#departamentos_hide2").hide();
        }
        else
        {
          $("#departamentos_hide2").show();
        }
      });

      $("#departamentos").change(function(){
        var departamento = $("#departamentos").val();
        $.ajax({
                      async: true,
                      type: "POST",
                      url: "provincias.php",
                      data: "search="+departamento,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        $("#provincias").html(data);
                      }
              });  
      });

      $("#departamentos1").change(function(){
        var departamento = $("#departamentos1").val();
        $.ajax({
                      async: true,
                      type: "POST",
                      url: "provincias.php",
                      data: "search="+departamento,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        $("#provincias1").html(data);
                      }
              });  
      });

      $("#departamentos2").change(function(){
        var departamento = $("#departamentos2").val();
        $.ajax({
                      async: true,
                      type: "POST",
                      url: "provincias.php",
                      data: "search="+departamento,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        $("#provincias2").html(data);
                      }
              });  
      });

      $("#provincias").click(function(){
        var provincias = $("#provincias").val();
        $.ajax({
                      async: true,
                      type: "POST",
                      url: "distritos.php",
                      data: "search="+provincias,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        $("#distritos").html(data);
                      }
              });  
      });

      $("#provincias1").click(function(){
        var provincias = $("#provincias1").val();
        $.ajax({
                      async: true,
                      type: "POST",
                      url: "distritos.php",
                      data: "search="+provincias,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        $("#distritos1").html(data);
                      }
              });  
      });

      $("#provincias2").click(function(){
        var provincias = $("#provincias2").val();
        $.ajax({
                      async: true,
                      type: "POST",
                      url: "distritos.php",
                      data: "search="+provincias,
                      dataType: "html",
                      error: function(){
                        alert("error petición ajax");
                      },
                      success: function(data){
                        $("#distritos2").html(data);
                      }
              });  
      });
  });  
</script>  