﻿<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <frame src=”http://localhost/primaxv2” frameborder=”0″>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />       
    <title>T-SOLUCIONA</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/mio.css">
</head>


<body background="img/fondo.jpg">
    <div class="titulo">
        <br><br><br><br><br>
        <!--<center><img src="images/asistencia.png"></center>-->
    </div>
    <div class="container-fluid"><br><br><br><br><br><br>
        
        
        <div class="row">
            <div row form-group>
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <br>
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <form class="form-signin" action="php/iniciar-sesion.php" method="POST">
                        
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input type="text" class="form-control pull-right" name="txtUsuario" required="" placeholder="Correo">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </div>
                                <input type="password" class="form-control pull-right" name="txtClave" required="" placeholder="Contraseña">
                            </div>
                        </div> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button class="btn btn-block" style="background-color: #f1871a" type="submit"><center>INGRESAR</center></button>      
                        </div>        
                        </form>
                    </div>
                    <div class="col-md-3"></div>     
                </div>
                <div class="col-md-3">
                    
                </div>
            </div>
        </div>
    </div>
</body>    

</html>